﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Moq;
using Xunit;

namespace Hayate.Containers
{
    public sealed class AvailableContainerTest : IDisposable
    {
        public AvailableContainerTest()
        {
            _containerId = new Mock<IContainerId>().Object;
            _mockDriver = new Mock<IDriver>(MockBehavior.Strict);
            _imageId = new Mock<IImageId>().Object;

            _target = Container.Available(_containerId, _mockDriver.Object, _imageId);
        }

        public void Dispose()
        {
            _mockDriver.VerifyAll();
        }

        private readonly Container _target;
        private readonly Mock<IDriver> _mockDriver;
        private readonly IContainerId _containerId;
        private readonly IImageId _imageId;

        [Fact]
        public async Task LogsAsync()
        {
            var stream = LogsSource.FromString("hello");

            _mockDriver.Setup(x => x.LogsContainerAsync(_containerId))
                .ReturnsAsync(stream);

            var result = await _target.LogsAsync();

            Assert.Equal("hello", result.ToString());
        }

        [Fact]
        public async Task TtyAsync()
        {
            var stream = TtyStream.FromString("hello");

            _mockDriver.Setup(x => x.ExecContainerAsync(_containerId, "/bin/bash"))
                .ReturnsAsync(stream);

            var result = await _target.TtyAsync();

            Assert.Equal("hello", result.ToString());
        }

        [Fact]
        public async Task StartAsync()
        {
            _mockDriver.Setup(x => x.StartContainerAsync(_containerId))
                .Returns(Task.CompletedTask);

            await _target.StartAsync();
        }

        [Fact]
        public async Task StatusAsync()
        {
            _mockDriver.Setup(x => x.ContainerStatusAsync(_containerId))
                .ReturnsAsync(InstanceStatus.Dead);

            var result = await _target.StatusAsync();

            Assert.Equal(InstanceStatus.Dead, result);
        }

        [Fact]
        public void IsCreatedFrom()
        {
            var result = _target.IsCreatedFrom(_imageId);

            Assert.True(result);
        }

        [Fact]
        public void IsCreatedFromNot()
        {
            var result = _target.IsCreatedFrom(new Mock<IImageId>().Object);

            Assert.False(result);
        }
    }
}
