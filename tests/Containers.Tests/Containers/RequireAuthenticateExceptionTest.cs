﻿using System;
using Xunit;

namespace Hayate.Containers
{
    public sealed class RequireAuthenticateExceptionTest
    {
        [Fact]
        public void ContainsAggregate()
        {
            var target = new AggregateException(new Exception(), new RequireAuthenticateException());

            var result = RequireAuthenticateException.Contains(target);

            Assert.True(result);
        }

        [Fact]
        public void ContainsAggregateNotRequireAuthenticateException()
        {
            var target = new AggregateException(new Exception(), new Exception());

            var result = RequireAuthenticateException.Contains(target);

            Assert.False(result);
        }

        [Fact]
        public void ContainsInnerException()
        {
            var target = new Exception("", new RequireAuthenticateException());

            var result = RequireAuthenticateException.Contains(target);

            Assert.True(result);
        }

        [Fact]
        public void ContainsNotRequireAuthenticateException()
        {
            var target = new Exception();

            var result = RequireAuthenticateException.Contains(target);

            Assert.False(result);
        }

        [Fact]
        public void ContainsNullException()
        {
            var result = RequireAuthenticateException.Contains(null);

            Assert.False(result);
        }

        [Fact]
        public void ContainsRequireAuthenticateException()
        {
            var target = new RequireAuthenticateException();

            var result = RequireAuthenticateException.Contains(target);

            Assert.True(result);
        }
    }
}
