﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Instances;
using Moq;
using Xunit;

namespace Hayate.Containers
{
    public sealed class ImageAliasTest : IDisposable
    {
        public ImageAliasTest()
        {
            _imageId = new ImageId("imageid");
            _imageName = new Mock<IImageName>().Object;
            _mockDriver = new Mock<IDriver>(MockBehavior.Strict);

            _target = new ImageAlias(_imageName, _mockDriver.Object);
        }

        public void Dispose()
        {
            _mockDriver.VerifyAll();
        }

        private readonly ImageAlias _target;
        private readonly Mock<IDriver> _mockDriver;
        private readonly ImageId _imageId;
        private readonly IImageName _imageName;

        private sealed class ImageId : IImageId
        {
            private readonly string _id;

            public ImageId(string id)
            {
                _id = id;
            }

            public override string ToString()
            {
                return _id;
            }
        }

        [Fact]
        public async Task PullAsync()
        {
            _mockDriver.Setup(x => x.PullAsync(_imageName))
                .ReturnsAsync(_imageId);

            var result = await _target.PullAsync();

            Assert.Equal("Image: (Id => imageid)", result.ToString());
        }
    }
}