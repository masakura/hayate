﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Moq;
using Xunit;

namespace Hayate.Containers
{
    public class ImageRegistryTest : IDisposable
    {
        public ImageRegistryTest()
        {
            _imageName = new ImageName("imagename");
            _mockDriver = new Mock<IDriver>(MockBehavior.Strict);

            _target = new ImageRegistry(_mockDriver.Object);
        }

        public void Dispose()
        {
            _mockDriver.VerifyAll();
        }

        private readonly ImageRegistry _target;
        private readonly Mock<IDriver> _mockDriver;
        private readonly ImageName _imageName;

        private sealed class ImageName : IImageName
        {
            private readonly string _name;

            public ImageName(string name)
            {
                _name = name;
            }

            public override string ToString()
            {
                return _name;
            }
        }

        [Fact]
        public async Task FindAsync()
        {
            var result = await _target.FindAsync(_imageName);

            Assert.Equal("Image: (Name => imagename)", result.ToString());
        }
    }
}