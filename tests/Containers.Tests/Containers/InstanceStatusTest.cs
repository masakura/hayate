﻿using System.Collections.Generic;
using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Containers
{
    public sealed class InstanceStatusTest
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public static IEnumerable<object[]> Data => new[]
        {
            new object[] {"Running", InstanceStatus.Running},
            new object[] {"Pulling", InstanceStatus.Pulling},
            new object[] {"Creating", InstanceStatus.Creating},
            new object[] {"Created", InstanceStatus.Created},
            new object[] {"Unknown", InstanceStatus.Unknown},
            new object[] {"Restarting", InstanceStatus.Restarting},
            new object[] {"Removing", InstanceStatus.Removing},
            new object[] {"Paused", InstanceStatus.Paused},
            new object[] {"Exited", InstanceStatus.Exited},
            new object[] {"Dead", InstanceStatus.Dead},
            new object[] {"Pending", InstanceStatus.Pending}
        };

        [Theory]
        [MemberData(nameof(Data))]
        public void Parse(string status, InstanceStatus expected)
        {
            Assert.Equal(expected, InstanceStatus.Parse(status));
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void ToStringTest(string expected, InstanceStatus status)
        {
            Assert.Equal(expected, status.ToString());
        }

        [Fact]
        public void ParseFromNull()
        {
            Assert.Equal(InstanceStatus.Unknown, InstanceStatus.Parse(null));
        }

        [Fact]
        public void ParseFromUnknownStatus()
        {
            Assert.Equal(InstanceStatus.Unknown, InstanceStatus.Parse("siranai keyword"));
        }
    }
}