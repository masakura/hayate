﻿using System;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace Hayate.Containers
{
    public sealed class ImageTest : IDisposable
    {
        public ImageTest()
        {
            _imageId = new Mock<IImageId>().Object;
            _containerId = new ContainerId("containerid");
            _mockDriver = new Mock<IDriver>(MockBehavior.Strict);

            _target = new Image(_imageId, _mockDriver.Object);
        }

        public void Dispose()
        {
            _mockDriver.VerifyAll();
        }

        private readonly Image _target;
        private readonly Mock<IDriver> _mockDriver;
        private readonly IImageId _imageId;
        private readonly ContainerId _containerId;

        private sealed class ContainerId : IContainerId
        {
            private readonly string _id;

            public ContainerId(string id)
            {
                _id = id;
            }

            public override string ToString()
            {
                return _id;
            }
        }

        [Fact]
        public async Task CreateContainerAsync()
        {
            _mockDriver.Setup(x => x.CreateContainerAsync(_imageId))
                .ReturnsAsync(_containerId);

            var result = await _target.CreateContainerAsync();

            Assert.Equal("Container: (Id => containerid)", result.ToString());
        }
    }
}