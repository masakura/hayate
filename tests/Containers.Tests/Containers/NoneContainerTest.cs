﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Containers
{
    public sealed class NoneContainerTest
    {
        public NoneContainerTest()
        {
            _target = Container.NoneContainer();
        }

        private readonly Container _target;

        [Fact]
        public async Task EndPointAsync()
        {
            var result = await _target.EndPointAsync();

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public async Task LogsAsync()
        {
            var result = await _target.LogsAsync();

            Assert.Equal("No container\r\n", result.ToString());
        }

        [Fact]
        public async Task TtyAsync()
        {
            var result = await _target.TtyAsync();

            Assert.Equal("No container\r\n", result.ToString());
        }

        [Fact]
        public async Task StartAsync()
        {
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await _target.StartAsync());
        }

        [Fact]
        public async Task StatusAsync()
        {
            var result = await _target.StatusAsync();

            Assert.Equal(InstanceStatus.Pending, result);
        }
    }
}
