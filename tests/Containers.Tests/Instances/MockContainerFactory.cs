﻿using Hayate.Containers;
using Moq;

namespace Hayate.Instances
{
    internal sealed class MockContainerFactory
    {
        private readonly Mock<IDriver> _mock;
        private readonly MockImage _image;
        private int _number;

        public MockContainerFactory(Mock<IDriver> mock, MockImage image)
        {
            _mock = mock;
            _image = image;
            _number = 0;
        }

        public MockContainer CreateNew()
        {
            _number++;
            return MockContainer.Create(_mock, _image, _number);
        }
    }
}
