﻿using Hayate.Containers;
using Hayate.Core.Applications;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Moq;
using Xunit;

namespace Hayate.Instances
{
    public sealed class InstanceRegistryTest
    {
        public InstanceRegistryTest()
        {
            _target = new InstanceRegistry(new Mock<IImageRegistry>().Object, new InstanceStore());

            _images = new IRegistryImage[]
            {
                new Image("nginx:v1.0"),
                new Image("nginx:v1.1"),
                new Image("nginx:v1.2"),
                new Image("nginx:v1.3")
            };
        }

        private readonly InstanceRegistry _target;
        private readonly IRegistryImage[] _images;

        private static InstanceId InstanceId(IRegistryImage image)
        {
            return Core.Instances.InstanceId.From(image.ImageName);
        }

        private sealed class Image : IRegistryImage
        {
            public Image(string imageName) : this(new ImageName(imageName))
            {
            }

            private Image(IImageName imageName)
            {
                ImageName = imageName;
            }

            public EnvironmentName Name { get; } = default(EnvironmentName);
            // ReSharper disable once MemberHidesStaticFromOuterClass
            public IImageName ImageName { get; }
            public bool IsNotFound { get; } = false;

            public override string ToString()
            {
                return ImageName.ToString();
            }
        }

        private sealed class ImageName : IImageName
        {
            private readonly string _value;

            public ImageName(string value)
            {
                _value = value;
            }


            public override string ToString()
            {
                return _value;
            }
        }

        [Fact]
        public void Find()
        {
            foreach (var name in _images) _target.FindOrCreate(name);

            var result = _target.Find(InstanceId(_images[2]));

            Assert.Equal($"Instance: (Id => {InstanceId(_images[2]).ToString()})", result.ToString());
        }

        [Fact]
        public void FindOrCreate()
        {
            foreach (var name in _images) _target.FindOrCreate(name);

            var result = _target.FindOrCreate(_images[1]);

            Assert.Equal($"Instance: (Id => {InstanceId(_images[1]).ToString()})", result.ToString());
        }

        [Fact]
        public void FindOrCreateNew()
        {
            var result = _target.FindOrCreate(_images[2]);

            Assert.Equal($"Instance: (Id => {InstanceId(_images[2]).ToString()})", result.ToString());
        }

        [Fact]
        public void FindOrCreateSameInstance()
        {
            var one = _target.FindOrCreate(_images[2]);
            var two = _target.FindOrCreate(_images[2]);

            Assert.Equal(one, two);
        }
    }
}
