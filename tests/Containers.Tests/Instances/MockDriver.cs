﻿using Hayate.Containers;
using Moq;

namespace Hayate.Instances
{
    internal sealed class MockDriver
    {
        private readonly Mock<IDriver> _mock;
        private readonly ImageRegistry _images;

        public MockDriver()
        {
            _mock = new Mock<IDriver>(MockBehavior.Strict);
            _images = new ImageRegistry(_mock.Object);
        }

        public MockAlias AddImage(string imageName, string imageId)
        {
            return new MockAlias(_mock, _images, imageName, imageId);
        }
    }
}
