﻿using System;
using Hayate.Containers;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Instances
{
    internal sealed class MockImage
    {
        private readonly MockAlias _alias;
        private readonly ContainerProvider _containers;

        public MockImage(Mock<IDriver> mock, string imageId, MockAlias alias)
        {
            Id = new Identifier(imageId);
            _alias = alias;
            _containers = new ContainerProvider(mock, this);
        }

        public Identifier Id { get; }
        public MockContainer Container => _containers.Current;
        public InstanceStatus CreatingStatus => _containers.CreatingStatus;
        public IInstance Instance => _alias.Instance;
    }

    internal sealed class MockAlias
    {
        private readonly Mock<IDriver> _mock;
        private readonly ImageRegistry _images;
        private MockImage _image;

        public MockAlias(Mock<IDriver> mock, ImageRegistry images, string imageName, string imageId)
        {
            _mock = mock;
            _images = images;
            PushNewImage(imageId);

            Name = new Identifier(imageName);

            Instance = new Instance(Name, images);

            _mock.Setup(x => x.PullAsync(Name))
                .ReturnsAsync(() => Id)
                .Callback(async () => { PullingStatus = await Instance.StatusAsync(); });
        }

        public Instance Instance { get; }
        public IImageId Id => _image.Id;
        private IImageName Name { get; }
        public MockContainer Container => _image.Container;
        public InstanceStatus PullingStatus { get; private set; }
        public InstanceStatus CreatingStatus => _image.CreatingStatus;

        public void PushNewImage(string id)
        {
            _image = new MockImage(_mock, id, this);
        }
    }
}
