﻿using System;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Instances
{
    internal abstract class MockContainer
    {
        public static MockContainer None { get; } = new NoneContainer();
        public abstract IContainerId Id { get; }
        public abstract int TimesOfRestart { get; }

        public static MockContainer Create(Mock<IDriver> mock, MockImage image, int number)
        {
            return new Container(mock, image, number);
        }

        public abstract void SetStatus(InstanceStatus status);

        public abstract void SetEndPoint(EndPoint endPoint);

        public abstract void SetLogs(string message);

        private sealed class NoneContainer : MockContainer
        {
            public override IContainerId Id { get; } = new Identifier(string.Empty);
            public override int TimesOfRestart { get; } = 0;

            public override void SetStatus(InstanceStatus status)
            {
                throw new InvalidOperationException();
            }

            public override string ToString()
            {
                return "Container: (None)";
            }

            public override void SetEndPoint(EndPoint endPoint)
            {
                throw new InvalidOperationException();
            }

            public override void SetLogs(string message)
            {
                throw new InvalidOperationException();
            }
        }

        private sealed class Container : MockContainer
        {
            private readonly Mock<IDriver> _mock;
            private int _timesOfRestart;

            public Container(Mock<IDriver> mock, MockImage image, int number)
            {
                _mock = mock;
                Id = new Identifier($"{image.Id}#{number}");

                mock.Setup(x => x.StartContainerAsync(Id))
                    .Returns(Task.CompletedTask)
                    .Callback(() => SetStatus(InstanceStatus.Running));
                mock.Setup(x => x.RestartContainerAsync(Id))
                    .Returns(Task.CompletedTask)
                    .Callback(() =>
                    {
                        _timesOfRestart++;
                        SetStatus(InstanceStatus.Running);
                    });
            }

            public override int TimesOfRestart => _timesOfRestart;

            public override IContainerId Id { get; }

            public override void SetStatus(InstanceStatus status)
            {
                _mock.Setup(x => x.ContainerStatusAsync(Id)).ReturnsAsync(status);
            }

            public override void SetEndPoint(EndPoint endPoint)
            {
                _mock.Setup(x => x.EndPointAsync(Id)).ReturnsAsync(endPoint);
            }

            public override string ToString()
            {
                return $"Container: (Id => {Id})";
            }

            public override void SetLogs(string message)
            {
                _mock.Setup(x => x.LogsContainerAsync(Id))
                    .ReturnsAsync(LogsSource.FromString(message));
            }
        }
    }
}
