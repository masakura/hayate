﻿using Hayate.Containers;
using Hayate.Core.Instances;

namespace Hayate.Instances
{
    internal struct Identifier : IImageName, IImageId, IContainerId
    {
        private readonly string _value;

        public Identifier(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}