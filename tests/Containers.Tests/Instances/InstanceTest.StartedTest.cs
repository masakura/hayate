﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Instances
{
    public static partial class InstanceTest
    {
        public sealed class StartedTest
        {
            public StartedTest()
            {
                var mock = new MockDriver();

                _image1 = mock.AddImage("image1", "image1id");

                _target = _image1.Instance;

                _target.RunAsync().Wait();
            }

            private readonly MockAlias _image1;
            private readonly Instance _target;


            [Theory]
            [InlineData(nameof(InstanceStatus.Exited))]
            [InlineData(nameof(InstanceStatus.Running))]
            public async Task StatusAsync(string statusText)
            {
                var status = InstanceStatus.Parse(statusText);

                _image1.Container.SetStatus(status);

                var result = await _target.StatusAsync();

                Assert.Equal(status, result);
            }

            [Fact]
            public void CanRestart()
            {
                var result = _target.CanRestart;

                Assert.True(result);
            }

            [Fact]
            public void CanTryUpgrade()
            {
                var result = _target.CanTryUpgrade;

                Assert.True(result);
            }

            [Fact]
            public async Task EndPointAsync()
            {
                var endPoint = new EndPoint("http://example.com/endpoint");

                _image1.Container.SetEndPoint(endPoint);

                var result = await _target.EndPointAsync();

                Assert.Equal(endPoint, result);
            }

            [Fact]
            public async Task LogsAsync()
            {
                _image1.Container.SetLogs("hello");

                var stream = await _target.LogsAsync();
                var result = await stream.ReadAsync();

                Assert.Equal("hello", result);
            }

            [Fact]
            public async Task TransitRestartedAsync()
            {
                var container = _target.Container;

                await _target.TransitAsync(InstanceTransition.Restarted);

                var result = _target.Container;
                var status = await _target.StatusAsync();

                Assert.Equal(container, result);
                Assert.Equal(1, _image1.Container.TimesOfRestart);
                Assert.Equal(InstanceStatus.Running, status);
            }

            [Fact]
            public async Task TransitStartedAsync()
            {
                var container = _target.Container;

                await _target.TransitAsync(InstanceTransition.Running);

                var result = _target.Container;

                Assert.Equal(container, result);
            }

            [Fact]
            public async Task TransitUpgradedAsync()
            {
                var container = _target.Container;
                _image1.PushNewImage("newimage");

                await _target.TransitAsync(InstanceTransition.TryUpgrade);

                var result = _target.Container;

                Assert.NotEqual(container, result);
            }

            [Fact]
            public async Task TransitUpgradedAsyncNotUpdateImage()
            {
                var container = _target.Container;

                await _target.TransitAsync(InstanceTransition.TryUpgrade);

                var result = _target.Container;

                Assert.Equal(container, result);
            }
        }
    }
}
