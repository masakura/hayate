﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Instances
{
    public static partial class InstanceTest
    {
        public sealed class RunTest
        {
            public RunTest()
            {
                var mock = new MockDriver();

                _image1 = mock.AddImage("image1", "image1id");

                _target = _image1.Instance;
            }

            private readonly MockAlias _image1;
            private readonly Instance _target;

            [Fact]
            public async Task RunAsync()
            {
                await _target.RunAsync();

                var result = _target.Container;

                Assert.Equal(_image1.Container.ToString(), result.ToString());
            }

            [Fact]
            public async Task StatusOnCreating()
            {
                await _target.RunAsync();

                var result = _image1.CreatingStatus;

                Assert.Equal(InstanceStatus.Creating, result);
            }

            [Fact]
            public async Task StatusOnPulling()
            {
                await _target.RunAsync();

                var result = _image1.PullingStatus;

                Assert.Equal(InstanceStatus.Pulling, result);
            }
        }
    }
}
