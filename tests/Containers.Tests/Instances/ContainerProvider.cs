﻿using Hayate.Containers;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Instances
{
    internal sealed class ContainerProvider
    {
        private readonly MockImage _image;
        private readonly Mock<IDriver> _mock;
        private readonly MockContainerFactory _factory;

        public ContainerProvider(Mock<IDriver> mock, MockImage image)
        {
            _mock = mock;
            _image = image;
            _factory = new MockContainerFactory(mock, image);
            Current = MockContainer.None;

            _mock.Setup(x => x.CreateContainerAsync(_image.Id))
                .ReturnsAsync(() => CreateNew().Id)
                .Callback(async () => { CreatingStatus = await image.Instance.StatusAsync(); });
        }

        public InstanceStatus CreatingStatus { get; private set; }

        public MockContainer Current { get; private set; }

        public MockContainer CreateNew()
        {
            Current = _factory.CreateNew();
            return Current;
        }
    }
}
