﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Instances
{
    public static partial class InstanceTest
    {
        public sealed class InitialTest
        {
            public InitialTest()
            {
                var mock = new MockDriver();

                var image1 = mock.AddImage("image1", "image1id");

                _target = image1.Instance;
            }

            private readonly Instance _target;

            [Fact]
            public void CanRestart()
            {
                var result = _target.CanRestart;

                Assert.False(result);
            }

            [Fact]
            public void CanTryUpgrade()
            {
                var result = _target.CanTryUpgrade;

                Assert.False(result);
            }

            [Fact]
            public void ContainerIsNone()
            {
                var result = _target.Container;

                Assert.False(result.IsAvailable);
            }

            [Fact]
            public async Task EndPointAsyncIsEmpty()
            {
                var result = await _target.Container.EndPointAsync();

                Assert.True(result.IsEmpty);
            }

            [Fact]
            public async Task LogsAsyncIsEmpty()
            {
                var stream = await _target.LogsAsync();
                var result = await stream.ReadAsync();

                Assert.Equal("No container\r\n", result);
            }

            [Fact]
            public async Task StatusAsyncIsPending()
            {
                var result = await _target.StatusAsync();

                Assert.Equal(InstanceStatus.Pending, result);
            }

            [Fact]
            public async Task TransitRestartedAsync()
            {
                await Assert.ThrowsAsync<InvalidOperationException>(async () => await _target.TransitAsync(InstanceTransition.Restarted));
            }

            [Fact]
            public async Task TransitStartedAsync()
            {
                await _target.TransitAsync(InstanceTransition.Running);

                var result = _target.Container;

                Assert.True(result.IsAvailable);
            }

            [Fact]
            public async Task TransitUpgradedAsync()
            {
                await Assert.ThrowsAsync<InvalidOperationException>(async () => await _target.TransitAsync(InstanceTransition.TryUpgrade));
            }
        }
    }
}
