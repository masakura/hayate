﻿using System.Threading.Tasks;
using Hayate.StreamBinding;
using Hayate.Utility.Mocks;
using Xunit;

namespace Hayate.Utility
{
    public sealed class SourceTest
    {
        [Fact]
        public async Task PipeAsync()
        {
            var source = new OneTimeSource("hello");
            var destination = new Destination();

            await source.PipeAsync(destination);

            Assert.Equal("hello", destination.ToString());
        }

        [Fact]
        public async Task PipeAsyncBrokenDestination()
        {
            var source = new DisposableSource("hello");
            var destination = new BrokenDestination();

            // もし、壊れたデスティネーションへ書き込むと例外が発生して、このテストは失敗する
            await source.PipeAsync(destination);

            Assert.True(source.IsClosed);
        }

        [Fact]
        public async Task PipeAsyncBrokenSource()
        {
            var source = new BreakableSource("hello");
            var destination = new DisposableDestination();

            // もし、壊れたソースから読み取ると例外が発生して、このテストは失敗する
            await source.PipeAsync(destination);

            Assert.True(destination.IsClosed);
        }

        [Fact]
        public async Task PipeAsyncCloseDestinationIfSourceClosed()
        {
            var source = new OneTimeSource("hello");
            var destination = new DisposableDestination();

            await source.PipeAsync(destination);

            Assert.True(destination.IsClosed);
        }

        [Fact]
        public async Task PipeAsyncCloseSourceIfDestinationClosed()
        {
            var source = new DisposableSource("hello");
            var destination = new OneTimeDestination();

            await source.PipeAsync(destination);

            Assert.True(source.IsClosed);
        }

        [Fact]
        public async Task PipeToNullAsync()
        {
            var source = new OneTimeSource("hello");

            await source.PipeToNullAsync();

            // Null に出力するテストなので何も起きません
        }
    }
}