﻿using System;

namespace Hayate.Utility.Mocks
{
    internal sealed class DisposableDestination : Destination, IDisposable
    {
        public void Dispose()
        {
            IsClosed = true;
        }
    }
}