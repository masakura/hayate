﻿namespace Hayate.Utility.Mocks
{
    /// <summary>
    /// オープンしているか確認した直後にクローズして読み取れなくなるソース。
    /// </summary>
    internal sealed class BreakableSource : Source
    {
        public BreakableSource(string s) : base(s)
        {
        }

        public override bool IsClosed
        {
            get
            {
                var result = base.IsClosed;
                base.IsClosed = true;
                return result;
            }
            protected set => base.IsClosed = value;
        }
    }
}