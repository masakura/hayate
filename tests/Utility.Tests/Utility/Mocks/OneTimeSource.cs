﻿using System.Threading.Tasks;
using Hayate.Utility.Mocks;

namespace Hayate.Utility
{
    internal sealed class OneTimeSource : Source
    {
        public OneTimeSource(string s) : base(s)
        {
        }

        public override async Task<string> ReadAsync()
        {
            var result = await base.ReadAsync();
            IsClosed = true;
            return result;
        }
    }
}