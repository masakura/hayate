﻿using System;

namespace Hayate.Utility.Mocks
{
    internal sealed class DisposableSource : Source, IDisposable   
    {
        public DisposableSource(string s) : base(s)
        {
        }

        public void Dispose()
        {
            IsClosed = true;
        }
    }
}