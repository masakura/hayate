﻿using System;
using System.IO;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Utility.Mocks
{
    internal class Source : ISource
    {
        private readonly StringReader _reader;

        public Source(string s)
        {
            _reader = new StringReader(s);
        }

        public virtual bool IsClosed { get; protected set; }

        public virtual async Task<string> ReadAsync()
        {
            if (IsClosed) throw new InvalidOperationException("Source is closed");

            return await _reader.ReadToEndAsync();
        }
    }
}