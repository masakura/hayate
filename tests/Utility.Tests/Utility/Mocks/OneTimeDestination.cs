﻿using System.Threading.Tasks;

namespace Hayate.Utility.Mocks
{
    internal sealed class OneTimeDestination : Destination
    {
        public override async Task WriteAsync(string s)
        {
            await base.WriteAsync(s);
            IsClosed = true;
        }
    }
}