﻿using System;
using System.IO;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Utility.Mocks
{
    public class Destination : IDestination
    {
        private readonly StringWriter _writer;

        public Destination()
        {
            _writer = new StringWriter();
        }

        public virtual bool IsClosed { get; protected set; }

        public virtual async Task WriteAsync(string s)
        {
            if (IsClosed) throw new InvalidOperationException("Destination is closed");

            await _writer.WriteAsync(s);
        }

        public override string ToString()
        {
            return _writer.ToString();
        }
    }
}