﻿using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Utility.Mocks
{
    internal sealed class Reflection : ISource, IDestination
    {
        private string _text;

        public bool IsClosed { get; } = false;

        public Task WriteAsync(string s)
        {
            _text = s;
            return Task.CompletedTask;
        }

        public Task<string> ReadAsync()
        {
            return Task.FromResult(_text);
        }
    }
}