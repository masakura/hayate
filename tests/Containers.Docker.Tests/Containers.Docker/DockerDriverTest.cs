﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;
using Hayate.Core.Instances;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerDriverTest : IDisposable
    {
        public DockerDriverTest()
        {
            var mockClient = new Mock<IDockerClient>();

            _mockImages = mockClient.BuildChildMock(x => x.Images);
            _mockContainers = mockClient.BuildChildMock(x => x.Containers);

            var options = new DockerOptions
            {
                DockerHost = "dockerd.example.com"
            };
            _target = DockerDriver.Create(mockClient.Object, new OptionsWrapper<DockerOptions>(options));

            _scope = DockerRegistryPasswordCredentialScope.NewScope(DockerRegistryPasswordCredential.Empty);
        }

        public void Dispose()
        {
            _scope.Dispose();

            _mockImages.VerifyAll();
            _mockContainers.VerifyAll();
        }

        private readonly DockerDriver _target;
        private readonly Mock<IImageOperations> _mockImages;
        private readonly Mock<IContainerOperations> _mockContainers;

        private static readonly DockerImageId ImageId =
            new DockerImageId("sha256:819725cec8dd22abe303d87fc35a51b6f2c46f63954aa62c9765f3bfbf21fcaf");

        private static readonly DockerImageName ImageName = DockerImageName.Parse("nginx:v1.0");

        private static readonly DockerContainerId ContainerId =
            new DockerContainerId("98600f68ad42e74e7fb40181d9e21b370b1350c5fd75bd58bf8e4c2c45e68a96");

        private readonly DockerRegistryPasswordCredentialScope _scope;

        private static ImagesCreateParameters Is(ImagesCreateParameters p)
        {
            return Match.Create<ImagesCreateParameters>(x =>
                x.FromImage == p.FromImage &&
                x.FromSrc == p.FromSrc &&
                x.Repo == p.Repo &&
                x.Tag == p.Tag);
        }

        private CreateContainerParameters Is(CreateContainerParameters p)
        {
            return Match.Create<CreateContainerParameters>(x =>
                x.Image == p.Image &&
                x.HostConfig.PublishAllPorts == p.HostConfig.PublishAllPorts);
        }

        private static ContainerInspectResponse InspectResponse(Dictionary<string, IList<PortBinding>> ports)
        {
            return new ContainerInspectResponse
            {
                NetworkSettings = new NetworkSettings
                {
                    Ports = ports
                }
            };
        }

        [Fact]
        public async Task ContainerStatusAsync()
        {
            _mockContainers.Setup(x =>
                    x.InspectContainerAsync(ContainerId.ToString(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ContainerInspectResponse
                {
                    State = new ContainerState
                    {
                        Status = "running"
                    }
                });

            var result = await _target.ContainerStatusAsync(ContainerId);

            Assert.Equal(InstanceStatus.Running, result);
        }

        [Fact]
        public async Task CreateContainerAsync()
        {
            _mockContainers.Setup(x =>
                    x.CreateContainerAsync(Is(new CreateContainerParameters
                        {
                            Image = ImageId.ToString(),
                            HostConfig = new HostConfig
                            {
                                PublishAllPorts = true
                            }
                        }),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CreateContainerResponse {ID = ContainerId.ToString()});

            var result = await _target.CreateContainerAsync(ImageId);

            Assert.Equal(ContainerId, result);
        }

        [Fact]
        public async Task EndPointAsyncNoEndpoint()
        {
            _mockContainers.Setup(x =>
                    x.InspectContainerAsync(ContainerId.ToString(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(InspectResponse(new Dictionary<string, IList<PortBinding>>()));

            var result = await _target.EndPointAsync(ContainerId);

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public async Task EndPointAsyncOneEndpoint()
        {
            _mockContainers.Setup(x =>
                    x.InspectContainerAsync(ContainerId.ToString(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(InspectResponse(new Dictionary<string, IList<PortBinding>>
                {
                    {
                        "80/tcp",
                        new List<PortBinding>
                        {
                            new PortBinding {HostIP = "0.0.0.0", HostPort = "32768"}
                        }
                    }
                }));

            var result = await _target.EndPointAsync(ContainerId);

            Assert.Equal(new Uri("http://dockerd.example.com:32768"), result.ToUri());
        }

        [Fact]
        public async Task ExecContainerAsync()
        {
            var response = new ContainerExecCreateResponse{ID = "execid"};
            var stream = new MultiplexedStream(new WriteClosableStreamStub(), true);

            _mockContainers.Setup(x =>
                    x.ExecCreateContainerAsync(ContainerId.ToString(),
                        It.Is<ContainerExecCreateParameters>(p =>
                            p.AttachStdin &&
                            p.AttachStdout &&
                            p.AttachStderr &&
                            p.Tty &&
                            p.Cmd.Count == 1 &&
                            p.Cmd[0] == "/bin/bash"),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);
            _mockContainers.Setup(x =>
                    x.StartWithConfigContainerExecAsync("execid", It.Is<ContainerExecStartParameters>(p =>
                        p.AttachStdin && p.AttachStdout && p.AttachStderr && p.Tty), It.IsAny<CancellationToken>()))
                .ReturnsAsync(stream);

            var result = await _target.ExecContainerAsync(ContainerId, "/bin/bash");

            Assert.IsAssignableFrom<DockerContainerTtyStream>(result);
            Assert.True(((DockerContainerTtyStream)result).Is(stream));
        }

        [Fact]
        public async Task LogsContainerAsync()
        {
            var stream = new DockerLogsStreamBuilder("hello\n").Build();

            _mockContainers.Setup(x =>
                    x.GetContainerLogsAsync(ContainerId.ToString(),
                        It.Is<ContainerLogsParameters>(p =>
                            p.Follow == true && p.ShowStdout == true && p.ShowStderr == true && p.Tail == "25"),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(stream);

            var result = await _target.LogsContainerAsync(ContainerId);

            Assert.Equal("hello\r\n", result.ToString());
        }

        [Fact]
        public async Task PullAsync()
        {
            _mockImages.Setup(x =>
                    x.CreateImageAsync(Is(new ImagesCreateParameters {FromImage = ImageName.ToString()}),
                        null,
                        It.IsAny<IProgress<JSONMessage>>(),
                        It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            _mockImages.Setup(x =>
                    x.InspectImageAsync(ImageName.ToString(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ImageInspectResponse
                {
                    ID = ImageId.ToString()
                });

            var result = await _target.PullAsync(ImageName);

            Assert.Equal(ImageId, result);
        }

        [Fact]
        public async Task RestartContainerAsync()
        {
            _mockContainers.Setup(x => x.RestartContainerAsync(ContainerId.ToString(),
                    It.IsAny<ContainerRestartParameters>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            await _target.RestartContainerAsync(ContainerId);
        }

        [Fact]
        public async Task StartContainerAsync()
        {
            _mockContainers.Setup(x =>
                    x.StartContainerAsync(ContainerId.ToString(),
                        It.IsAny<ContainerStartParameters>(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            await _target.StartContainerAsync(ContainerId);
        }
    }
}
