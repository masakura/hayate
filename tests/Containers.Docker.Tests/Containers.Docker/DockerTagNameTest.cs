﻿using System;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerTagNameTest
    {
        [Fact]
        public void AEqualA()
        {
            var result = new DockerTagName("A");

            Assert.Equal(new DockerTagName("A"), result);
        }

        [Fact]
        public void ANotEqualB()
        {
            var result = new DockerTagName("A");

            Assert.NotEqual(new DockerTagName("B"), result);
        }

        [Fact]
        public void Default()
        {
            var result = DockerTagName.Default;

            Assert.Equal(new DockerTagName("latest"), result);
        }

        [Fact]
        public void EmptyNameIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentException>(() => new DockerTagName(string.Empty));
        }

        [Fact]
        public void NullNameIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentNullException>(() => new DockerTagName(null));
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new DockerTagName("hello");

            Assert.Equal("hello", result.ToString());
        }
    }
}