﻿using System;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerRepositoryNameTest
    {
        [Fact]
        public void AEqualA()
        {
            var result = new DockerRepositoryName("A");

            Assert.Equal(new DockerRepositoryName("A"), result);
        }

        [Fact]
        public void ANotEqualB()
        {
            var result = new DockerRepositoryName("A");

            Assert.NotEqual(new DockerRepositoryName("B"), result);
        }

        [Fact]
        public void EmptyNameIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentException>(() => new DockerRepositoryName(string.Empty));
        }

        [Fact]
        public void NullNameIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentNullException>(() => new DockerRepositoryName(null));
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new DockerRepositoryName("hello");

            Assert.Equal("hello", result.ToString());
        }
    }
}