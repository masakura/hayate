﻿using System.IO;
using System.Threading.Tasks;
using Docker.DotNet;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed partial class DockerContainerTtyStreamTest
    {
        public DockerContainerTtyStreamTest()
        {
            _input = new MemoryStream();
            _output = new MemoryStream();

            _target = new DockerContainerTtyStream(new MultiplexedStream(new WriteClosableStreamStub(_input, _output),
                true));
        }

        private readonly MemoryStream _input;
        private readonly MemoryStream _output;
        private DockerContainerTtyStream _target;

        [Fact]
        public async Task ReadAsync()
        {
            _input.WriteOutput(StreamType.StandardOutput, "hello!");

            var result = await _target.ReadAsync();

            Assert.Equal("hello!", result);
        }

        [Fact]
        public async Task WriteAsync()
        {
            await _target.WriteAsync("hello!");
            var result = _output.ReadToEnd();

            Assert.Equal("hello!", result);
        }
    }
}
