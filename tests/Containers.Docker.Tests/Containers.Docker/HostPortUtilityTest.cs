﻿using System.Collections.Generic;
using System.Linq;
using Docker.DotNet.Models;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class HostPortUtilityTest
    {
        private sealed class Ports : Dictionary<string, IList<PortBinding>>
        {
        }

        private static PortBinding Binding(string ipAddress, string port)
        {
            return new PortBinding {HostIP = ipAddress, HostPort = port};
        }

        private static IList<PortBinding> Bindings(params int[] ports)
        {
            return ports.Select(port => Binding("0.0.0.0", port.ToString()))
                .ToList();
        }

        [Fact]
        public void GetHostPort()
        {
            var result = HostPortUtility.GetHostPort(new Ports
            {
                {"22/tcp", Bindings(2022, 2023)},
                {"80/tcp", Bindings(1024, 1025, 1000)},
                {"1000/tcp", Bindings()}
            });

            Assert.Equal(1000, result);
        }

        [Fact]
        public void GetHostPortManyPort()
        {
            var result = HostPortUtility.GetHostPort(new Ports
            {
                {"22/tcp", Bindings(1234)},
                {"3000/tcp", Bindings(5000)}
            });

            Assert.Equal(5000, result);
        }

        [Fact]
        public void GetHostPortNoPort()
        {
            var result = HostPortUtility.GetHostPort(new Ports());

            Assert.Equal(0, result);
        }

        [Fact]
        public void GetHostPortOnePort()
        {
            var result = HostPortUtility.GetHostPort(new Ports
            {
                {"80/tcp", Bindings(32768)}
            });

            Assert.Equal(32768, result);
        }

        [Fact]
        public void GetHostPortOnePortManyBindings()
        {
            var result = HostPortUtility.GetHostPort(new Ports
            {
                {"80/tcp", Bindings(3000, 3001, 2000)}
            });

            Assert.Equal(2000, result);
        }

        [Fact]
        public void GetHostPortOnePortNoBindings()
        {
            var result = HostPortUtility.GetHostPort(new Ports
            {
                {"80/tcp", Bindings()}
            });

            Assert.Equal(0, result);
        }
    }
}