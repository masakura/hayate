﻿using System.Threading.Tasks;
using Hayate.Core.Tokens;
using Moq;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerRegistryPasswordCredentialStoreTest
    {
        public DockerRegistryPasswordCredentialStoreTest()
        {
            _mock = new Mock<IGitLabPatRegistrationService>(MockBehavior.Strict);
            _target = new DockerRegistryPasswordCredentialStore(_mock.Object);
        }

        private readonly Mock<IGitLabPatRegistrationService> _mock;
        private readonly DockerRegistryPasswordCredentialStore _target;

        [Fact]
        public async Task GetCurrentUserCredentialAsync()
        {
            _mock.Setup(x => x.LoadPatAsync()).ReturnsAsync(new PersonalAccessToken("password"));

            var result = await _target.GetCurrentUserCredentialAsync();

            Assert.Equal(result, new DockerRegistryPasswordCredential("hayate", "password"));
        }

        [Fact]
        public async Task GetCurrentUserCredentialAsyncNoPat()
        {
            _mock.Setup(x => x.LoadPatAsync()).ReturnsAsync(PersonalAccessToken.Empty);

            var result = await _target.GetCurrentUserCredentialAsync();

            Assert.Equal(result, new DockerRegistryPasswordCredential("hayate", null));
        }
    }
}
