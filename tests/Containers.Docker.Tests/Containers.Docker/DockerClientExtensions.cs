﻿using System;
using System.Linq.Expressions;
using Docker.DotNet;
using Moq;

namespace Hayate.Containers.Docker
{
    internal static class DockerClientExtensions
    {
        public static Mock<T> BuildChildMock<T>(this Mock<IDockerClient> client, Expression<Func<IDockerClient, T>> expression) where T : class
        {
            var mock = new Mock<T>(MockBehavior.Strict);

            client.SetupGet(expression).Returns(mock.Object);

            return mock;
        }
    }
}