﻿using System;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerImageIdTest
    {
        [Fact]
        public void AEqualA()
        {
            var result = new DockerImageId("A");

            Assert.Equal(new DockerImageId("A"), result);
        }

        [Fact]
        public void ANotEqualB()
        {
            var result = new DockerImageId("A");

            Assert.NotEqual(new DockerImageId("B"), result);
        }

        [Fact]
        public void EmptyIdIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentException>(() => new DockerImageId(string.Empty));
        }

        [Fact]
        public void NullIdIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentNullException>(() => new DockerImageId(null));
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new DockerImageId("efh3294g");

            Assert.Equal("efh3294g", result.ToString());
        }
    }
}