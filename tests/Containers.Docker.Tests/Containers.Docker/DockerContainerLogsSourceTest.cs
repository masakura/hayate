﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerContainerLogsSourceTest
    {
        [Fact]
        public async Task ReadAsync()
        {
            var stream = new DockerLogsStreamBuilder("hello\n").Build();

            var target = new DockerContainerLogsSource(stream);

            var result = await target.ReadAsync();
            
            Assert.Equal("hello\r\n", result);
        }

        [Fact]
        public async Task TaskReadAsync1025()
        {
            var @long = LongString(1025);
            var stream = new DockerLogsStreamBuilder(@long).Build();
            
            var target = new DockerContainerLogsSource(stream);

            var result = await target.ReadAsync();
            
            Assert.Equal(@long, result);
        }

        private static string LongString(int count)
        {
            return new string(Enumerable.Range(0, count).Select(i => 'a').ToArray());
        }
    }
}