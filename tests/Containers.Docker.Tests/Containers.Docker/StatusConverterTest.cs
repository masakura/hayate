﻿using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Containers.Docker
{
    public class StatusConverterTest
    {
        [Fact]
        public void Created()
        {
            Assert.Equal(InstanceStatus.Created, StatusConverter.FromDockerContainer("created"));
        }

        [Fact]
        public void Paused()
        {
            Assert.Equal(InstanceStatus.Paused, StatusConverter.FromDockerContainer("paused"));
        }

        [Fact]
        public void Removing()
        {
            Assert.Equal(InstanceStatus.Removing, StatusConverter.FromDockerContainer("removing"));
        }

        [Fact]
        public void Restarting()
        {
            Assert.Equal(InstanceStatus.Restarting, StatusConverter.FromDockerContainer("restarting"));
        }

        [Fact]
        public void Running()
        {
            Assert.Equal(InstanceStatus.Running, StatusConverter.FromDockerContainer("running"));
        }

        [Fact]
        public void Exited()
        {
            Assert.Equal(InstanceStatus.Exited, StatusConverter.FromDockerContainer("exited"));
        }

        [Fact]
        public void Dead()
        {
            Assert.Equal(InstanceStatus.Dead, StatusConverter.FromDockerContainer("dead"));
        }
    }
}