﻿using System;
using System.IO;
using Microsoft.Net.Http.Client;

namespace Hayate.Containers.Docker
{
    internal sealed class WriteClosableStreamStub : WriteClosableStream
    {
        private readonly MemoryStream _input;
        private readonly Stream _output;

        public WriteClosableStreamStub(MemoryStream input, Stream output)
        {
            _input = input;
            _output = output;
        }

        public WriteClosableStreamStub() : this(new MemoryStream(), new MemoryStream())
        {
        }

        public override bool CanRead { get; } = true;
        public override bool CanSeek { get; } = true;
        public override bool CanWrite { get; } = true;
        public override long Length => throw new InvalidOperationException();
        public override long Position { get; set; }
        public override bool CanCloseWrite => throw new InvalidOperationException();

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _input.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new InvalidOperationException();
        }

        public override void SetLength(long value)
        {
            throw new InvalidOperationException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _output.Write(buffer, offset, count);
        }

        public override void CloseWrite()
        {
            throw new InvalidOperationException();
        }
    }
}
