﻿using System.IO;
using System.Text;

namespace Hayate.Containers.Docker
{
    internal static class StreamExtensions
    {
        public static string ReadToEnd(this Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static void WriteOutput(this Stream stream, StreamType type, string s)
        {
            var header = new byte[8];
            header[0] = (byte) type;

            var payload = Encoding.UTF8.GetBytes(s);
            header[7] = (byte) payload.Length;

            stream.Write(header, 0, header.Length);
            stream.Write(payload, 0, payload.Length);
            stream.Seek(0, SeekOrigin.Begin);
        }
    }
}
