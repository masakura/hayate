﻿using System;
using System.IO;
using System.Text;

namespace Hayate.Containers.Docker
{
    internal sealed class DockerLogsStreamBuilder : IDisposable
    {
        private readonly MemoryStream _stream = new MemoryStream();

        private DockerLogsStreamBuilder()
        {
        }

        public DockerLogsStreamBuilder(string s) : this()
        {
            Append(s);
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

        private void Append(string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            var header = new byte[] {1, 0, 0, 0, GetSize(bytes, 24), GetSize(bytes, 16), GetSize(bytes, 8), GetSize(bytes, 0)};
            _stream.Write(header, 0, header.Length);
            _stream.Write(bytes, 0, bytes.Length);
        }

        private static byte GetSize(byte[] bytes, int shift)
        {
            var temp = bytes.Length >> shift;
            return (byte) temp;
        }

        public Stream Build()
        {
            _stream.Position = 0;
            return _stream;
        }
    }
}