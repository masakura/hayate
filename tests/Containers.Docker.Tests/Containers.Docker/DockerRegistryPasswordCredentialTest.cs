﻿using System.Net.Http.Headers;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerRegistryPasswordCredentialTest
    {
        [Fact]
        public void RequireAuthenticate()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", "password");

            Assert.True(target.HasPassword);
        }

        [Fact]
        public void RequireAuthenticateEmptyPassword()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", "");

            Assert.False(target.HasPassword);
        }

        [Fact]
        public void RequireAuthenticateNoPassword()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", null);

            Assert.False(target.HasPassword);
        }

        [Fact]
        public void ToAuthConfig()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", "password");

            var result = target.ToAuthConfig();

            Assert.Equal("hogehoge", result.Username);
            Assert.Equal("password", result.Password);
        }

        [Fact]
        public void ToAuthConfigEmptyPassword()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", "");

            Assert.Null(target.ToAuthConfig());
        }

        [Fact]
        public void ToAuthConfigNoPassword()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", null);

            Assert.Null(target.ToAuthConfig());
        }

        [Fact]
        public void ToAuthenticationHeader()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", "password");

            Assert.Equal(target.ToAuthenticationHeader(),
                new AuthenticationHeaderValue("Basic", "aG9nZWhvZ2U6cGFzc3dvcmQ="));
        }

        [Fact]
        public void ToAuthenticationHeaderNoPassword()
        {
            var target = new DockerRegistryPasswordCredential("hogehoge", null);

            Assert.Null(target.ToAuthenticationHeader());
        }
    }
}
