﻿namespace Hayate.Containers.Docker
{
    internal enum StreamType : byte
    {
        StandardInput = 0,
        StandardOutput = 1,
        StandardError = 2
    }
}