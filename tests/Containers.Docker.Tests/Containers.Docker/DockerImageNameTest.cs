﻿using System;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerImageNameTest
    {
        public DockerImageNameTest()
        {
            _repository1 = new DockerRepositoryName("repository1");
            _repository2 = new DockerRepositoryName("repository2");
            _tag1 = new DockerTagName("tag1");
            _tag2 = new DockerTagName("tag2");
        }

        private readonly DockerRepositoryName _repository1;
        private readonly DockerRepositoryName _repository2;
        private readonly DockerTagName _tag1;
        private readonly DockerTagName _tag2;

        [Fact]
        public void AppendHostAndPort5000Port()
        {
            var result = new DockerImageName(_repository1, _tag1)
                .AppendHostAndPort(new Uri("http://registry.example.com:5000"));

            Assert.Equal(DockerImageName.Parse("registry.example.com:5000/repository1:tag1"), result);
        }

        [Fact]
        public void AppendHostAndPortHttp()
        {
            var result = new DockerImageName(_repository1, _tag1)
                .AppendHostAndPort(new Uri("http://registry.example.com"));

            Assert.Equal(DockerImageName.Parse("registry.example.com/repository1:tag1"), result);
        }

        [Fact]
        public void AppendHostAndPortHttps()
        {
            var result = new DockerImageName(_repository1, _tag1)
                .AppendHostAndPort(new Uri("https://registry.example.com"));

            Assert.Equal(DockerImageName.Parse("registry.example.com/repository1:tag1"), result);
        }

        [Fact]
        public void Name11EqualName11()
        {
            var result = new DockerImageName(_repository1, _tag1);

            Assert.Equal(new DockerImageName(_repository1, _tag1), result);
        }

        [Fact]
        public void Name11IsNotEqualName12()
        {
            var result = new DockerImageName(_repository1, _tag2);

            Assert.NotEqual(new DockerImageName(_repository1, _tag1), result);
        }

        [Fact]
        public void Name11IsNotEqualName21()
        {
            var result = new DockerImageName(_repository2, _tag1);

            Assert.NotEqual(new DockerImageName(_repository1, _tag1), result);
        }

        [Fact]
        public void Name11IsNotEqualName22()
        {
            var result = new DockerImageName(_repository2, _tag2);

            Assert.NotEqual(new DockerImageName(_repository1, _tag1), result);
        }

        [Fact]
        public void ParseWithoutTag()
        {
            var result = DockerImageName.Parse("repository1");

            Assert.Equal(new DockerImageName(_repository1), result);
        }

        [Fact]
        public void ParseWithTag()
        {
            var result = DockerImageName.Parse("repository1:tag1");

            Assert.Equal(new DockerImageName(_repository1, _tag1), result);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new DockerImageName(_repository1, _tag1);

            Assert.Equal("repository1:tag1", result.ToString());
        }

        [Fact]
        public void UseDefaultTag()
        {
            var result = new DockerImageName(_repository1);

            Assert.Equal("repository1:latest", result.ToString());
        }
    }
}