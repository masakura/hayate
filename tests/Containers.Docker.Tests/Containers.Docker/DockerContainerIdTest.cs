﻿using System;
using Xunit;

namespace Hayate.Containers.Docker
{
    public sealed class DockerContainerIdTest
    {
        [Fact]
        public void AEqualA()
        {
            var result = new DockerContainerId("A");

            Assert.Equal(new DockerContainerId("A"), result);
        }

        [Fact]
        public void ANotEqualB()
        {
            var result = new DockerContainerId("A");

            Assert.NotEqual(new DockerContainerId("B"), result);
        }

        [Fact]
        public void EmptyIdIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentException>(() => new DockerContainerId(string.Empty));
        }

        [Fact]
        public void NullIdIsInvalid()
        {
            // ReSharper disable once HeapView.BoxingAllocation
            Assert.Throws<ArgumentNullException>(() => new DockerContainerId(null));
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new DockerContainerId("efh3294g");

            Assert.Equal("efh3294g", result.ToString());
        }
    }
}