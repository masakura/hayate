﻿using Hayate.Provider.GitLab;
using Xunit;

namespace Hayate.Authentication.GitLab
{
    public class GitLabProviderOptionsExtensionsTest
    {
        public GitLabProviderOptionsExtensionsTest()
        {
            _target = new GitLabProviderOptions {Endpoint = "http://gitlab.com"};
        }

        private readonly GitLabProviderOptions _target;

        [Fact]
        public void AuthorizeEndpoint()
        {
            var result = _target.AuthorizationEndpoint();

            Assert.Equal("http://gitlab.com/oauth/authorize", result);
        }

        [Fact]
        public void TokenEndpoint()
        {
            var result = _target.TokenEndpoint();

            Assert.Equal("http://gitlab.com/oauth/token", result);
        }

        [Fact]
        public void UserInfoEndpoint()
        {
            var result = _target.UserInfoEndpoint();

            Assert.Equal("http://gitlab.com/api/v4/user", result);
        }
    }
}