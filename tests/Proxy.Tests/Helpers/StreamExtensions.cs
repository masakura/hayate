﻿using System;
using System.IO;

namespace Hayate.Helpers
{
    internal static class StreamExtensions
    {
        public static string ReadAsString(this Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));

            stream.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}