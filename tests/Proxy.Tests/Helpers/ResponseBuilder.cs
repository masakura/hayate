﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Hayate.Proxy.Converters;

namespace Hayate.Helpers
{
    internal sealed class ResponseBuilder
    {
        private readonly HttpResponseMessage _response;

        public ResponseBuilder() : this(new HttpResponseMessage())
        {
        }

        private ResponseBuilder(HttpResponseMessage response)
        {
            _response = response;
        }

        public HttpResponseMessage Build()
        {
            return _response;
        }

        public ResponseBuilder Content(string body)
        {
            var content = new StringContent(body);
            AddContentType(content);
            _response.Content = content;
            return this;
        }

        private static void AddContentType(HttpContent content, string mediaType = "text/html",
            string charSet = "utf-8")
        {
            content.Headers.ContentType = new MediaTypeHeaderValue(mediaType)
            {
                CharSet = charSet
            };
        }

        public ResponseBuilder Location(string location)
        {
            _response.Headers.Location = new Uri(location);
            return this;
        }

        public ResponseBuilder SetCookie(string cookie)
        {
            _response.Headers.Add("Set-Cookie", cookie);
            return this;
        }

        public ResponseBuilder StatusCode(HttpStatusCode statusCode)
        {
            _response.StatusCode = statusCode;
            return this;
        }

        public ResponseBuilder GZippedContent(string body)
        {
            var content = GZipHttpContentCreator.GZipFromAsync(body).Result;
            AddContentType(content);
            content.Headers.ContentEncoding.Add("gzip");
            _response.Content = content;
            return this;
        }
    }
}