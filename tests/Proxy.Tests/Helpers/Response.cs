﻿using System.Net;
using System.Net.Http;

namespace Hayate.Helpers
{
    internal static class Response {
        public static HttpResponseMessage Location(string location)
        {
            return new ResponseBuilder()
                .Location(location)
                .Content("hello")
                .Build();
        }

        public static HttpResponseMessage Html(string html)
        {
            return new ResponseBuilder()
                .Content(html)
                .Build();
        }

        public static HttpResponseMessage SetCookie(string cookie)
        {
            return new ResponseBuilder()
                .SetCookie(cookie)
                .Content("hello")
                .Build();
        }

        public static HttpResponseMessage GZipHtml(string html)
        {
            return new ResponseBuilder()
                .GZippedContent(html)
                .Build();
        }

        public static HttpResponseMessage Redirect(string location)
        {
            return new ResponseBuilder()
                .Location(location)
                .Content("hello")
                .StatusCode(HttpStatusCode.Redirect)
                .Build();
        }
    }
}