﻿using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy;
using Hayate.Proxy.Applications;
using Hayate.Proxy.Containers;
using Xunit;

namespace Hayate
{
    public class CompositeApplicationMiddlewareRegistryTest
    {
        public CompositeApplicationMiddlewareRegistryTest()
        {
            _target = new CompositeApplicationMiddlewareRegistry();
        }

        private readonly CompositeApplicationMiddlewareRegistry _target;

        [Fact]
        public async Task FindAsyncProxy()
        {
            var application = new Application
            {
                EndPoint = new EndPoint("http://localhost:1234/")
            };

            var result = await _target.FindAsync(application);

            Assert.IsAssignableFrom<ContainerProxyMiddleware>(result);
        }

        [Fact]
        public async Task FindAsyncRedirectToManagement()
        {
            var application = new Application
            {
                Status = InstanceStatus.Pending,
                RequireStart = true
            };

            var result = await _target.FindAsync(application);

            Assert.IsAssignableFrom<RedirectToEnvironmentManagerMiddleware>(result);
        }

        [Fact]
        public async Task FindAsyncStatus()
        {
            var application = new Application
            {
                Status = InstanceStatus.Restarting
            };

            var result = await _target.FindAsync(application);

            Assert.IsAssignableFrom<ApplicationStatusMiddleware>(result);
        }
    }
}