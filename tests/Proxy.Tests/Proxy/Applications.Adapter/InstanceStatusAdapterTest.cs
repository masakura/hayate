﻿using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Proxy.Applications.Adapter
{
    public sealed class InstanceStatusAdapterTest
    {
        public InstanceStatusAdapterTest()
        {
            _target = InstanceStatusAdapter.Create(InstanceStatus.Creating);
        }

        private readonly InstanceStatusAdapter _target;

        [Fact]
        public void EqualsTest()
        {
            var another = InstanceStatusAdapter.Create(InstanceStatus.Creating);

            // ReSharper disable once EqualExpressionComparison
            Assert.True(_target.Equals(_target));
            Assert.True(_target.Equals(another));
            Assert.True(another.Equals(_target));
        }

        [Fact]
        public void NotEqualsTest()
        {
            var another = InstanceStatusAdapter.Create(InstanceStatus.Dead);

            // ReSharper disable once EqualExpressionComparison
            Assert.False(_target.Equals(another));
            Assert.False(another.Equals(_target));
        }

        [Fact]
        public void Same()
        {
            var another = InstanceStatusAdapter.Create(InstanceStatus.Creating);
            
            Assert.Same(another, _target);
        }

        [Fact]
        public void Message()
        {
            Assert.Equal("Creating", _target.Message);
        }
    }
}