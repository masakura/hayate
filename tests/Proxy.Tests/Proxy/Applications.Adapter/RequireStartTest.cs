﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Proxy.Applications.Adapter
{
    public sealed class RequireStartTest
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public static IEnumerable<object[]> Requires = new object[]
        {
            InstanceStatus.Unknown,
            InstanceStatus.Removing,
            InstanceStatus.Paused,
            InstanceStatus.Exited,
            InstanceStatus.Dead,
            InstanceStatus.Pending
        }.Select(i => new[] {i});

        // ReSharper disable once MemberCanBePrivate.Global
        public static IEnumerable<object[]> NotRequires = new object[]
        {
            InstanceStatus.Running,
            InstanceStatus.Pulling,
            InstanceStatus.Creating,
            InstanceStatus.Created,
            InstanceStatus.Restarting
        }.Select(i => new[] {i});

        [Theory]
        [MemberData(nameof(Requires))]
        public void Require(InstanceStatus status)
        {
            var result = RequireStart.Require(status);

            Assert.True(result);
        }
    }
}