﻿using System.Net.Http;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy.Containers;
using Microsoft.AspNetCore.Builder;
using Moq;
using Xunit;

namespace Hayate.Proxy
{
    public sealed class ProxyMiddlewareOptionsFactoryTest
    {
        public ProxyMiddlewareOptionsFactoryTest()
        {
            _handler = new Mock<HttpClientHandler>().Object;
            _target = ProxyMiddlewareOptionsFactory.Create(Target, _handler);
        }

        private static readonly EndPoint Target = new EndPoint("http://example.com:3000");
        private readonly ProxyOptions _target;
        private readonly HttpClientHandler _handler;

        [Fact]
        public void CreateBasicSetting()
        {
            Assert.Equal("http", _target.Scheme);
            Assert.Equal("example.com", _target.Host);
            Assert.Equal("3000", _target.Port);
            Assert.Equal(_handler, _target.BackChannelMessageHandler);
        }
    }
}