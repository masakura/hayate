﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Hayate.Helpers;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;
using EndPoint = Hayate.Core.Instances.EndPoint;

namespace Hayate.Proxy.Containers
{
    public sealed class ContainerProxyMiddlewareTest : IDisposable
    {
        public ContainerProxyMiddlewareTest()
        {
            _mockCallback = new Mock<IHandlerCallback>(MockBehavior.Strict);
            _target = new ContainerProxyMiddleware(new EndPoint("http://localhost:1234/"),
                new Handler(_mockCallback.Object), ErrorHandler);
        }

        public void Dispose()
        {
            _mockCallback.VerifyAll();
        }

        private static Task ErrorHandler(HttpContext context)
        {
            return context.Response.WriteAsync("Waiting connected...");
        }

        private readonly ContainerProxyMiddleware _target;
        private readonly Mock<IHandlerCallback> _mockCallback;
        private readonly Uri _uri = new Uri("http://localhost:1234/abc");


        // ReSharper disable once MemberCanBePrivate.Global
        public interface IHandlerCallback
        {
            Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
        }

        private sealed class Handler : HttpClientHandler
        {
            private readonly IHandlerCallback _callback;

            public Handler(IHandlerCallback callback)
            {
                _callback = callback;
            }

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                CancellationToken cancellationToken)
            {
                return _callback.SendAsync(request);
            }
        }

        private static HttpResponseMessage Response(HttpStatusCode statusCode, string body)
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(body),
                StatusCode = statusCode
            };
        }

        private static HttpContext Context(string method, Uri uri)
        {
            var context = new DefaultHttpContext();
            context.Request.Host = new HostString($"{uri.Host}:{uri.Port}");
            context.Request.Scheme = uri.Scheme;
            context.Request.Path = uri.PathAndQuery;
            context.Request.Method = method;
            context.Response.Body = new MemoryStream();
            return context;
        }

        [Fact]
        public async Task Invoke()
        {
            _mockCallback.Setup(x => x.SendAsync(It.Is<HttpRequestMessage>(r => r.RequestUri == _uri)))
                .ReturnsAsync(Response(HttpStatusCode.OK, "hello"));

            var context = Context("GET", _uri);

            await _target.Invoke(context);

            var result = context.Response;

            Assert.Equal((int) HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("hello", result.Body.ReadAsString());
        }

        [Fact]
        public async Task InvokeButNotConnect()
        {
            _mockCallback.Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ThrowsAsync(new HttpRequestException());

            var context = Context("GET", _uri);

            await _target.Invoke(context);

            var result = context.Response;

            Assert.Equal((int) HttpStatusCode.OK, result.StatusCode);
            Assert.Equal("Waiting connected...", result.Body.ReadAsString());
        }

        [Fact]
        public async Task InvokeRedirectAction()
        {
            _mockCallback.Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>()))
                .ReturnsAsync(Helpers.Response.Redirect("http://localhost:9999/abc"));

            var context = Context("GET", _uri);

            await _target.Invoke(context);

            var result = context.Response;

            Assert.Equal((int) HttpStatusCode.Redirect, result.StatusCode);
            Assert.Equal("http://localhost:9999/abc", result.Headers["Location"]);
        }
    }
}