﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Helpers;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class HtmlPathResponseTranslatorTest : ResponseTranslatorTestBase
    {
        public HtmlPathResponseTranslatorTest()
        {
            _target = new HtmlPathResponseTranslator();
        }

        private readonly HtmlPathResponseTranslator _target;

        [Fact]
        public async Task TranslateAsyncAction()
        {
            var response = Response.Html("<form action='/users'>");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal("<form action='/_/proxy/app1/users'>", result);
        }

        [Fact]
        public async Task TranslateAsyncCopyHeaders()
        {
            var response = Response.Html("");
            response.Content.Headers.ContentLanguage.Add("ja");

            await _target.TranslateAsync(response, Original, Destination);

            var result = response.Content.Headers.ContentLanguage.ToArray();

            Assert.Equal(new[] {"ja"}, result);
        }

        [Fact]
        public async Task TranslateAsyncDoubleQuote()
        {
            var response = Response.Html("href=\"/abc/def\"");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal("href=\"/_/proxy/app1/abc/def\"", result);
        }

        [Fact]
        public async Task TranslateAsyncGZip()
        {
            var response = Response.GZipHtml("<a href='/abc'>abc</a>");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await GUnzipAsync(response);

            Assert.Equal("<a href='/_/proxy/app1/abc'>abc</a>", result);
        }

        [Fact]
        public async Task TranslateAsyncHref()
        {
            var response = Response.Html("<a href='/abc'>abc</a>");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal("<a href='/_/proxy/app1/abc'>abc</a>", result);
        }

        [Fact]
        public async Task TranslateAsyncNotModified()
        {
            var response = new HttpResponseMessage
            {
                Content = new StringContent(""),
                StatusCode = HttpStatusCode.NotModified
            };
            response.Content.Headers.ContentType = null;

            // Content-Type ヘッダーがなくても落ちないかの確認
            await _target.TranslateAsync(response, Original, Destination);
        }

        [Fact]
        public async Task TranslateAsyncSingleQuote()
        {
            var response = Response.Html("href='/abc/def'");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal("href='/_/proxy/app1/abc/def'", result);
        }

        [Fact]
        public async Task TranslateAsyncSrc()
        {
            var response = Response.Html("<script src='/abc.js'></script>");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal("<script src='/_/proxy/app1/abc.js'></script>", result);
        }
    }
}