﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Helpers;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class DefaultResponseTranslatorTest : ResponseTranslatorTestBase
    {
        public DefaultResponseTranslatorTest()
        {
            _target = ResponseTranslator.Default;
        }

        private readonly ResponseTranslator _target;

        [Fact]
        public async Task TranslateAsync()
        {
            var response = new ResponseBuilder()
                .GZippedContent("<a href='/abc'><img src=\"http://gitlab.example.com:8000/favicon.ico\">")
                .Location("http://gitlab.example.com:8000/users/sign_in")
                .SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; path=/users; HttpOnly")
                .Build();

            await _target.TranslateAsync(response, Original, Destination);

            Assert.Equal("<a href='/_/proxy/app1/abc'><img src=\"http://localhost:5000/_/proxy/app1/favicon.ico\">",
                await GUnzipAsync(response));
            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; path=/_/proxy/app1/users; HttpOnly",
                GetSetCookieHeader(response));
            Assert.Equal("http://localhost:5000/_/proxy/app1/users/sign_in", response.Headers.Location.ToString());
        }

        [Fact]
        public async Task TranslateAsyncCookiePath()
        {
            var response = Response.SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; path=/; HttpOnly");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; path=/_/proxy/app1/; HttpOnly", result);
        }

        [Fact]
        public async Task TranslateAsyncHtmlPath()
        {
            var response = Response.Html("<a href='/foo'>foo</a>");

            await _target.TranslateAsync(response, Original, Destination);

            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal(@"<a href='/_/proxy/app1/foo'>foo</a>", result);
        }

        [Fact]
        public async Task TranslateAsyncLocationHeader()
        {
            var response = Response.Location("http://gitlab.example.com:8000/users/sign_in");

            await _target.TranslateAsync(response, Original, Destination);

            var result = response.Headers.Location;

            Assert.Equal(new Uri("http://localhost:5000/_/proxy/app1/users/sign_in"), result);
        }
    }
}