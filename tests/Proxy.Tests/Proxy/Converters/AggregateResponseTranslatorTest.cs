﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class AggregateResponseTranslatorTest : ResponseTranslatorTestBase
    {
        public AggregateResponseTranslatorTest()
        {
            _mocks = new[]
            {
                MockTranslator(),
                MockTranslator(),
                MockTranslator()
            };
        }

        private readonly Mock<ResponseTranslator>[] _mocks;
        private readonly HttpResponseMessage _response = new HttpResponseMessage();

        private static Mock<ResponseTranslator> MockTranslator()
        {
            return new Mock<ResponseTranslator>(MockBehavior.Strict);
        }

        private void SetUp(Mock<ResponseTranslator> mock)
        {
            mock.Setup(x => x.TranslateAsync(_response, Original, Destination))
                .Returns(Task.CompletedTask);
        }

        [Fact]
        public void InitializeNotNull()
        {
            Assert.Throws<ArgumentNullException>(() => new AggregateResponseTranslator(null));
        }

        [Fact]
        public async Task TranslateAsync()
        {
            SetUp(_mocks[0]);

            var target = new AggregateResponseTranslator(_mocks.Take(1).Select(x => x.Object));

            await target.TranslateAsync(_response, Original, Destination);
        }

        [Fact]
        public async Task TranslateAsyncEmptyTranslators()
        {
            var target = new AggregateResponseTranslator(new ResponseTranslator[] { });

            await target.TranslateAsync(_response, Original, Destination);
        }

        [Fact]
        public async Task TranslateAsyncMultipleTranslators()
        {
            foreach (var mock in _mocks) SetUp(mock);

            var target = _mocks.Select(x => x.Object).ToAggregate();

            await target.TranslateAsync(_response, Original, Destination);
        }

        private static async Task<HttpResponseMessage> GZipHtmlResposeAsync(string html)
        {
            var content =  await GZipHttpContentCreator.GZipFromAsync(html);
            content.Headers.ContentType = new MediaTypeHeaderValue("text/html"){CharSet = "utf-8"};
            content.Headers.ContentEncoding.Add("gzip");
            return new HttpResponseMessage { Content = content};
        }
    }
}