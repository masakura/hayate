﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class HttpContentCreatorTest
    {
        private static StringContent Content(string contentType)
        {
            var content = new StringContent("hello");
            content.Headers.ContentType = MediaTypeHeaderValue.Parse(contentType);
            return content;
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        private sealed class TargetHttpContentCreator : HttpContentCreator
        {
            protected override Task<HttpContent> CreateHttpContentAsync(string body, Encoding encoding)
            {
                throw new InvalidOperationException();
            }

            protected override Task<string> ReadStringAsync(HttpContent content, Encoding encoding)
            {
                throw new InvalidOperationException();
            }

            public new static Encoding GetEncoding(HttpContent content)
            {
                return HttpContentCreator.GetEncoding(content);
            }
        }

        [Fact]
        public void GetEncoding()
        {
            var content = Content("text/html; charset=utf-8");

            var result = TargetHttpContentCreator.GetEncoding(content);

            Assert.Equal(Encoding.UTF8, result);
        }

        [Fact]
        public void GetEncodingNoCharSet()
        {
            var content = Content("text/html");

            var result = TargetHttpContentCreator.GetEncoding(content);

            Assert.Equal(Encoding.UTF8, result);
        }

        [Fact]
        public void GetEncodingUsAscii()
        {
            var content = Content("text/html; charset=us-ascii");

            var result = TargetHttpContentCreator.GetEncoding(content);

            Assert.Equal(Encoding.ASCII, result);
        }
    }
}