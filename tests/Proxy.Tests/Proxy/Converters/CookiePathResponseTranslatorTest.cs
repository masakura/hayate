﻿using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Helpers;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public class CookiePathResponseTranslatorTest : ResponseTranslatorTestBase
    {
        public CookiePathResponseTranslatorTest()
        {
            _target = new CookiePathResponseTranslator();
        }

        private readonly CookiePathResponseTranslator _target;

        [Fact]
        public async Task TranslateAsync()
        {
            var response = Response.SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; path=/; HttpOnly");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; path=/_/proxy/app1/; HttpOnly", result);
        }

        [Fact]
        public async Task TranslateAsyncEndPath()
        {
            var response = Response.SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; path=/");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; path=/_/proxy/app1/", result);
        }

        [Fact]
        public async Task TranslateAsyncNoHeader()
        {
            var response = Response.Html("hello");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Null(result);
        }

        [Fact]
        public async Task TranslateAsyncNoPath()
        {
            var response = Response.SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; HttpOnly");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; HttpOnly", result);
        }

        [Fact]
        public async Task TranslateAsyncSubPath()
        {
            var response =
                Response.SetCookie("_gitlab_session=7e89230bccd36479f195761a85456649; path=/users; HttpOnly");

            await _target.TranslateAsync(response, Original, Destination);

            var result = GetSetCookieHeader(response);

            Assert.Equal("_gitlab_session=7e89230bccd36479f195761a85456649; path=/_/proxy/app1/users; HttpOnly",
                result);
        }
    }
}