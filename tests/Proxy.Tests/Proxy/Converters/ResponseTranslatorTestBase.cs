﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Helpers;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    public abstract class ResponseTranslatorTestBase
    {
        protected readonly EndPoint Destination = new EndPoint("http://gitlab.example.com:8000/");
        protected readonly EndPoint Original = new EndPoint("http://localhost:5000/_/proxy/app1/");

        protected static async Task<string> GUnzipAsync(HttpResponseMessage response)
        {
            return await GZipHttpContentCreator.GUnzipFromAsync(response.Content);
        }

        protected static string GetSetCookieHeader(HttpResponseMessage response)
        {
            if (!response.Headers.TryGetValues("Set-Cookie", out var values)) return null;

            return values.FirstOrDefault();
        }
    }
}