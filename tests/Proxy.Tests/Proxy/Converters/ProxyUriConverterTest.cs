﻿using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class ProxyUriConverterTest
    {
        public ProxyUriConverterTest()
        {
            _target = ProxyUriConverter.Instance;
        }

        private readonly ProxyUriConverter _target;
        private readonly EndPoint _original = new EndPoint("http://localhost:1234/_/proxy/abc1/");
        private readonly EndPoint _destination = new EndPoint("http://localhost:8323/");

        [Fact]
        public void ConvertIncludePathAndQuery()
        {
            var result = _target.Convert("http://localhost:8323/abc?a=b", _original, _destination);

            Assert.Equal("http://localhost:1234/_/proxy/abc1/abc?a=b", result);
        }

        [Fact]
        public void ConvertPathOnlyPathAndQuery()
        {
            var result = _target.Convert("/abc?a=b", _original, _destination);

            Assert.Equal("/_/proxy/abc1/abc?a=b", result);
        }

        [Fact]
        public void ConvertPathOnlySimple()
        {
            var result = _target.Convert("/", _original, _destination);

            Assert.Equal("/_/proxy/abc1/", result);
        }

        [Fact]
        public void ConvertSimple()
        {
            var result = _target.Convert("http://localhost:8323/", _original, _destination);

            Assert.Equal("http://localhost:1234/_/proxy/abc1/", result);
        }

        [Fact]
        public void NotConvert()
        {
            var result = _target.Convert("sample", _original, _destination);

            Assert.Equal("sample", result);
        }
    }
}