﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Xunit;

namespace Hayate.Proxy.Converters
{
    public sealed class LocationHeaderResponseTranslatorTest
    {
        public LocationHeaderResponseTranslatorTest()
        {
            _target = new LocationHeaderResponseTranslator();
        }

        private readonly EndPoint _original = new EndPoint("http://localhost:5000/_/proxy/app1/");
        private readonly EndPoint _destination = new EndPoint("http://gitlab.example.com:8000/");
        private readonly LocationHeaderResponseTranslator _target;

        private static HttpResponseMessage Response(string location)
        {
            var response = new HttpResponseMessage
            {
                Headers =
                {
                    {"Location", location}
                }
            };
            return response;
        }

        [Fact]
        public async Task TranslateAsync()
        {
            var response = Response("http://gitlab.example.com:8000/users/sign_in");

            await _target.TranslateAsync(response, _original, _destination);

            var result = response.Headers.Location;

            Assert.Equal(new Uri("http://localhost:5000/_/proxy/app1/users/sign_in"), result);
        }

        [Fact]
        public async Task TranslateAsyncNoReplaceIfAnotherTarget()
        {
            const string uri = "http://gitlab.example.net:8000/users/sign_in";
            var response = Response(uri);

            await _target.TranslateAsync(response, _original, _destination);

            var result = response.Headers.Location;

            Assert.Equal(new Uri(uri), result);
        }

        [Fact]
        public async Task TranslateAsyncNoReplaceIfIncludeTarget()
        {
            const string uri = "http://example.com/http://gitlab.example.com:8000/users/sign_in";
            var response = Response(uri);

            await _target.TranslateAsync(response, _original, _destination);

            var result = response.Headers.Location;

            Assert.Equal(new Uri(uri), result);
        }

        [Fact]
        public async Task TranslateAsyncNoReplaceIfNoLocation()
        {
            var response = new HttpResponseMessage();

            await _target.TranslateAsync(response, _original, _destination);

            Assert.Null(response.Headers.Location);
        }
    }
}