﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy.Applications.Adapter;
using Moq;
using Xunit;

namespace Hayate.Proxy.Applications
{
    public sealed class ApplicationStatusMiddlewareRegistryTest : IDisposable
    {
        public ApplicationStatusMiddlewareRegistryTest()
        {
            _application = new Application();
            _mockApplication = new Mock<IApplication>(MockBehavior.Strict);
            _target = ApplicationStatusMiddlewareRegistry.Create();
        }

        public void Dispose()
        {
            _mockApplication.VerifyAll();
        }

        private readonly ApplicationStatusMiddlewareRegistry _target;
        private readonly Mock<IApplication> _mockApplication;
        private readonly Application _application;

        private void SetStatus(InstanceStatus status)
        {
            _application.Status = status;
            _application.Status2 = InstanceStatusAdapter.Create(status);
        }

        private async Task<ApplicationStatusMiddleware> FindMiddlewareAsync()
        {
            var middleware = (ApplicationStatusMiddleware) await _target.FindAsync(_application);
            return middleware;
        }

        [Fact]
        public async Task FindAsync()
        {
            SetStatus(InstanceStatus.Created);

            var middleware = await FindMiddlewareAsync();
            var result = middleware.Status;

            Assert.Equal(InstanceStatusAdapter.Create(InstanceStatus.Created), result);
        }

        [Fact]
        public async Task FindAsyncCached()
        {
            SetStatus(InstanceStatus.Paused);

            var one = await FindMiddlewareAsync();
            var two = await FindMiddlewareAsync();

            Assert.Same(one, two);
        }
    }
}