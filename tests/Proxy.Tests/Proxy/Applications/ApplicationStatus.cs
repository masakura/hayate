﻿namespace Hayate.Proxy.Applications
{
    internal sealed class ApplicationStatus : IApplicationStatus
    {
        public string Message { get; set; }
    }
}