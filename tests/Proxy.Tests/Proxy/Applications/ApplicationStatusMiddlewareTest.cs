﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace Hayate.Proxy.Applications
{
    public sealed class ApplicationStatusMiddlewareTest : IDisposable
    {
        private readonly Mock<ICallback> _mockHandler;
        private readonly HttpContext _context;

        public ApplicationStatusMiddlewareTest()
        {
            _mockHandler = new Mock<ICallback>(MockBehavior.Strict);
            _context = new Mock<HttpContext>().Object;
        }

        public void Dispose()
        {
            _mockHandler.VerifyAll();
        }

        private ApplicationStatusMiddleware NewTarget(IApplicationStatus status)
        {
            return new ApplicationStatusMiddleware(status, _mockHandler.Object.HandleAsync);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public interface ICallback
        {
            Task HandleAsync(HttpContext context, IApplicationStatus status);
        }

        [Fact]
        public async Task Invoke()
        {
            _mockHandler.Setup(x => x.HandleAsync(_context, It.Is<IApplicationStatus>(s => s.Message == "hello")))
                .Returns(Task.CompletedTask);
            
            var target = NewTarget(new ApplicationStatus() {Message = "hello"});

            await target.Invoke(_context);
        }
    }
}