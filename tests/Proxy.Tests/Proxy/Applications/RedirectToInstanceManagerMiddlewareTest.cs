﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace Hayate.Proxy.Applications
{
    public sealed class RedirectToInstanceManagerMiddlewareRegistryTest
    {
        private readonly RedirectToEnvironmentManagerMiddlewareRegistry _target;
        private readonly Application _application;

        public RedirectToInstanceManagerMiddlewareRegistryTest()
        {
            _target = RedirectToEnvironmentManagerMiddlewareRegistry.Create();

            _application = new Application
            {
                InstanceId = new InstanceId("hoge:v1.0")
            };
        }

        [Fact]
        public async Task FindAsync()
        {
            _application.RequireStart = true;

            var result = (RedirectToEnvironmentManagerMiddleware) await _target.FindAsync(_application);
            
            Assert.Equal(new InstanceId("hoge:v1.0"), result.InstanceId);
        }

        [Fact]
        public async Task FindAsyncNotRequireStart()
        {
            _application.RequireStart = false;

            var result = await _target.FindAsync(_application);
            
            Assert.Null(result);
        }
    }
    public sealed class RedirectToInstanceManagerMiddlewareTest
    {
        private readonly RedirectToEnvironmentManagerMiddleware _target;

        public RedirectToInstanceManagerMiddlewareTest()
        {
            _target = new RedirectToEnvironmentManagerMiddleware(new InstanceId("id-to-instance"));
        }

        [Fact]
        public void Invoke()
        {
            var context = new DefaultHttpContext();

            _target.Invoke(context);

            Assert.Equal("/Redirect/FromInstanceId?instanceId=id-to-instance",
                context.Response.Headers["Location"]);
            Assert.Equal(302, context.Response.StatusCode);
        }
    }
}