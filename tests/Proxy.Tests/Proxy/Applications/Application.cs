﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Proxy.Applications
{
    internal sealed class Application : IApplication
    {
        // ReSharper disable MemberCanBePrivate.Global
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public InstanceStatus Status { get; set; }
        public IApplicationStatus Status2 { get; set; }
        public EndPoint EndPoint { get; set; }
        public bool RequireStart { get; set; }
        public InstanceId InstanceId { get; set; }

        public Task<EndPoint> EndPointAsync()
        {
            return Task.FromResult(EndPoint);
        }

        public Task<IApplicationStatus> StatusAsync()
        {
            return Task.FromResult(Status2);
        }

        public Task<bool> RequireStartAsync()
        {
            return Task.FromResult(RequireStart);
        }
    }
}