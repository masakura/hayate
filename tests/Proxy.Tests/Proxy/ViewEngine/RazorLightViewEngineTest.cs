﻿using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace Hayate.Proxy.ViewEngine
{
    public sealed class RazorLightViewEngineTest
    {
        public RazorLightViewEngineTest()
        {
            var uri = new Uri(typeof(RazorLightViewEngineTest).Assembly.CodeBase);
            var path = Path.GetDirectoryName(Uri.UnescapeDataString(uri.AbsolutePath));
            path = Path.Combine(path, "./Proxy/ViewEngine/Views");

            _target = new RazorLightViewEngine(path);
        }

        private readonly RazorLightViewEngine _target;

        // ReSharper disable once MemberCanBePrivate.Global
        public sealed class Model
        {
            public Model(string value)
            {
                Value = value;
            }

            // ReSharper disable once MemberCanBePrivate.Global
            // ReSharper disable once UnusedAutoPropertyAccessor.Global
            public string Value { get; }
        }

        [Fact]
        public async Task CompileRenderAsync()
        {
            var result = await _target.CompileRenderAsync("Razor.cshtml");

            Assert.Equal("Hello, world!", result);
        }

        [Fact]
        public async Task CompileRenderAsyncModel()
        {
            var result = await _target.CompileRenderAsync("RazorModel.cshtml", new Model("Hello"));

            Assert.Equal("Hello, world!", result);
        }
    }
}