﻿using System.Collections.Generic;
using Hayate.Provider.DockerRegistry;
using Newtonsoft.Json;

namespace Hayate.Provider.Utility
{
    public sealed class TagsListJson : JsonObjectBase<TagsListResponse>
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("tags")] public IEnumerable<string> Tags { get; set; }
    }
}