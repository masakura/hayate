﻿using Hayate.Provider.GitLab;
using Newtonsoft.Json;

namespace Hayate.Provider.Utility
{
    public sealed class ProjectJsonObject: JsonObjectBase<GitLabProject>
    {
        [JsonProperty("id")] public long Id { get; set; }
        [JsonProperty("path_with_namespace")] public string PathWithNamespace { get; set; }
        [JsonProperty("name_with_namespace")] public string NameWithNamespace { get; set; }
        [JsonProperty("web_url")] public string WebUrl { get; set; }
    }
}