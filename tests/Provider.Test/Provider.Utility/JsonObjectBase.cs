﻿using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace Hayate.Provider.Utility
{
    public abstract class JsonObjectBase<T>
    {
        public T ToJsonObject()
        {
            return JToken.FromObject(this).ToObject<T>();
        }

        public override string ToString()
        {
            return JToken.FromObject(this).ToString();
        }

        public HttpResponseMessage ToJsonReponse()
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(this.ToString())
            };
        }
    }
}