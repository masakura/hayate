﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class BearerPartsTest
    {
        public BearerPartsTest()
        {
            _target = new BearerParts(WwwAuthenticateParameter);
        }

        private const string WwwAuthenticateParameter =
            @"realm=""https://auth.docker.io/token"",service=""registry.docker.io"",scope=""repository:library/hello-world:pull";

        private readonly BearerParts _target;

        private readonly Uri _authenticateUri =
            new Uri(
                "https://auth.docker.io/token?service=registry.docker.io&scope=repository:library/hello-world:pull");

        private static AuthenticationHeaderValue Bearer(string parameter)
        {
            return new AuthenticationHeaderValue(nameof(Bearer), parameter);
        }

        private static AuthenticationHeaderValue Basic(string parameter)
        {
            return new AuthenticationHeaderValue(nameof(Basic), parameter);
        }

        private static HttpResponseMessage Response(params AuthenticationHeaderValue[] parameters)
        {
            var response = new HttpResponseMessage();
            foreach (var parameter in parameters) response.Headers.WwwAuthenticate.Add(parameter);

            return response;
        }

        [Fact]
        public void From()
        {
            var response = Response(Bearer(WwwAuthenticateParameter));

            var result = BearerParts.From(response);

            Assert.Equal(_authenticateUri, result.ToUri());
        }

        [Fact]
        public void FromNoBearer()
        {
            var response = Response(Basic("foo:pass"));

            var result = BearerParts.From(response);

            Assert.Null(result);
        }

        [Fact]
        public void FromNoWwwAuthenticate()
        {
            var response = new HttpResponseMessage();

            var result = BearerParts.From(response);

            Assert.Null(result);
        }

        [Fact]
        public void FromWwwAuthentications()
        {
            var response = Response(Basic("foo:pass"), Bearer(WwwAuthenticateParameter));

            var result = BearerParts.From(response);

            Assert.Equal(_authenticateUri, result.ToUri());
        }

        [Fact]
        public void NoScope()
        {
            const string s = @"realm=""http://gitlab.hayate.example.jp/jwt/auth"",service=""container_registry""";
            var target = new BearerParts(s);
            Assert.Equal(new Uri("http://gitlab.hayate.example.jp/jwt/auth?service=container_registry"),
                target.ToUri());
        }

        [Fact]
        public void TestBearerParts()
        {
            Assert.Equal("https://auth.docker.io/token", _target.Realm);
        }

        [Fact]
        public void ToUri()
        {
            var result = _target.ToUri();

            Assert.Equal(_authenticateUri, result);
        }
    }
}
