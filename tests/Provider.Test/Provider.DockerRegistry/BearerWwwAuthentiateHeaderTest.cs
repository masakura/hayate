﻿using System;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class BearerWwwAuthentiateHeaderTest
    {
        [Fact]
        public void Parse()
        {
            var result = BearerWwwAuthentiateHeader.Parse(
                @"Bearer realm=""http://gitlab.example.com/jwt/auth"",service=""container_registry"",scope=""repository:hayate/project1:pull""");

            Assert.Equal("Bearer", result.Scheme);
            Assert.Equal("http://gitlab.example.com/jwt/auth", result.Realm);
            Assert.Equal("container_registry", result.Service);
            Assert.Equal("repository:hayate/project1:pull", result.Scope);
        }
    }
}
