﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class BearerTest
    {
        public BearerTest()
        {
            var value = BearerWwwAuthentiateHeader.Parse(
                @"Bearer realm=""http://gitlab.example.com/jwt/auth"",service=""container_registry"",scope=""repository:hayate/project1:pull""");
            _target = new Bearer(value);

            _requestUri = new Uri(
                "http://gitlab.example.com/jwt/auth?service=container_registry&scope=repository:hayate/project1:pull");
        }

        private readonly Uri _requestUri;
        private readonly Bearer _target;

        private static HttpResponseMessage TokenResponse()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(@"{""token"":""tokenstring""}")
            };
        }

        [Fact]
        public void GetRequestUri()
        {
            var result = _target.GetRequestUri();

            Assert.Equal(_requestUri, result);
        }

        [Fact]
        public async Task GetTokenAsync()
        {
            var mockHandler = new Mock<IHttpMessageHandler>(MockBehavior.Strict);
            mockHandler.Setup(x =>
                    x.SendAsync(
                        It.Is<HttpRequestMessage>(p => p.RequestUri == _requestUri &&
                                                       p.Headers.Authorization.Scheme == "Basic" &&
                                                       p.Headers.Authorization.Parameter == "aGF5YXRlOnBhc3N3b3Jk"),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(TokenResponse());

            var result = await _target.GetTokenAsync("password", new HttpMessageHandlerWrapper(mockHandler.Object));

            Assert.Equal("tokenstring", result);
        }
    }
}
