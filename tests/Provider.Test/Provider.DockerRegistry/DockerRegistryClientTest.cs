﻿using System;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Moq;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class DockerRegistryClientTest
    {
        public DockerRegistryClientTest()
        {
            _mock = new Mock<IDockerRegistryPasswordCredentialStore>(MockBehavior.Strict);
            var client = new DockerRegistryPasswordCredentialHttpClient(_mock.Object);
            _target = new DockerRegistryClient(new Uri("https://registry.hub.docker.com"), client);
        }

        private readonly DockerRegistryClient _target;
        private readonly Mock<IDockerRegistryPasswordCredentialStore> _mock;

        [Fact]
        public async Task TagsAsync()
        {
            _mock.Setup(x => x.GetCurrentUserCredentialAsync())
                .ReturnsAsync(DockerRegistryPasswordCredential.Empty);

            var result = await _target.TagsAsync(new DockerRepositoryName("library/hello-world"));

            Assert.Contains(new DockerTagName("linux"), result.Tags);
        }

        [Fact]
        public async Task TagsAynscRequireAuthenticate()
        {
            _mock.Setup(x => x.GetCurrentUserCredentialAsync())
                .ReturnsAsync(DockerRegistryPasswordCredential.Empty);

            var result = await _target.TagsAsync(new DockerRepositoryName("library/hello-world"));

            Assert.Contains(new DockerTagName("linux"), result.Tags);
        }

        [Fact]
        public async Task VerifyAccessAsync()
        {
            _mock.Setup(x => x.GetCurrentUserCredentialAsync())
                .ReturnsAsync(DockerRegistryPasswordCredential.Empty);

            var result = await _target.VerifyAccessAsync();

            Assert.True(result);
        }
    }
}
