﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Containers.Docker;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class TagsListResponseTest
    {
        private readonly string _json;

        public TagsListResponseTest()
        {
            _json = @"{""name"":""masakura/masakura1"",""tags"":[""nginx"",""latest""]}";
        }

        [Fact]
        public void ParseFromJson()
        {
            var result = JToken.Parse(_json).ToObject<TagsListResponse>();
            
            Assert.Equal(Repository("masakura/masakura1"), result.Name);
            Assert.Equal(Tags("nginx", "latest"), result.Tags.ToArray());
        }

        private static IEnumerable<DockerTagName> Tags(params string[] tags)
        {
            return tags.Select(tag => new DockerTagName(tag)).ToArray();
        }

        private static DockerRepositoryName Repository(string name)
        {
            return new DockerRepositoryName(name);
        }
    }
}