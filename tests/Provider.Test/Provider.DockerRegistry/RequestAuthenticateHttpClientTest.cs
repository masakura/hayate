﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Http;
using Moq;
using Xunit;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class RequestAuthenticateHttpClientTest
    {
        public RequestAuthenticateHttpClientTest()
        {
            _okResponse = new HttpResponseMessage {StatusCode = HttpStatusCode.OK};
            _forbiddenResponse = new HttpResponseMessage {StatusCode = HttpStatusCode.Forbidden};

            _mock = new Mock<IHttpClient>(MockBehavior.Strict);
            _target = new RequestAuthenticateHttpClient(_mock.Object);
        }

        private readonly Mock<IHttpClient> _mock;
        private readonly HttpResponseMessage _okResponse;
        private readonly RequestAuthenticateHttpClient _target;
        private readonly HttpResponseMessage _forbiddenResponse;

        [Fact]
        public async Task SendAsyncForbidden()
        {
            _mock.Setup(x => x.GetAsync("uri")).ReturnsAsync(_forbiddenResponse);

            await Assert.ThrowsAsync<RequireAuthenticateException>(() => _target.GetAsync("uri"));
        }

        [Fact]
        public async Task SendAsyncOk()
        {
            _mock.Setup(x => x.GetAsync("uri")).ReturnsAsync(_okResponse);

            var result = await _target.GetAsync("uri");

            Assert.Equal(_okResponse, result);
        }
    }
}
