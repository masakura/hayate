﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Hayate.Provider.DockerRegistry
{
    internal sealed class HttpMessageHandlerWrapper : HttpMessageHandler
    {
        private readonly IHttpMessageHandler _handler;

        public HttpMessageHandlerWrapper(IHttpMessageHandler handler)
        {
            _handler = handler;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return await _handler.SendAsync(request, cancellationToken);
        }
    }
}
