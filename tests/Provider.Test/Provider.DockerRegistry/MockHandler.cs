﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;

namespace Hayate.Provider.DockerRegistry
{
    internal sealed class MockHandler : IHttpMessageHandler
    {
        public static Uri RequestUri = new Uri("http://registry.example.com/v2/hayate%2fproject1/tags/list");
        private readonly Uri _baseAddress;
        private readonly Mock<IHttpMessageHandler> _mock;

        public MockHandler(string baseAddress = "http://registry.example.com/v2/")
        {
            _baseAddress = new Uri(baseAddress);
            _mock = new Mock<IHttpMessageHandler>(MockBehavior.Strict);
        }

        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return _mock.Object.SendAsync(request, cancellationToken);
        }

        public void SetupPublic(string requestUri, string responseText)
        {
            _mock.Setup(x =>
                    x.SendAsync(
                        It.Is<HttpRequestMessage>(p => p.RequestUri == Relative(requestUri)),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(ResponseData(responseText));
        }

        public void SetupUnauthorized(string requestUri)
        {
            _mock.Setup(x => x.SendAsync(It.Is<HttpRequestMessage>(p => p.RequestUri == Relative(requestUri)),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(Unauthorized());
        }

        public void SetupJwtAuth(string password)
        {
            throw new NotImplementedException();
        }

        private HttpResponseMessage  Unauthorized()
        {
            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.Headers.Add("WWW-Authenticate",
                @"Bearer realm=""http://gitlab.example.com/jwt/auth"",service=""container_registry"",scope=""repository:hayate/project1:pull""");
            return response;
        }

        private Uri Relative(string requestUri)
        {
            return new Uri(_baseAddress, requestUri);
        }

        private HttpResponseMessage ResponseData(string responseText)
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(responseText)
            };
        }
    }
}
