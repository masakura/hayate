﻿using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabNamespaceNameTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabNamespaceName("Group / Sub");
            
            Assert.Equal("Group / Sub", result.ToString());
        }
    }
}