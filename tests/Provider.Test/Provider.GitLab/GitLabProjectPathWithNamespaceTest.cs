﻿using Hayate.Containers.Docker;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectPathWithNamespaceTest
    {
        private const string Json = @"{""PathWithNamespace"":""group/project""}";

        private static GitLabProjectPath Path(string value)
        {
            return new GitLabProjectPath(value);
        }

        private static GitLabNamespacePath Namespace(string value)
        {
            return new GitLabNamespacePath(value);
        }

        [Theory]
        [InlineData("group/subgroup/project", "group/subgroup", "project")]
        [InlineData("group1/project1", "group1", "project1")]
        public void Parse(string pathWithNamespace, string @namespace, string path)
        {
            var result = GitLabProjectPathWithNamespace.Parse(pathWithNamespace);

            Assert.Equal(@namespace, result.Namespace.ToString());
            Assert.Equal(path, result.Project.ToString());
        }

        [Fact]
        public void ConvertFromJson()
        {
            var result = JToken.Parse(Json).ToObject<Container>();

            Assert.Equal("group", result.PathWithNamespace.Namespace.ToString());
            Assert.Equal("project", result.PathWithNamespace.Project.ToString());
        }

        [Fact]
        public void ConvertFromJsonNoMember()
        {
            var result = JToken.Parse("{}").ToObject<Container>();

            Assert.Equal(default(GitLabProjectPathWithNamespace), result.PathWithNamespace);
        }

        [Fact]
        public void Empty()
        {
            var result = new GitLabProjectPathWithNamespace();

            Assert.Equal("", result.ToString());
        }

        [Fact]
        public void ParseEmpty()
        {
            var result = GitLabProjectPathWithNamespace.Parse(string.Empty);

            Assert.Null(result.Namespace.ToString());
            Assert.Null(result.Project.ToString());
        }

        [Fact]
        public void ParseNotContainsSlash()
        {
            var result = GitLabProjectPathWithNamespace.Parse("abc");

            Assert.Null(result.Namespace.ToString());
            Assert.Null(result.Project.ToString());
        }

        [Fact]
        public void ParseNull()
        {
            var result = GitLabProjectPathWithNamespace.Parse(null);

            Assert.Null(result.Namespace.ToString());
            Assert.Null(result.Project.ToString());
        }

        [Fact]
        public void ToRepositoryName()
        {
            // TODO 大文字小文字変換を行う。GitLab のプロジェクトパスと、イメージリポジトリ名をマッピングさせる。
            var result = GitLabProjectPathWithNamespace.Parse("group/subgroup/project");

            Assert.Equal(new DockerRepositoryName("group/subgroup/project"), result.ToRepositoryName());
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabProjectPathWithNamespace(Namespace("group/subgroup"), Path("project"));

            Assert.Equal("group/subgroup/project", result.ToString());
        }
    }
}