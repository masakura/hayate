﻿using Newtonsoft.Json.Linq;
using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectNameWithNamespaceTest
    {
        private const string Json = @"{""NameWithNamespace"":""Group / Project""}";

        private static GitLabProjectNameWithNamespace NewTarget(string @namespace, string project)
        {
            return new GitLabProjectNameWithNamespace(Namespace(@namespace), Project(project));
        }

        private static GitLabNamespaceName Namespace(string @namespace)
        {
            return new GitLabNamespaceName(@namespace);
        }

        private static GitLabProjectName Project(string project)
        {
            return new GitLabProjectName(project);
        }

        [Fact]
        public void ConvertFromJson()
        {
            var result = JToken.Parse(Json).ToObject<Container>();

            Assert.Equal("Group", result.NameWithNamespace.Namespace.ToString());
            Assert.Equal("Project", result.NameWithNamespace.Project.ToString());
        }

        [Fact]
        public void ConvertFromJsonNoMember()
        {
            var result = JToken.Parse("{}").ToObject<Container>();
            
            Assert.Equal(default(GitLabProjectNameWithNamespace),  result.NameWithNamespace);
        }

        [Fact]
        public void Parse()
        {
            var result = GitLabProjectNameWithNamespace.Parse("Group / Sub / Project");

            Assert.Equal(Namespace("Group / Sub"), result.Namespace);
            Assert.Equal(Project("Project"), result.Project);
            Assert.Equal("Group / Sub / Project", result.ToString());
        }

        [Fact]
        public void Properties()
        {
            var result = NewTarget("Group / Sub", "Project");

            Assert.Equal(Namespace("Group / Sub"), result.Namespace);
            Assert.Equal(Project("Project"), result.Project);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = NewTarget("Group / Sub", "Project");

            Assert.Equal("Group / Sub / Project", result.ToString());
        }
    }
}