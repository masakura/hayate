﻿namespace Hayate.Provider.GitLab
{
    // ReSharper disable UnusedAutoPropertyAccessor.Global
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Container
    {
        public GitLabProjectPathWithNamespace PathWithNamespace { get; set; }
        public GitLabProjectNameWithNamespace NameWithNamespace { get; set; }
        public GitLabProjectId Id { get; set; }
    }
}