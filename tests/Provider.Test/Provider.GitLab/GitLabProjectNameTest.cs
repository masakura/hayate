﻿using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectNameTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabProjectName("project");
            
            Assert.Equal("project", result.ToString());
        }
    }
}