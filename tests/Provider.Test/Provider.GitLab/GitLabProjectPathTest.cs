﻿using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectPathTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabProjectPath("hello");
            
            Assert.Equal("hello", result.ToString());
        }
    }
}