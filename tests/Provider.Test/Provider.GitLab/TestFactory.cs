﻿using System;

namespace Hayate.Provider.GitLab
{
    internal static class TestFactory
    {
        public static GitLabProject CreateProject(int id,
            string pathWithNamespace,
            string nameWithNamespace,
            Uri webUrl)
        {
            return new GitLabProject(new GitLabProjectId(id),
                GitLabProjectPathWithNamespace.Parse(pathWithNamespace),
                GitLabProjectNameWithNamespace.Parse(nameWithNamespace),
                webUrl);
        }
    }
}