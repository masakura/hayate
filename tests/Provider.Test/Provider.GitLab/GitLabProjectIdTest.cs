﻿using Newtonsoft.Json.Linq;
using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectIdTest
    {
        private const string Json = @"{""Id"":15}";

        [Fact]
        public void ConvertFromJson()
        {
            var result = JToken.Parse(Json).ToObject<Container>();

            Assert.Equal("15", result.Id.ToString());
        }

        [Fact]
        public void ConvertFromJsonNoMember()
        {
            var result = JToken.Parse("{}").ToObject<Container>();

            Assert.Equal(default(GitLabProjectId), result.Id);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabProjectId(15);

            Assert.Equal("15", result.ToString());
        }
    }
}