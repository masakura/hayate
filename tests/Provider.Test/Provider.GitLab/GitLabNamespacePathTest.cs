﻿using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabNamespacePathTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new GitLabNamespacePath("root/hoge");
            
            Assert.Equal("root/hoge", result.ToString());
        }
    }
}