﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectRepositoryTest
    {
        public GitLabProjectRepositoryTest()
        {
            _mock = new MockGitLabHttpClient();
            _project1 = _mock.Add(991, "g1/sg1/p1", "Group 1 / Sub group 1 / Project 1");
            _project2 = _mock.Add(992, "g1/sg1/p2", "Group 1 / Sub group 1 / Project 2");
            _project3 = _mock.Add(993, "g1/sg1/p3", "Group 1 / Sub group 1 / Project 3");

            _target = new GitLabProjectRepository(_mock);
        }

        private readonly GitLabProjectRepository _target;
        private readonly MockProject _project1;
        private readonly MockGitLabHttpClient _mock;
        private readonly MockProject _project2;
        private readonly MockProject _project3;

        [Fact]
        public async Task ListOwnedProjectsAsync()
        {
            var result = (await _target.ListProjectsAsync()).ToList();

            Assert.Equal(3, result.Count);

            Assert.Equal(result[0].Id, _project1.Id);
            Assert.Equal(result[1].Id, _project2.Id);
            Assert.Equal(result[2].Id, _project3.Id);
            
            Assert.Equal(result[2].PathWithNamespace, _project3.Path);
            Assert.Equal(result[2].NameWithNamespace, _project3.Name);
        }

        [Fact]
        public async Task ProjectAsync()
        {
            var result = await _target.ProjectAsync(_project1.Id);

            Assert.Equal(_project1.Id, result.Id);
            Assert.Equal(_project1.Path, result.PathWithNamespace);
            Assert.Equal(_project1.Name, result.NameWithNamespace);
            Assert.False(result.IsNotFound);
        }

        [Fact]
        public async Task ProjectAsyncInternalServerError()
        {
            _mock.AddInternalServerError(109);

            await Assert.ThrowsAsync<HttpRequestException>(async () =>
                await _target.ProjectAsync(new GitLabProjectId(109)));
        }

        [Fact]
        public async Task ProjectAsyncNotFound()
        {
            var result = await _target.ProjectAsync(new GitLabProjectId(99999));

            Assert.True(result.IsNotFound);
        }

        [Fact]
        public async Task ProjectAsyncWebUrl()
        {
            var result = await _target.ProjectAsync(_project1.Id);

            Assert.Equal(_project1.WebUrl, result.WebUrl);
            Assert.False(result.IsNotFound);
        }
    }
}