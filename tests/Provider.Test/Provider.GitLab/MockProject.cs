﻿using System;

namespace Hayate.Provider.GitLab
{
    internal sealed class MockProject
    {
        public MockProject(int id, string path, string name)
        {
            Id = new GitLabProjectId(id);
            Path = GitLabProjectPathWithNamespace.Parse(path);
            Name = GitLabProjectNameWithNamespace.Parse(name);
            WebUrl = new Uri($"http://gitlab.hayate.example.jp/{path}");
        }

        public GitLabProjectPathWithNamespace Path { get; }
        public GitLabProjectNameWithNamespace Name { get; }
        public GitLabProjectId Id { get; }
        public Uri WebUrl { get; }
    }
}