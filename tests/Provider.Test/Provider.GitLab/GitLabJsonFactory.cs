﻿using System.Collections.Generic;
using System.Linq;

namespace Hayate.Provider.GitLab
{
    internal static class GitLabJsonFactory
    {
        public static string ToJson(this IEnumerable<MockProject> projects)
        {
            var array = string.Join(",\n", projects.Select(p => p.ToJson()));
            return $"[\n{array}\n]";
        }

        public static string ToJson(this MockProject project)
        {
            return @"
{
  ""id"": ${id},
  ""description"": """",
  ""name"": ""Project 1"",
  ""name_with_namespace"": ""${name}"",
  ""path"": ""p1"",
  ""path_with_namespace"": ""${path}"",
  ""created_at"": ""2018-04-20T01:11:59.804Z"",
  ""default_branch"": null,
  ""tag_list"": [],
  ""ssh_url_to_repo"": ""git@gitlab.hayate.example.jp:g1/sg1/p1.git"",
  ""http_url_to_repo"": ""http://gitlab.hayate.example.jp/g1/sg1/p1.git"",
  ""web_url"": ""${webUrl}"",
  ""avatar_url"": null,
  ""star_count"": 0,
  ""forks_count"": 0,
  ""last_activity_at"": ""2018-04-20T01:11:59.804Z"",
  ""_links"": {
    ""self"": ""http://gitlab.hayate.example.jp/api/v4/projects/103"",
    ""issues"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/issues"",
    ""merge_requests"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/merge_requests"",
    ""repo_branches"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/repository/branches"",
    ""labels"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/labels"",
    ""events"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/events"",
    ""members"": ""http://gitlab.hayate.example.jp/api/v4/projects/103/members""
  },
  ""archived"": false,
  ""visibility"": ""private"",
  ""resolve_outdated_diff_discussions"": false,
  ""container_registry_enabled"": true,
  ""issues_enabled"": true,
  ""merge_requests_enabled"": true,
  ""wiki_enabled"": true,
  ""jobs_enabled"": true,
  ""snippets_enabled"": true,
  ""shared_runners_enabled"": true,
  ""lfs_enabled"": true,
  ""creator_id"": 2,
  ""namespace"": {
    ""id"": 4,
    ""name"": ""Sub group 1"",
    ""path"": ""sg1"",
    ""kind"": ""group"",
    ""full_path"": ""g1/sg1"",
    ""parent_id"": 3
  },
  ""import_status"": ""none"",
  ""import_error"": null,
  ""open_issues_count"": 0,
  ""runners_token"": ""S-rbvr2XHF8AGtfxC6Ks"",
  ""public_jobs"": true,
  ""ci_config_path"": null,
  ""shared_with_groups"": [],
  ""only_allow_merge_if_pipeline_succeeds"": false,
  ""request_access_enabled"": false,
  ""only_allow_merge_if_all_discussions_are_resolved"": false,
  ""printing_merge_request_link_enabled"": true,
  ""permissions"": {
    ""project_access"": null,
    ""group_access"": {
      ""access_level"": 50,
      ""notification_level"": 3
    }
  }
}
"
                .Replace("${id}", project.Id.ToString())
                .Replace("${path}", project.Path.ToString())
                .Replace("${name}", project.Name.ToString())
                .Replace("${webUrl}", project.WebUrl.ToString());
        }
    }
}