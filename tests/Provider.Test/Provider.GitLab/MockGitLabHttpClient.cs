﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;

namespace Hayate.Provider.GitLab
{
    internal sealed class MockGitLabHttpClient : IGitLabHttpClient
    {
        private readonly Mock<IGitLabHttpClient> _mock;
        private readonly IList<MockProject> _projects = new List<MockProject>();

        public MockGitLabHttpClient()
        {
            _mock = new Mock<IGitLabHttpClient>(MockBehavior.Strict);

            _mock.Setup(x => x.GetAsync(It.Is<string>(p => p.StartsWith("projects/"))))
                .ReturnsAsync(NotFoundResponse());

            _mock.Setup(x => x.GetAsync("projects?owned=true"))
                .ReturnsAsync(() => JsonResponse(_projects.ToJson()));
        }

        Task<HttpResponseMessage> IGitLabHttpClient.GetAsync(string requestUri)
        {
            return _mock.Object.GetAsync(requestUri);
        }

        public MockProject Add(int id, string path, string name)
        {
            var project = new MockProject(id, path, name);

            SetUpGetAsync(id, JsonResponse(project.ToJson()));

            _projects.Add(project);
            return project;
        }

        private void SetUpGetAsync(int id, HttpResponseMessage response)
        {
            _mock.Setup(x => x.GetAsync($"projects/{id.ToString()}"))
                .ReturnsAsync(response);
        }

        public void AddInternalServerError(int id)
        {
            SetUpGetAsync(id, InternalServerErrorResponse());
        }

        private static HttpResponseMessage JsonResponse(string json)
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(json)
            };
        }

        private static HttpResponseMessage NotFoundResponse()
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(@"{""message"": ""404 Project Not Found""}}")
            };
        }

        private static HttpResponseMessage InternalServerErrorResponse()
        {
            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.InternalServerError,
                Content = new StringContent(@"{""error"": ""has error""}")
            };
        }
    }
}
