﻿using System.Security.Claims;
using Hayate.Core.Users;
using Xunit;

namespace Hayate.Cure.Users
{
    public sealed class UserTest
    {
        public UserTest()
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, "userid1")
            });
            _principal1 = new ClaimsPrincipal(identity);
        }

        private readonly ClaimsPrincipal _principal1;

        [Fact]
        public void CreateFromAnonymouse()
        {
            var anonymouse = new ClaimsPrincipal(new ClaimsIdentity());

            var result = User.From(anonymouse);

            Assert.True(result.IsAnonymouse);
        }

        [Fact]
        public void CreateFromPrincipal()
        {
            var result = User.From(_principal1);

            Assert.Equal(new UserId("userid1"), result.UserId);
        }
    }
}
