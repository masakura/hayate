﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Core.Applications
{
    internal sealed class MockInstance : IInstance
    {
        private readonly Mock<IRegistryImage> _mockImage;
        private readonly StartRecorder _startRecorder = new StartRecorder();
        private InstanceStatus _status = InstanceStatus.Pending;

        public MockInstance(string imageName)
        {
            var dockerImageName = DockerImageName.Parse(imageName);
            EnvironmentName = new EnvironmentName(dockerImageName.Tag.ToString());
            Id = InstanceId.From(dockerImageName);
            Image = new RegistryImage(dockerImageName);

            Sha256Hash = Sha256(imageName);

            _mockImage = new Mock<IRegistryImage>(MockBehavior.Strict);
            _mockImage.SetupGet(x => x.ImageName).Returns(dockerImageName);
            _mockImage.SetupGet(x => x.Name).Returns(EnvironmentName);
        }

        public EnvironmentName EnvironmentName { get; }
        public string Sha256Hash { get; }
        public IRegistryImage Image { get; }
        public string ContainerId => _startRecorder.Message();
        public InstanceId Id { get; }
        public bool CanRestart { get; private set; }
        public bool CanTryUpgrade { get; private set; }

        public bool CanTransit(InstanceTransition transition)
        {
            switch (transition)
            {
                case InstanceTransition.Running: return true;
                case InstanceTransition.Restarted: return CanRestart;
                case InstanceTransition.TryUpgrade: return CanTryUpgrade;
                default: throw new InvalidOperationException();
            }
        }

        public void Transit(InstanceTransition transition)
        {
            switch (transition)
            {
                case InstanceTransition.Running:
                    TransitRunning();
                    break;

                case InstanceTransition.Restarted:
                    TransitRestarted();
                    break;

                case InstanceTransition.TryUpgrade:
                    TransitUpgraded();
                    break;

                default: throw new InvalidOperationException();
            }
        }

        private void TransitRunning()
        {
            _status = InstanceStatus.Running;
            _startRecorder.Increase();
            CanRestart = true;
            CanTryUpgrade = true;
        }

        private void TransitRestarted()
        {
            _status = InstanceStatus.Running;
            _startRecorder.IncreaseRestart();
        }

        public void TransitUpgraded()
        {
            _startRecorder.Increase();
        }

        Task<InstanceStatus> IInstance.StatusAsync()
        {
            return Task.FromResult(_status);
        }

        Task<EndPoint> IInstance.EndPointAsync()
        {
            throw new InvalidOperationException();
        }

        Task<LogsSource> IInstance.LogsAsync()
        {
            throw new InvalidOperationException();
        }

        public Task<TtyStream> TtyAsync()
        {
            throw new InvalidOperationException();
        }

        private static string Sha256(string imageName)
        {
            var bytes = Encoding.UTF8.GetBytes(imageName);
            using (var crypto = new SHA256CryptoServiceProvider())
            {
                var hash = crypto.ComputeHash(bytes);
                var builder = new StringBuilder();
                foreach (var b in hash) builder.Append(b.ToString("x2"));

                return builder.ToString();
            }
        }

        public void SetStatus(InstanceStatus status)
        {
            _status = status;
        }

        public void SetIsNotFound(bool b)
        {
            _mockImage.SetupGet(x => x.IsNotFound).Returns(b);
        }

        public Environment ToEnvironment()
        {
            return new Environment(_mockImage.Object, this);
        }

        private sealed class StartRecorder
        {
            private int _restartTimes;
            private int _startTimes;

            public void Increase()
            {
                _startTimes++;
            }

            public void IncreaseRestart()
            {
                _restartTimes++;
            }

            public string Message()
            {
                if (_startTimes == 0) return string.Empty;

                var builder = new StringBuilder();
                builder.Append($"#{_startTimes.ToString()}");

                if (_restartTimes > 0) builder.Append($"@{_restartTimes.ToString()}");

                return builder.ToString();
            }
        }

        private sealed class RegistryImage : IRegistryImage
        {
            public RegistryImage(IImageName imageName)
            {
                ImageName = imageName;
            }

            public EnvironmentName Name { get; } = default(EnvironmentName);
            public IImageName ImageName { get; }
            public bool IsNotFound { get; } = false;
        }
    }
}
