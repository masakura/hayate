﻿using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Core.Applications
{
    internal sealed class MockInstanceRegistry : IInstanceRegistry
    {
        private readonly Mock<IInstanceRegistry> _mock;

        public MockInstanceRegistry()
        {
            _mock = new Mock<IInstanceRegistry>(MockBehavior.Strict);
        }

        IInstance IInstanceRegistry.Find(InstanceId id)
        {
            return _mock.Object.Find(id);
        }

        public IInstance FindOrCreate(IRegistryImage registryImage)
        {
            return _mock.Object.FindOrCreate(registryImage);
        }

        public MockInstance Add(string name)
        {
            var instance = new MockInstance(name);

            _mock.Setup(x => x.FindOrCreate(It.Is<IRegistryImage>(i => i.ImageName.Equals(instance.Image.ImageName))))
                .Returns(instance);

            return instance;
        }
    }
}