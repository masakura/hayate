﻿using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationNameTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new ApplicationName("hello").ToString();
            
            Assert.Equal("hello", result);
        }
    }
}