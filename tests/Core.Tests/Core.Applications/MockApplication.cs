﻿using System.Collections.Generic;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    internal sealed class MockApplication
    {
        private readonly MockApplications _owner;

        public MockApplication(MockApplications owner)
        {
            _owner = owner;
        }

        public ApplicationId Id { get; set; }
        public ApplicationName Name { get; set; }
        public string RepositoryName { get; set; }
        public TargetProjectId TargetId { get; set; }
        public IEnumerable<IRegistryImage> Images { get; set; }

        public void SetStatus(string tag, InstanceStatus status)
        {
            _owner.SetStatus(RepositoryName, tag, status);
        }
    }
}