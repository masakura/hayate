﻿using System;
using System.Linq;
using Hayate.Containers.Docker;
using Hayate.Core.Instances;
using Hayate.Core.Projects;
using Hayate.Instances;
using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationTest
    {
        private readonly Project _project;
        private readonly Application _target;

        public ApplicationTest()
        {
            _project = new MockProject
            {
                Id = ProjectId.NewId(),
                Name = new ProjectName("hello"),
                RepositoryName = new DockerRepositoryName("registry.example.com/group1/project1"),
                SiteUrl = new SiteUrl(new Uri("http://gitlab.example.com/group1/project1"))
            }.ToProject();

            var instances = new[]
            {
                new MockInstance("registry.example.com/group1/project1:latest"),
                new MockInstance("registry.example.com/group1/project1:v1.0")
            };

            _target = new Application(_project, instances.Select(x => x.ToEnvironment()));
        }

        [Fact]
        public void BindApplicationAndInstance()
        {

            var result = _target.Environments.Select(instance => instance.Application).ToArray();

            Assert.All(result, element => Assert.Equal(_target, element));
        }

        [Fact]
        public void SiteUrl()
        {
            var result = _target.SiteUrl;
            
            Assert.Equal(_project.SiteUrl, result);
        }
    }
}