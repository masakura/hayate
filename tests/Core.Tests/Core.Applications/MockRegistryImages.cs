﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Core.Applications
{
    internal sealed class MockRegistryImages : IRegistryImageRepository
    {
        private readonly Mock<IRegistryImageRepository> _mock;

        public MockRegistryImages()
        {
            _mock = new Mock<IRegistryImageRepository>(MockBehavior.Strict);
            _mock.Setup(x => x.FindAsync(It.IsAny<IRepositoryName>()))
                .ReturnsAsync(Enumerable.Empty<IImageName>());
        }

        Task<IEnumerable<IImageName>> IRegistryImageRepository.FindAsync(IRepositoryName repositoryName)
        {
            return _mock.Object.FindAsync(repositoryName);
        }

        Task<IRegistryImage> IRegistryImageRepository.FindAsync(IImageName imageName)
        {
            return _mock.Object.FindAsync(imageName);
        }

        public IEnumerable<IRegistryImage> AddImages(string repository, params string[] environmentNames)
        {
            var environments = new MockEnvironmentCollection(repository, environmentNames);

            var images = environments.ToImages();

            _mock.Setup(x => x.FindAsync(environments.Repository))
                .ReturnsAsync(images.Select(image => image.ImageName));

            foreach (var image in environments.ToImages())
                _mock.Setup(x => x.FindAsync(image.ImageName))
                    .ReturnsAsync(image);

            return images;
        }
    }
}