﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationRegistryTest : IDisposable
    {
        public ApplicationRegistryTest()
        {
            _mock = new MockApplications();
            _application1 = _mock.AddApplication(103, "Project1", "latest", "running", "v1.0");
            _application1.SetStatus("latest", InstanceStatus.Pending);
            _application1.SetStatus("running", InstanceStatus.Running);
            _application1.SetStatus("v1.0", InstanceStatus.Dead);

            _mock.AddApplication(110, "Project2");
            _mock.AddApplication(201, "Project3");

            _target = _mock.ApplicationRegistry();
        }

        public void Dispose()
        {
            _mock.Dispose();
        }

        private readonly ApplicationRegistry _target;
        private readonly MockApplication _application1;
        private readonly MockApplications _mock;

        private static ApplicationId NewApplicationId()
        {
            return new ApplicationId(Guid.NewGuid());
        }

        [Fact]
        public async Task FindAsync()
        {
            var result = await _target.FindAsync(_application1.Id);

            Assert.False(result.IsNotFound);
            Assert.Equal(_application1.Id, result.Id);
            Assert.Equal(_application1.Name, result.Name);
        }

        [Fact]
        public async Task FindAsyncByTargetId()
        {
            var result = await _target.FindAsync(_application1.TargetId);

            Assert.False(result.IsNotFound);
            Assert.Equal(_application1.Id, result.Id);
            Assert.Equal(_application1.Name, result.Name);
        }

        [Fact]
        public async Task FindAsyncInstances()
        {
            var result = (await _target.FindAsync(_application1.Id)).Environments;

            Assert.Equal(3, result.Count());
            Assert.Equal(new[] {"latest", "running", "v1.0"}, result.ToNames());
            Assert.Equal(new[] {InstanceStatus.Pending, InstanceStatus.Running, InstanceStatus.Dead},
                result.ToStatuses());
        }

        [Fact]
        public async Task FindAsyncNotFound()
        {
            var notFound = NewApplicationId();

            var result = await _target.FindAsync(notFound);

            Assert.True(result.IsNotFound);
            Assert.Equal(notFound, result.Id);
        }

        [Fact]
        public async Task FindEnvironmentAsync()
        {
            var result =
                await _target.FindEnvironmentAsync(_application1.Id, new EnvironmentName("latest"));

            Assert.False(result.IsNotFound);
            Assert.Equal(new EnvironmentName("latest"), result.Name);
        }

        [Fact]
        public async Task FindEnvironmentAsyncFromInstanceId()
        {
            var image = _application1.Images.ElementAt(2);
            var instanceId = InstanceId.From(image.ImageName);

            var result = await _target.FindEnvironmentAsync(instanceId);

            Assert.Equal(EnvironmentId.From(instanceId), result.Id);
        }

        [Fact]
        public async Task FindEnvironmentAsyncFromInstanceIdNotFound()
        {
            var notFound = new InstanceId("notfound");

            var result = await _target.FindEnvironmentAsync(notFound);

            Assert.Equal(EnvironmentId.From(notFound), result.Id);
            Assert.True(result.IsNotFound);
            // Assert.True(result.Application.IsNotFound);
        }

        [Fact]
        public async Task FindEnvironmentAsyncNotFoundApplication()
        {
            var result =
                await _target.FindEnvironmentAsync(new ApplicationId(Guid.NewGuid()), new EnvironmentName("latest"));

            Assert.True(result.IsNotFound);
            Assert.True(result.Application.IsNotFound);
            Assert.Equal(new EnvironmentName("latest"), result.Name);
        }

        [Fact]
        public async Task FindEnvironmentAsyncNotFoundInstance()
        {
            var result =
                await _target.FindEnvironmentAsync(_application1.Id, new EnvironmentName("notfound"));

            Assert.True(result.IsNotFound);
            Assert.Equal(new EnvironmentName("notfound"), result.Name);
            Assert.Equal(_application1.Id, result.Application.Id);
        }

        [Fact]
        public async Task ListAsync()
        {
            var result = await _target.ListAsync();

            Assert.Equal(3, result.Count());
            Assert.Equal(new[] {"Project1", "Project2", "Project3"}, result.ToNames());
        }
    }
}