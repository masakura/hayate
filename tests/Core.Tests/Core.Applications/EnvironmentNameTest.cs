﻿using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class EnvironmentNameTest
    {
        [Fact]
        public void ToStringTest()
        {
            var result = new EnvironmentName("latest").ToString();
            
            Assert.Equal("latest", result);
        }
    }
}