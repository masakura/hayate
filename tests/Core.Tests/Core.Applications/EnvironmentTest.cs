﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class EnvironmentTest
    {
        public EnvironmentTest()
        {
            _mock = new MockInstance("registry.example.com/group1/project1:latest");
            _target = _mock.ToEnvironment();
        }

        private readonly MockInstance _mock;
        private readonly Environment _target;

        [Fact]
        public void CanTransitRestarted()
        {
            var result = _target.CanTransit(InstanceTransition.Restarted);

            Assert.False(result);
        }

        [Fact]
        public void CanTransitRestartedIfRunning()
        {
            _target.Transit(InstanceTransition.Running);

            var result = _target.CanTransit(InstanceTransition.Restarted);

            Assert.True(result);
        }

        [Fact]
        public void CanTransitTryUpgraded()
        {
            var result = _target.CanTransit(InstanceTransition.TryUpgrade);

            Assert.False(result);
        }

        [Fact]
        public void CanTransitTryUpgradedIfRunning()
        {
            _target.Transit(InstanceTransition.Running);

            var result = _target.CanTransit(InstanceTransition.TryUpgrade);

            Assert.True(result);
        }

        [Fact]
        public void Id()
        {
            var result = _target.Id;

            Assert.Equal(_mock.Sha256Hash, result.ToString());
        }

        [Fact]
        public void IsNotFound()
        {
            _mock.SetIsNotFound(true);

            Assert.True(_target.IsNotFound);
        }

        [Fact]
        public void IsNotFoundFound()
        {
            _mock.SetIsNotFound(false);

            Assert.False(_target.IsNotFound);
        }

        [Fact]
        public void Name()
        {
            var result = _target.Name;

            Assert.Equal(_mock.EnvironmentName, result);
        }

        [Fact]
        public async Task StatusAsync()
        {
            _mock.SetStatus(InstanceStatus.Paused);

            var result = await _target.StatusAsync();

            Assert.Equal(InstanceStatus.Paused, result);
        }

        [Fact]
        public void TransitRestarted()
        {
            _target.Transit(InstanceTransition.Running);
            _mock.SetStatus(InstanceStatus.Exited);

            _target.Transit(InstanceTransition.Restarted);

            var result = _mock.ContainerId;

            Assert.Equal("#1@1", result);
        }

        [Fact]
        public void TransitRunning()
        {
            _target.Transit(InstanceTransition.Running);

            var result = _mock.ContainerId;

            Assert.Equal("#1", result);
        }

        [Fact]
        public void TransitUpgraded()
        {
            _target.Transit(InstanceTransition.Running);

            _target.Transit(InstanceTransition.TryUpgrade);

            var result = _mock.ContainerId;

            Assert.Equal("#2", result);
        }
    }
}
