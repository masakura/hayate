﻿using Hayate.Core.Instances;
using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class LogsSourceTest
    {
        [Fact]
        public void NoContainer()
        {
            var result = LogsSource.NoContainer().ToString();

            Assert.Equal("No container\r\n", result);
        }

        [Fact]
        public void NotFound()
        {
            var result = LogsSource.NotFound().ToString();

            Assert.Empty(result);
        }
    }
}