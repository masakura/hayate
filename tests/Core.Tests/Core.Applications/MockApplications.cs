﻿using System;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    internal sealed class MockApplications : IDisposable
    {
        private readonly MockRegistryImages _mockImages;
        private readonly MockExternalProjects _mockProjects;

        public MockApplications()
        {
            _mockProjects = new MockExternalProjects("registry2.example.com");
            _mockImages = new MockRegistryImages();

            MockProjects = _mockProjects.ProjectRegistry();
            MockInstances = new MockInstanceRegistry();
            MockImages = new RegistryImageRegistry(_mockImages);
        }

        private RegistryImageRegistry MockImages { get; }
        private MockInstanceRegistry MockInstances { get; }
        private IProjectRegistry MockProjects { get; }

        public void Dispose()
        {
            _mockProjects?.Dispose();
        }

        public MockApplication AddApplication(int targetId, string projectName, params string[] tags)
        {
            var project = _mockProjects.AddRegistered(targetId, projectName);
            var images = _mockImages.AddImages(project.RepositoryName.ToString(), tags);

            return new MockApplication(this)
            {
                Id = new ApplicationId(project.Id.ToGuid()),
                TargetId = project.TargetId,
                Name = new ApplicationName(project.Name.ToString()),
                RepositoryName = project.RepositoryName.ToString(),
                Images = images
            };
        }

        public ApplicationRegistry ApplicationRegistry()
        {
            return new ApplicationRegistry(MockProjects, MockImages, MockInstances);
        }

        public void SetStatus(string repositoryName, string tag, InstanceStatus status)
        {
            MockInstances.Add($"{repositoryName}:{tag}").SetStatus(status);
        }
    }
}