﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    internal static class EnvironmentEnumerableExtensions
    {
        public static IEnumerable<string> ToNames(this IEnumerable<Environment> collection)
        {
            return collection.Select(x => x.Name.ToString()).ToArray();
        }

        public static IEnumerable<InstanceStatus> ToStatuses(this IEnumerable<Environment> collection)
        {
            return collection.Select(async x => await x.StatusAsync())
                .Select(x => x.Result)
                .ToArray();
        }
    }
}