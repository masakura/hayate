﻿using System.Collections.Generic;
using System.Linq;

namespace Hayate.Core.Applications
{
    internal static class ApplicationEnumerableExtensions
    {
        public static IEnumerable<string> ToNames(this IEnumerable<Application> collection)
        {
            return collection.Select(x => x.Name.ToString()).ToArray();
        }
    }
}