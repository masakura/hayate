﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Hayate.Containers.Docker;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Moq;

namespace Hayate.Core.Applications
{
    internal sealed class MockEnvironmentCollection
    {
        private readonly ReadOnlyCollection<string> _environmentNames;

        public MockEnvironmentCollection(string repository, IEnumerable<string> environmentNames)
        {
            Repository = new DockerRepositoryName(repository);
            _environmentNames = environmentNames.ToList().AsReadOnly();
        }

        public DockerRepositoryName Repository { get; }

        public IEnumerable<IRegistryImage> ToImages()
        {
            return _environmentNames
                .Select(name =>
                {
                    var mock = new Mock<IRegistryImage>(MockBehavior.Strict);
                    mock.SetupGet(x => x.Name).Returns(new EnvironmentName(name));
                    mock.SetupGet(x => x.ImageName).Returns(ImageName(name));
                    mock.SetupGet(x => x.IsNotFound).Returns(false);
                    return mock.Object;
                })
                .ToArray();
        }

        private IImageName ImageName(string name)
        {
            return DockerImageName.Parse($"{Repository.ToString()}:{name}");
        }
    }
}