﻿using System;
using Xunit;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationIdTest
    {
        [Fact]
        public void IsEmpty()
        {
            var result = new ApplicationId().IsEmpty;

            Assert.True(result);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new ApplicationId(Guid.NewGuid()).IsEmpty;

            Assert.False(result);
        }

        [Fact]
        public void ToGuid()
        {
            var guid = Guid.NewGuid();

            var result = new ApplicationId(guid).ToGuid();

            Assert.Equal(guid, result);
        }

        [Fact]
        public void ToStringTest()
        {
            var guid = Guid.NewGuid();

            var result = new ApplicationId(guid).ToString();

            Assert.Equal(guid.ToString(), result);
        }
    }
}