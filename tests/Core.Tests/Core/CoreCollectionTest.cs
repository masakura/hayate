﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Hayate.Core
{
    public sealed class CoreCollectionTest
    {
        [Fact]
        public void IsEmpty()
        {
            var result = new ElementCollection(Enumerable.Empty<Element>()).IsEmpty;
            
            Assert.True(result);
        }

        [Fact]
        public void IsEmptyIsNotEmpty()
        {
            var result = new ElementCollection(new Element[1]).IsEmpty;
            
            Assert.False(result);
        }

        private sealed class Element
        {
        }

        private sealed class ElementCollection : CoreCollection<Element>
        {
            public ElementCollection(IEnumerable<Element> collection) : base(collection)
            {
            }
        }
    }
}