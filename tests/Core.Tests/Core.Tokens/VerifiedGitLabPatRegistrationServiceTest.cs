﻿using System;
using System.Threading.Tasks;
using Hayate.Infrastructure;
using Hayate.Infrastructure.Core;
using Moq;
using Xunit;

namespace Hayate.Core.Tokens
{
    public sealed class VerifiedGitLabPatRegistrationServiceTest
    {
        public VerifiedGitLabPatRegistrationServiceTest()
        {
            _token1 = new PersonalAccessToken("tokenstring1");
            _token2 = new PersonalAccessToken("tokenstring2");

            _switchUser = new SwitchUser();
            var pats = new PatRepository(ApplicationDbContext.CreateInMemory());

            _mockVerifyService = new Mock<IGitLabPatVerifyService>(MockBehavior.Strict);
            _mockVerifyService.Setup(x => x.VerfyAsync(_token1)).ReturnsAsync(true);
            _mockVerifyService.Setup(x => x.VerfyAsync(_token2)).ReturnsAsync(true);

            var registration = new GitLabPatRegistrationService(_switchUser, pats);
            _target = new VerifiedGitLabPatRegistrationService(registration, _mockVerifyService.Object);
        }

        private readonly VerifiedGitLabPatRegistrationService _target;
        private readonly PersonalAccessToken _token1;
        private readonly PersonalAccessToken _token2;
        private readonly SwitchUser _switchUser;
        private readonly Mock<IGitLabPatVerifyService> _mockVerifyService;

        [Fact]
        public async Task CannotSaveIfAnonymouse()
        {
            _switchUser.Switch(SwitchUser.Anonymouse);

            await Assert.ThrowsAsync<InvalidOperationException>(() => _target.SavePatAsync(_token1));
        }

        [Fact]
        public async Task NotVerified()
        {
            _mockVerifyService.Setup(x => x.VerfyAsync(_token1)).ReturnsAsync(false);

            _switchUser.Switch(SwitchUser.User1);

            await _target.SavePatAsync(_token1);
            var result = await _target.LoadVerifiedPatAsync();

            Assert.Equal(new VerifiedPersonalAccessToken(_token1, false), result);
        }

        [Fact]
        public async Task PatIsNotRegistration()
        {
            var result = await _target.LoadVerifiedPatAsync();

            Assert.Equal(new VerifiedPersonalAccessToken(PersonalAccessToken.Empty, false), result);
        }

        [Fact]
        public async Task SavePat()
        {
            _switchUser.Switch(SwitchUser.User1);

            await _target.SavePatAsync(_token1);
            var result = await _target.LoadVerifiedPatAsync();

            Assert.Equal(new VerifiedPersonalAccessToken(_token1, true), result);
        }

        [Fact]
        public async Task SavePatDifferenceUser()
        {
            _switchUser.Switch(SwitchUser.User1);
            await _target.SavePatAsync(_token1);

            _switchUser.Switch(SwitchUser.User2);
            await _target.SavePatAsync(_token2);

            _switchUser.Switch(SwitchUser.User1);
            var result = await _target.LoadVerifiedPatAsync();

            Assert.Equal(new VerifiedPersonalAccessToken(_token1, true), result);
        }
    }
}
