﻿using Hayate.Core.Users;

namespace Hayate.Core.Tokens
{
    internal sealed class SwitchUser : ICurrentUserService
    {
        private User _currentUser = Anonymouse;

        public void Switch(User user)
        {
            _currentUser = user;
        }

        User ICurrentUserService.GetCurrentUser()
        {
            return _currentUser;
        }

        public static User User1 { get; } = new User(new UserId("user1"));
        public static User User2 { get; } = new User(new UserId("user2"));
        public static User Anonymouse => User.Anonymouse;
    }
}
