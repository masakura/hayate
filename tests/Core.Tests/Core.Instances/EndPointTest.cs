﻿using System;
using Xunit;

namespace Hayate.Core.Instances
{
    public sealed class EndPointTest
    {
        [Fact]
        public void Empty()
        {
            var result = EndPoint.Empty;

            Assert.True(result.IsEmpty);
            Assert.Null(result.ToUri());
        }

        [Fact]
        public void Equal()
        {
            var left = new EndPoint("http://example.com").GetHashCode();
            var right = new EndPoint("http://example.com").GetHashCode();

            Assert.True(left.Equals(right));
            Assert.True(right.Equals(left));
        }

        [Fact]
        public void GetHashCodeEqual()
        {
            var left = new EndPoint("http://example.com").GetHashCode();
            var right = new EndPoint("http://example.com").GetHashCode();

            Assert.Equal(left, right);
        }

        [Fact]
        public void GetHashCodeNotEqual()
        {
            var left = new EndPoint("http://example.com").GetHashCode();
            var right = new EndPoint("http://example.net").GetHashCode();

            Assert.NotEqual(left, right);
        }

        [Fact]
        public void IsEmpty()
        {
            var result = new EndPoint();

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new EndPoint("http://example.com");

            Assert.False(result.IsEmpty);
        }

        [Fact]
        public void NotEqual()
        {
            var left = new EndPoint("http://example.com").GetHashCode();
            var right = new EndPoint("http://example.net").GetHashCode();

            Assert.False(left.Equals(right));
            Assert.False(right.Equals(left));
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new EndPoint("http://example.com:5000").ToString();

            Assert.Equal("http://example.com:5000/", result);
        }

        [Fact]
        public void ToUri()
        {
            var result = new EndPoint("http://example.com").ToUri();

            Assert.Equal(new Uri("http://example.com"), result);
        }
    }
}