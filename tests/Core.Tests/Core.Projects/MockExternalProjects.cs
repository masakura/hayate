﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Infrastructure;
using Hayate.Infrastructure.Core;
using Moq;

namespace Hayate.Core.Projects
{
    internal sealed class MockExternalProjects : IExternalProjectRepository, IProjectMappingRepository, IDisposable
    {
        private readonly ApplicationDbContext _db;
        private readonly ProjectMappingRepository _mockMappings;
        private readonly Mock<IExternalProjectRepository> _mockProjects;
        private readonly IList<MockProject> _projects = new List<MockProject>();
        private readonly string _registryHostName;

        public MockExternalProjects() : this("registry.example.com")
        {
        }

        public MockExternalProjects(string registryHostName)
        {
            _registryHostName = registryHostName;
            _db = ApplicationDbContext.CreateInMemory();
            _mockMappings = new ProjectMappingRepository(_db);
            _mockProjects = new Mock<IExternalProjectRepository>(MockBehavior.Strict);

            _mockProjects.Setup(x => x.ListAsync())
                .ReturnsAsync(() => _projects.Select(x => x.External).ToArray());
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        Task<IExternalProject> IExternalProjectRepository.FindAsync(TargetProjectId targetProjectId)
        {
            return _mockProjects.Object.FindAsync(targetProjectId);
        }

        Task<IEnumerable<IExternalProject>> IExternalProjectRepository.ListAsync()
        {
            return _mockProjects.Object.ListAsync();
        }

        Task<IEnumerable<ProjectMapping>> IProjectMappingRepository.ListAsync()
        {
            return _mockMappings.ListAsync();
        }

        Task<TargetProjectId> IProjectMappingRepository.FindAsync(ProjectId projectId)
        {
            return _mockMappings.FindAsync(projectId);
        }

        Task<ProjectId> IProjectMappingRepository.FindAsync(TargetProjectId targetProjectId)
        {
            return _mockMappings.FindAsync(targetProjectId);
        }

        Task IProjectMappingRepository.AddAsync(ProjectId projectId, TargetProjectId targetProjectId)
        {
            return _mockMappings.AddAsync(projectId, targetProjectId);
        }

        public MockProject AddRegistered(int targetId, string name)
        {
            var project = new MockProject
            {
                Id = ProjectId.NewId(),
                TargetId = new TargetProjectId(targetId.ToString()),
                Name = new ProjectName(name),
                RepositoryName = new DockerRepositoryName($"{_registryHostName}/{targetId.ToString()}")
            };

            _db.ProjectMappings.Add(project.ToRow());
            _db.SaveChanges();

            _mockProjects.Setup(x => x.FindAsync(project.TargetId))
                .ReturnsAsync(project.External);

            return Add(project);
        }

        public MockProject AddNotRegistered(int targetId, string name)
        {
            var project = new MockProject
            {
                TargetId = new TargetProjectId(targetId.ToString()),
                Name = new ProjectName(name)
            };

            _mockProjects.Setup(x => x.FindAsync(project.TargetId))
                .ReturnsAsync(project.External);

            return Add(project);
        }

        private MockProject Add(MockProject project)
        {
            _projects.Add(project);
            return project;
        }

        public ProjectRegistry ProjectRegistry()
        {
            return new ProjectRegistry(this, this);
        }
    }
}