﻿using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class ProjectNameTest
    {
        [Fact]
        public void Empty()
        {
            var result = ProjectName.Empty;

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public void IsEmpty()
        {
            var result = new ProjectName().IsEmpty;

            Assert.True(result);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new ProjectName("name").IsEmpty;

            Assert.False(result);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new ProjectName("hello").ToString();

            Assert.Equal("hello", result);
        }
    }
}