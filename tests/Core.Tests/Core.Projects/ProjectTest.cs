﻿using System;
using Moq;
using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class ProjectTest : IDisposable
    {
        public ProjectTest()
        {
            _projectId = ProjectId.NewId();

            _mockExternal = new Mock<IExternalProject>(MockBehavior.Strict);
        }

        public void Dispose()
        {
            _mockExternal.VerifyAll();
        }

        private readonly ProjectId _projectId;
        private readonly Mock<IExternalProject> _mockExternal;

        [Fact]
        public void NotFound()
        {
            var result = Project.NotFound(_projectId);

            Assert.Equal(_projectId, result.Id);
            Assert.True(result.IsNotFound);
            Assert.True(result.IsNotRegistered);
        }

        [Fact]
        public void NotFoundEmptyProjectId()
        {
            Assert.Throws<ArgumentException>(() => Project.NotFound(ProjectId.Empty));
        }

        [Fact]
        public void NotRegistered()
        {
            _mockExternal.SetupGet(x => x.Name).Returns(new ProjectName("hello"));

            var result = Project.NotRegistered(_mockExternal.Object);

            Assert.True(result.IsNotRegistered);
            Assert.True(result.Id.IsEmpty);
            Assert.Equal(new ProjectName("hello"), result.Name);
        }

        [Fact]
        public void Registered()
        {
            _mockExternal.SetupGet(x => x.IsNotFound).Returns(false);

            var result = Project.Registered(_projectId, _mockExternal.Object);

            Assert.Equal(_projectId, result.Id);
            Assert.False(result.IsNotFound);
            Assert.False(result.IsNotRegistered);
        }

        [Fact]
        public void RegisteredEmptyProjectId()
        {
            Assert.Throws<ArgumentException>(() => Project.Registered(ProjectId.Empty, _mockExternal.Object));
        }

        [Fact]
        public void TargetId()
        {
            var targetProjectId = new TargetProjectId("109");

            _mockExternal.SetupGet(x => x.Id).Returns(targetProjectId);

            var result = Project.Registered(ProjectId.NewId(), _mockExternal.Object).TargetId;

            Assert.Equal(targetProjectId, result);
        }
    }
}