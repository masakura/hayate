﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class ProjectRegistryTest
    {
        public ProjectRegistryTest()
        {
            var mock = new MockExternalProjects();
            _registered = mock.AddRegistered(108, "App1");
            _notRegistered = mock.AddNotRegistered(109, "NotRegistered");

            _target = mock.ProjectRegistry();
        }

        private readonly ProjectRegistry _target;
        private readonly MockProject _registered;
        private readonly MockProject _notRegistered;

        [Fact]
        public async Task FindAsync()
        {
            var result = await _target.FindAsync(_registered.Id);

            Assert.False(result.IsNotFound);
            Assert.Equal(_registered.Id, result.Id);
            Assert.Equal(_registered.Name, result.Name);
        }

        [Fact]
        public async Task FindAsyncNotRegistered()
        {
            var notRegisteredId = ProjectId.NewId();

            var result = await _target.FindAsync(notRegisteredId);

            Assert.True(result.IsNotFound);
            Assert.Equal(notRegisteredId, result.Id);
        }

        [Fact]
        public async Task FindOrRegisterAsyncAlreadyExists()
        {
            var result = await _target.FindOrRegisterAsync(_registered.TargetId);

            Assert.False(result.IsNotFound);
            Assert.Equal(_registered.Id, result.Id);
            Assert.Equal(_registered.Name, result.Name);
        }

        [Fact]
        public async Task FindOrRegisterAsyncRegister()
        {
            var result = await _target.FindOrRegisterAsync(_notRegistered.TargetId);

            Assert.False(result.IsNotFound);
            Assert.False(result.Id.IsEmpty);
            Assert.Equal(_notRegistered.Name, result.Name);
        }

        [Fact]
        public async Task FindOrRegisterAsyncRegisterAndFind()
        {
            var project = await _target.FindOrRegisterAsync(_notRegistered.TargetId);

            var result = await _target.FindAsync(project.Id);

            Assert.False(result.IsNotFound);
            Assert.Equal(project.Id, result.Id);
            Assert.Equal(_notRegistered.Name, result.Name);
        }

        [Fact]
        public async Task ListAsync()
        {
            var result = await _target.ListAsync();

            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task ListRegisteredAsync()
        {
            var result = await _target.ListRegisteredAsync();

            Assert.Equal(1, result.Count);

            var result1 = result.Find(_registered.Id);

            Assert.Equal(_registered.Id, result1.Id);
            Assert.Equal(_registered.Name, result1.Name);
        }

        [Fact]
        public async Task ListRegisteredAsyncEmpty()
        {
            var target = new MockExternalProjects().ProjectRegistry();

            var result = await target.ListRegisteredAsync();

            Assert.Empty(result);
        }

        [Fact]
        public async Task ListRegisteredAsyncTwoItems()
        {
            await _target.FindOrRegisterAsync(_notRegistered.TargetId);

            var result = await _target.ListRegisteredAsync();

            Assert.Equal(2, result.Count());
            Assert.Contains(_registered.Name, result.Names());
            Assert.Contains(_notRegistered.Name, result.Names());
        }
    }
}