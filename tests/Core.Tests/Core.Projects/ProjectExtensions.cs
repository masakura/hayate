﻿using System.Collections.Generic;
using System.Linq;

namespace Hayate.Core.Projects
{
    public static class ProjectExtensions
    {
        public static IEnumerable<ProjectName> Names(this IEnumerable<Project> projects)
        {
            return projects.Select(x => x.Name).ToArray();
        }
    }
}