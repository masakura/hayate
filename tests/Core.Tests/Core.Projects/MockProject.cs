﻿using Hayate.Containers.Docker;
using Hayate.Infrastructure.Core;
using Moq;

namespace Hayate.Core.Projects
{
    internal sealed class MockProject
    {
        private readonly Mock<IExternalProject> _mock;

        public MockProject()
        {
            _mock = new Mock<IExternalProject>(MockBehavior.Strict);
            _mock.SetupGet(x => x.Id).Returns(() => TargetId);
            _mock.SetupGet(x => x.Name).Returns(() => Name);
            _mock.SetupGet(x => x.RepositoryName).Returns(() => RepositoryName);
            _mock.SetupGet(x => x.SiteUrl).Returns(() => SiteUrl);
            _mock.SetupGet(x => x.IsNotFound).Returns(false);
        }

        public ProjectId Id { get; set; }
        public TargetProjectId TargetId { get; set; }
        public ProjectName Name { get; set; }
        public DockerRepositoryName RepositoryName { get; set; }
        public SiteUrl SiteUrl { get; set; }

        public IExternalProject External => _mock.Object;

        public ProjectMappingRow ToRow()
        {
            return new ProjectMappingRow(Id.ToGuid(), TargetId.ToString());
        }

        public Project ToProject()
        {
            return Project.Registered(Id, External);
        }
    }
}