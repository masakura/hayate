﻿using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class TargetProjectIdTest
    {
        [Fact]
        public void Empty()
        {
            var result = TargetProjectId.Empty;

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public void IsEmpty()
        {
            var result = new TargetProjectId().IsEmpty;

            Assert.True(result);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new TargetProjectId("hello").IsEmpty;

            Assert.False(result);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new TargetProjectId("109").ToString();

            Assert.Equal("109", result);
        }
    }
}