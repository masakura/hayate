﻿using System;
using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class SiteUrlTest
    {
        [Fact]
        public void Empty()
        {
            var result = SiteUrl.Empty;

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public void IsEmpty()
        {
            var result = new SiteUrl().IsEmpty;

            Assert.True(result);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new SiteUrl(new Uri("http://www.example.com")).IsEmpty;

            Assert.False(result);
        }

        [Fact]
        public void ToStringTest()
        {
            var result = new SiteUrl(new Uri("http://gitlab.example.com/group1/project1")).ToString();

            Assert.Equal("http://gitlab.example.com/group1/project1", result);
        }
    }
}