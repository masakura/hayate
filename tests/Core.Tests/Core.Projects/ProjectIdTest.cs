﻿using System;
using Xunit;

namespace Hayate.Core.Projects
{
    public sealed class ProjectIdTest
    {
        [Fact]
        public void IsEmpty()
        {
            var result = new ProjectId().IsEmpty;

            Assert.True(result);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new ProjectId(Guid.NewGuid()).IsEmpty;

            Assert.False(result);
        }

        [Fact]
        public void NewId()
        {
            var result = ProjectId.NewId();

            Assert.False(result.IsEmpty);
        }

        [Fact]
        public void NewIdNew()
        {
            var result1 = ProjectId.NewId();
            var result2 = ProjectId.NewId();

            Assert.NotEqual(result1, result2);
        }

        [Fact]
        public void ToGuid()
        {
            var guid = Guid.NewGuid();

            var result = new ProjectId(guid).ToGuid();

            Assert.Equal(guid, result);
        }
    }
}