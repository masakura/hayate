﻿namespace Hayate.Projects
{
    /// <summary>
    ///     プロジェクト名。
    /// </summary>
    public struct ProjectName
    {
        private readonly string _value;

        public ProjectName(string value)
        {
            _value = value;
        }
    }
}