﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    internal sealed class AvailableContainer : Container
    {
        private readonly IContainerId _id;
        private readonly IDriver _driver;
        private readonly IImageId _imageId;

        public AvailableContainer(IContainerId id, IDriver driver, IImageId imageId)
        {
            _id = id;
            _driver = driver;
            _imageId = imageId;
        }

        public override bool IsAvailable { get; } = true;

        public override Task StartAsync()
        {
            return _driver.StartContainerAsync(_id);
        }

        public override Task RestartAsync()
        {
            return _driver.RestartContainerAsync(_id);
        }

        public override Task<InstanceStatus> StatusAsync()
        {
            return _driver.ContainerStatusAsync(_id);
        }

        public override Task<EndPoint> EndPointAsync()
        {
            return _driver.EndPointAsync(_id);
        }

        public override Task<LogsSource> LogsAsync()
        {
            return _driver.LogsContainerAsync(_id);
        }

        public override Task<TtyStream> TtyAsync()
        {
            return _driver.ExecContainerAsync(_id, "/bin/bash");
        }

        public override bool IsCreatedFrom(IImageId imageId)
        {
            return _imageId.Equals(imageId);
        }

        public override string ToString()
        {
            return $"Container: (Id => {_id})";
        }
    }
}
