﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    public sealed class ImageRegistry : IImageRegistry
    {
        private readonly IDriver _driver;

        public ImageRegistry(IDriver driver)
        {
            _driver = driver;
        }

        public Task<ImageAlias> FindAsync(IImageName imageName)
        {
            var alias = new ImageAlias(imageName, _driver);
            return Task.FromResult(alias);
        }
    }
}