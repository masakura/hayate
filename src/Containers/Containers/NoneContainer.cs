﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    internal sealed class NoneContainer : Container
    {
        public override bool IsAvailable { get; } = false;

        public override Task StartAsync()
        {
            throw new InvalidOperationException();
        }

        public override Task RestartAsync()
        {
            throw new InvalidOperationException();
        }

        public override Task<InstanceStatus> StatusAsync()
        {
            return Task.FromResult(InstanceStatus.Pending);
        }

        public override Task<EndPoint> EndPointAsync()
        {
            return Task.FromResult(EndPoint.Empty);
        }

        public override Task<LogsSource> LogsAsync()
        {
            return Task.FromResult(LogsSource.NoContainer());
        }

        public override Task<TtyStream> TtyAsync()
        {
            return Task.FromResult(TtyStream.NoContainer());
        }

        public override bool IsCreatedFrom(IImageId imageId)
        {
            return false;
        }

        public override string ToString()
        {
            return "Container: (None)";
        }
    }
}
