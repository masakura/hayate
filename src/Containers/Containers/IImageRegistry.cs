﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Containers
{
    public interface IImageRegistry
    {
        Task<ImageAlias> FindAsync(IImageName imageName);
    }
}