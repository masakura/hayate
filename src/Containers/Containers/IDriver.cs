﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    public interface IDriver
    {
        Task<IImageId> PullAsync(IImageName imageName);
        Task<IContainerId> CreateContainerAsync(IImageId imageId);
        Task StartContainerAsync(IContainerId containerId);
        Task RestartContainerAsync(IContainerId containerId);
        Task<InstanceStatus> ContainerStatusAsync(IContainerId containerId);
        Task<EndPoint> EndPointAsync(IContainerId containerId);
        Task<LogsSource> LogsContainerAsync(IContainerId containerId);
        Task<TtyStream> ExecContainerAsync(IContainerId containerId, string cmd);
    }
}
