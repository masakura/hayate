﻿using System;

namespace Hayate.Containers
{
    public sealed class RequireAuthenticateException : Exception
    {
        private static string ExceptionMessage = "Require authenticate";

        public RequireAuthenticateException() : base(ExceptionMessage)
        {
        }

        public RequireAuthenticateException(Exception innerException) : base(ExceptionMessage, innerException)
        {
        }

        public static bool Contains(Exception ex)
        {
            if (ex == null) return false;
            if (ex is RequireAuthenticateException) return true;
            if (ex is AggregateException aggregate)
                foreach (var inner in aggregate.InnerExceptions)
                    if (Contains(inner))
                        return true;

            return Contains(ex.InnerException);
        }
    }
}
