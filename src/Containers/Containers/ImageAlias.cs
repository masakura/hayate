﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    public sealed class ImageAlias
    {
        private readonly IImageName _name;
        private readonly IDriver _driver;

        public ImageAlias(IImageName name, IDriver driver)
        {
            _name = name;
            _driver = driver;
        }

        public async Task<Image> PullAsync()
        {
            var imageId = await _driver.PullAsync(_name);
            return new Image(imageId, _driver);
        }

        public override string ToString()
        {
            return $"Image: (Name => {_name})";
        }
    }
}
