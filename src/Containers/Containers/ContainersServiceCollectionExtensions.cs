﻿using Microsoft.Extensions.DependencyInjection;

namespace Hayate.Containers
{
    public static class ContainersServiceCollectionExtensions
    {
        public static void AddContainerServices(this IServiceCollection services)
        {
            services.AddTransient<IImageRegistry, ImageRegistry>();
        }
    }
}
