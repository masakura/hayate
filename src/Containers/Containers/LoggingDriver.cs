﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Microsoft.Extensions.Logging;

namespace Hayate.Containers
{
    public sealed class LoggingDriver : IDriver
    {
        private readonly ILogger<IDriver> _logger;
        private readonly IDriver _target;

        public LoggingDriver(IDriver target, ILogger<IDriver> logger)
        {
            _target = target;
            _logger = logger;
        }

        public Task<IImageId> PullAsync(IImageName imageName)
        {
            return WrapperAsync(() => _target.PullAsync(imageName), $"docker pull {imageName}");
        }

        public Task<IContainerId> CreateContainerAsync(IImageId imageId)
        {
            return WrapperAsync(() => _target.CreateContainerAsync(imageId), $"docker create {imageId}");
        }

        public Task StartContainerAsync(IContainerId containerId)
        {
            return WrapperAsync(() => _target.StartContainerAsync(containerId), $"docker start {containerId}");
        }

        public Task RestartContainerAsync(IContainerId containerId)
        {
            return WrapperAsync(() => _target.RestartContainerAsync(containerId), $"docker restart {containerId}");
        }

        public Task<InstanceStatus> ContainerStatusAsync(IContainerId containerId)
        {
            return WrapperAsync(() => _target.ContainerStatusAsync(containerId), $"docker status {containerId}");
        }

        public Task<EndPoint> EndPointAsync(IContainerId containerId)
        {
            return WrapperAsync(() => _target.EndPointAsync(containerId), $"docker inspect {containerId}");
        }

        public Task<LogsSource> LogsContainerAsync(IContainerId containerId)
        {
            return WrapperAsync(() => _target.LogsContainerAsync(containerId), $"docker logs {containerId}");
        }

        public Task<TtyStream> ExecContainerAsync(IContainerId containerId, string cmd)
        {
            return WrapperAsync(() => _target.ExecContainerAsync(containerId, cmd), $"docker exec {containerId} {cmd}");
        }

        private async Task<T> WrapperAsync<T>(Func<Task<T>> action, string commandText)
        {
            _logger.LogInformation($"run `{commandText}`");

            try
            {
                return await action();
            }
            catch (Exception ex)
            {
                _logger.LogError($"error `{commandText}`\n{ex}");
                throw;
            }
            finally
            {
                _logger.LogInformation($"finish `{commandText}`");
            }
        }

        private Task WrapperAsync(Func<Task> action, string commandText)
        {
            return WrapperAsync<object>(async () =>
            {
                await action();
                return Task.CompletedTask;
            }, commandText);
        }
    }
}
