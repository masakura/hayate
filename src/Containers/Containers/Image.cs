﻿using System.Threading.Tasks;

namespace Hayate.Containers
{
    public sealed class Image
    {
        private readonly IDriver _driver;

        public Image(IImageId id, IDriver driver)
        {
            Id = id;
            _driver = driver;
        }

        internal IImageId Id { get; }

        public async Task<Container> CreateContainerAsync()
        {
            var containerId = await _driver.CreateContainerAsync(Id);
            return Container.Available(containerId, _driver, Id);
        }

        public override string ToString()
        {
            return $"Image: (Id => {Id})";
        }
    }
}
