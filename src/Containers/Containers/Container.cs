﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers
{
    /// <summary>
    ///     コンテナー。
    /// </summary>
    public abstract class Container
    {
        /// <summary>
        ///     コンテナーが有効かどうかを取得します。
        /// </summary>
        public abstract bool IsAvailable { get; }

        /// <summary>
        ///     コンテナーが再起動できるかどうかを取得します。
        /// </summary>
        public bool CanRestart => IsAvailable;

        /// <summary>
        ///     コンテナーを開始します。
        /// </summary>
        public abstract Task StartAsync();

        /// <summary>
        ///     コンテナーを再起動します。
        /// </summary>
        public abstract Task RestartAsync();

        /// <summary>
        ///     コンテナーの状態を取得します。
        /// </summary>
        /// <returns></returns>
        public abstract Task<InstanceStatus> StatusAsync();

        /// <summary>
        ///     コンテナーの公開されたエンドポイントを取得します。
        /// </summary>
        /// <returns>公開されたエンドポイント。</returns>
        public abstract Task<EndPoint> EndPointAsync();

        /// <summary>
        ///     コンテナーのログストリームを取得します。
        /// </summary>
        /// <returns>ログストリーム。</returns>
        public abstract Task<LogsSource> LogsAsync();

        /// <summary>
        /// TTY を開きます。
        /// </summary>
        /// <returns>開いた TTY のストリーム。</returns>
        public abstract Task<TtyStream> TtyAsync();

        /// <summary>
        ///     指定されたイメージ ID から作成されたコンテナーかどうかを取得します。
        /// </summary>
        /// <param name="imageId">イメージ ID。</param>
        /// <returns>指定されたイメージ ID から作成されたかどうか。</returns>
        public abstract bool IsCreatedFrom(IImageId imageId);

        /// <summary>
        ///     作成されているコンテナーを取得します。
        /// </summary>
        /// <param name="id">コンテナーの ID。</param>
        /// <param name="driver">コンテナーを操作するためのドライバー。</param>
        /// <param name="imageId">コンテナーを作成したイメージの ID。</param>
        /// <returns>コンテナー。</returns>
        public static Container Available(IContainerId id, IDriver driver, IImageId imageId)
        {
            return new AvailableContainer(id, driver, imageId);
        }

        /// <summary>
        ///     作成されていないコンテナーを取得します。Null Object パターン。
        /// </summary>
        /// <returns>作成されていないコンテナー。</returns>
        public static Container NoneContainer()
        {
            return new NoneContainer();
        }
    }
}
