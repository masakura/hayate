﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Container = Hayate.Containers.Container;

namespace Hayate.Instances
{
    public sealed class Instance : IInstance
    {
        private readonly IImageName _imageName;
        private readonly IImageRegistry _images;
        private InstanceStatus _status = InstanceStatus.Pending;

        public Instance(IImageName imageName, IImageRegistry images)
        {
            _imageName = imageName;
            _images = images;
        }

        public Container Container { get; private set; } = Container.NoneContainer();
        public InstanceId Id => InstanceId.From(_imageName);
        public bool CanRestart => Container.CanRestart;
        public bool CanTryUpgrade => Container.IsAvailable;

        public bool CanTransit(InstanceTransition transition)
        {
            InstanceTransition transition1 = transition;
            switch (transition1)
            {
                case InstanceTransition.Running: return true;
                case InstanceTransition.Restarted: return CanRestart;
                case InstanceTransition.TryUpgrade: return CanTryUpgrade;
            }

            throw new InvalidEnumArgumentException(nameof(transition1), (int) transition1, typeof(InstanceTransition));
        }

        public void Transit(InstanceTransition transition)
        {
#pragma warning disable 4014
            TransitAsync(transition);
#pragma warning restore 4014
        }

        public async Task<InstanceStatus> StatusAsync()
        {
            if (_status != InstanceStatus.Created) return _status;

            return await Container.StatusAsync();
        }

        public Task<EndPoint> EndPointAsync()
        {
            return Container.EndPointAsync();
        }

        public Task<LogsSource> LogsAsync()
        {
            return Container.LogsAsync();
        }

        public Task<TtyStream> TtyAsync()
        {
            return Container.TtyAsync();
        }

        public override string ToString()
        {
            return $"Instance: (Id => {Id.ToString()})";
        }

        public async Task TransitAsync(InstanceTransition transition)
        {
            switch (transition)
            {
                case InstanceTransition.Running:
                    await TransitRunningAsync();
                    return;

                case InstanceTransition.Restarted:
                    await TransitRestartedAsync();
                    return;

                case InstanceTransition.TryUpgrade:
                    await TransitTryUpgradedAsync();
                    return;

            }

            throw new InvalidEnumArgumentException(nameof(transition), (int) transition, typeof(InstanceTransition));
        }

        public async Task RunAsync()
        {
            var image = await PullAsync();
            await CreateContainerAsync(image);
            await StartContainerAsync();
        }

        private async Task<Image> PullAsync()
        {
            _status = InstanceStatus.Pulling;
            var alias = await _images.FindAsync(_imageName);
            return await alias.PullAsync();
        }

        private async Task CreateContainerAsync(Image image)
        {
            _status = InstanceStatus.Creating;
            Container = await image.CreateContainerAsync();
            _status = InstanceStatus.Created;
        }

        private async Task StartContainerAsync()
        {
            await Container.StartAsync();
        }

        private async Task TransitRunningAsync()
        {
            if (Container.IsAvailable) return;

            await RunAsync();
        }

        private async Task TransitRestartedAsync()
        {
            await Container.RestartAsync();
        }

        private async Task TransitTryUpgradedAsync()
        {
            if (!Container.IsAvailable) throw new InvalidOperationException();

            // 裏で Pull したいので、ステータスを Pulling に変更しません
            // つまり、イメージが更新されていなければ、このメソッドは表面上何もしません
            var alias = await _images.FindAsync(_imageName);
            var image = await alias.PullAsync();

            if (Container.IsCreatedFrom(image.Id)) return;

            await CreateContainerAsync(image);
            await StartContainerAsync();
        }
    }
}
