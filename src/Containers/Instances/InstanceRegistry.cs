﻿using Hayate.Containers;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;

namespace Hayate.Instances
{
    public sealed class InstanceRegistry : IInstanceRegistry
    {
        private readonly IImageRegistry _images;
        private readonly IInstanceStore _store;

        public InstanceRegistry(IImageRegistry images, IInstanceStore store)
        {
            _images = images;
            _store = store;
        }

        public IInstance Find(InstanceId id)
        {
            return _store.Find(id);
        }

        public IInstance FindOrCreate(IRegistryImage registryImage)
        {
            var id = InstanceId.From(registryImage.ImageName);
            return Find(id) ?? Create(registryImage);
        }

        private IInstance Create(IRegistryImage registryImage)
        {
            var instance = new Instance(registryImage.ImageName, _images);
            _store.Add(instance);
            return instance;
        }
    }
}
