﻿using Hayate.Core.Instances;
using Hayate.Instances;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class InstancesServiceCollectionExtensions
    {
        public static void AddInstanceServices(this IServiceCollection services)
        {
            services.AddSingleton<IInstanceStore, InstanceStore>();
            services.AddTransient<IInstanceRegistry, InstanceRegistry>();
        }
    }
}
