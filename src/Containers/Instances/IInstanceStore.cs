﻿using Hayate.Core.Instances;

namespace Hayate.Instances
{
    public interface IInstanceStore
    {
        IInstance Find(InstanceId id);
        void Add(Instance instance);
    }
}