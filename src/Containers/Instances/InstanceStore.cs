﻿using System.Collections.Generic;
using Hayate.Core.Instances;

namespace Hayate.Instances
{
    public sealed class InstanceStore : IInstanceStore
    {
        private readonly IDictionary<InstanceId, Instance> _instances = new Dictionary<InstanceId, Instance>();

        public IInstance Find(InstanceId id)
        {
            if (_instances.TryGetValue(id, out var result)) return result;
            return null;
        }

        public void Add(Instance instance)
        {
            _instances.Add(instance.Id, instance);
        }
    }
}
