using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Microsoft.AspNetCore.Http;

namespace Hayate.Web.DockerRegistry
{
    /// <summary>
    ///     Docker Registry 用のパスワード認証をスコープ内で取得できるようにするミドルウェア。
    ///     <see cref="DockerRegistryPasswordCredentialScope.CurrentCredential" /> で認証情報を取得できるようになります。
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class DockerRegistryPasswordCredentialMiddleware
    {
        private readonly RequestDelegate _next;

        public DockerRegistryPasswordCredentialMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IDockerRegistryPasswordCredentialStore store)
        {
            using (DockerRegistryPasswordCredentialScope.NewScope(store))
            {
                await _next(context);
            }
        }
    }
}
