﻿using System;
using System.Threading.Tasks;
using Hayate.Containers;
using Microsoft.AspNetCore.Http;

namespace Hayate.Web.RequestAuthenticate
{
    /// <summary>
    ///     認証が必要な場合、GitLab PAT を設定するように促すするミドルウェア。
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class RequestAuthenticateMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestAuthenticateMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (RequireAuthenticateException.Contains(ex))
                {
                    context.Response.Redirect("/Errors/CannotAccessApplication");
                }
                else
                {
                    throw;
                }
            }
        }
    }
}
