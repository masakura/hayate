﻿using Hayate.Web.RequestAuthenticate;
using Microsoft.AspNetCore.Builder;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class RequestAuthenticateServiceCollectionExtensions
    {
        public static void UseRequestAuthenticate(this IApplicationBuilder app)
        {
            app.UseMiddleware<RequestAuthenticateMiddleware>();
        }
    }
}
