﻿using System.Threading.Tasks;
using Hayate.Core.Applications;
using Hayate.Core.Instances;
using Hayate.Core.Projects;
using Microsoft.AspNetCore.Mvc;
using Environment = Hayate.Core.Applications.Environment;

namespace Hayate.Web.Controllers
{
    public sealed class RedirectController : Controller
    {
        private readonly IApplicationRegistry _applications;

        public RedirectController(IApplicationRegistry applications)
        {
            _applications = applications;
        }

        public async Task<IActionResult> FromTargetId(string targetId, string environmentName)
        {
            var environment =
                await FromTargetIdAsync(new TargetProjectId(targetId), new EnvironmentName(environmentName));

            return RedirectToEnvironment(environment);
        }

        public async Task<IActionResult> FromInstanceId(string instanceId)
        {
            var environment = await FromInstanceId(new InstanceId(instanceId));

            return RedirectToEnvironment(environment);
        }

        private IActionResult RedirectToEnvironment(Environment environment)
        {
            return RedirectToAction(nameof(ApplicationsController.Preview), "Applications", new
            {
                environment.Application.Id,
                EnvironmentName = environment.Name
            });
        }

        private async Task<Environment> FromTargetIdAsync(TargetProjectId targetId, EnvironmentName environmentName)
        {
            return await _applications.FindEnvironmentAsync(targetId, environmentName);
        }

        private async Task<Environment> FromInstanceId(InstanceId instanceId)
        {
            return await _applications.FindEnvironmentAsync(instanceId);
        }
    }
}