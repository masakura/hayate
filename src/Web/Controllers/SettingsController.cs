﻿using System.Threading.Tasks;
using Hayate.Core.Tokens;
using Hayate.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    [Authorize]
    public sealed class SettingsController : Controller
    {
        private readonly IVerifiedGitLabPatRegistrationService _pats;

        public SettingsController(IVerifiedGitLabPatRegistrationService pats)
        {
            _pats = pats;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> PersonalAccessToken()
        {
            var token = await _pats.LoadVerifiedPatAsync();

            return View(new PersonalAccessTokenSettings
            {
                PersonalAccessToken = token.Token.ToString(),
                Verified = token.Verified,
                GitLabSettingsUri = "http://gitlab.hayate.example.jp/profile/personal_access_tokens"
            });
        }

        [HttpPost]
        public async Task<IActionResult> PersonalAccessToken(PersonalAccessTokenSettings settings)
        {
            await _pats.SavePatAsync(new PersonalAccessToken(settings.PersonalAccessToken));
            return RedirectToAction("PersonalAccessToken");
        }
    }
}
