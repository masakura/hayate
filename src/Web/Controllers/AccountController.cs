﻿using System.Threading.Tasks;
using Hayate.Authentication.GitLab;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        public IActionResult Login(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated) return this.RedirectToLocal(returnUrl);

            ViewData["returnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LoginGitLab(string returnUrl = null)
        {
            var properties = new AuthenticationProperties();
            properties.RedirectUri = Url.Action(nameof(LoginGitlLabCallback), "Account", new {returnUrl});

            return Challenge(properties, GitLabAuthenticationDefaults.AuthenticationScheme);
        }

        public IActionResult LoginGitlLabCallback(string returnUrl = null)
        {
            return this.RedirectToLocal(returnUrl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync();
            }

            return this.RedirectToLocal();
        }
    }
}