﻿using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    public sealed class ErrorsController : Controller
    {
        public IActionResult CannotAccessApplication()
        {
            return View();
        }
    }
}
