﻿using System.Threading.Tasks;
using Hayate.Core.Projects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    [Authorize]
    public sealed class RegistrationsController : Controller
    {
        private readonly IProjectRegistry _projects;

        public RegistrationsController(IProjectRegistry projects)
        {
            _projects = projects;
        }

        public async Task<IActionResult> Index()
        {
            var projects = await _projects.ListAsync();
            return View(projects);
        }

        [HttpPost]
        public async Task<IActionResult> Project(string id)
        {
            var project = await FindOrCreateAsync(id);
            return RedirectToAction("Index", "Applications", new {project.Id});
        }

        private async Task<Project> FindOrCreateAsync(string targetId)
        {
            return await _projects.FindOrRegisterAsync(new TargetProjectId(targetId));
        }
    }
}