﻿using Hayate.Core;
using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    public static class ControllerExtensions
    {
        public static IActionResult RedirectToLocal(this Controller controller, string urlExcludeHost = null)
        {
            if (urlExcludeHost != null && controller.Url.IsLocalUrl(urlExcludeHost))
                return controller.Redirect(urlExcludeHost);

            return controller.RedirectToAction("Index", "Home");
        }

        public static IActionResult ViewOrNotFound(this Controller controller, INotFound model)
        {
            if (model.IsNotFound) return controller.NotFound();

            return controller.View(model);
        }

        public static IActionResult ViewOrNotFound(this Controller controller, string viewName, INotFound model)
        {
            if (model.IsNotFound) return controller.NotFound();

            return controller.View(viewName, model);
        }
    }
}