﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Applications;
using Hayate.Core.Instances;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ApplicationId = Hayate.Core.Applications.ApplicationId;
using Environment = Hayate.Core.Applications.Environment;

namespace Hayate.Web.Controllers
{
    [Authorize]
    public sealed class ApplicationsController : Controller
    {
        private readonly IApplicationRegistry _applications;

        public ApplicationsController(IApplicationRegistry applications)
        {
            _applications = applications;
        }

        [HttpGet]
        public async Task<IActionResult> Index(Guid? id)
        {
            if (id == null)
            {
                var applications = await _applications.ListAsync();
                return View(applications);
            }

            return await ApplicationAsync(id.Value);
        }

        private async Task<IActionResult> ApplicationAsync(Guid id)
        {
            var application = await _applications.FindAsync(new ApplicationId(id));

            return this.ViewOrNotFound("Item", application);
        }

        [HttpGet]
        public async Task<IActionResult> Preview(Guid id, string environmentName)
        {
            var environment = await FindEnvironmentAsync(id, environmentName);

            return this.ViewOrNotFound(environment);
        }

        [HttpPost]
        [ActionName("Preview")]
        public async Task<IActionResult> PostPreview(Guid id, string environmentName, InstanceTransition transition)
        {
            var environment = await FindEnvironmentAsync(id, environmentName);
            environment.Transit(transition);

            if (environment.IsNotFound) return NotFound();

            return RedirectToProxy(environment);
        }

        private IActionResult RedirectToProxy(Environment environment)
        {
            // ToDo EnvironmentId と InstanceId をごっちゃにするのは良くないかも...
            return this.RedirectToLocal($"/_/proxy/{environment.Id.ToString()}");
        }

        private Task<Environment> FindEnvironmentAsync(Guid id, string environmentName)
        {
            return _applications.FindEnvironmentAsync(new ApplicationId(id),
                new EnvironmentName(environmentName));
        }
    }
}
