﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Hayate.Web.Controllers
{
    [AllowAnonymous]
    public sealed class GitLabCiTemplateController : Controller
    {
        [Route("/.gitlab-ci-hayate.yml")]
        public IActionResult Index()
        {
            return PartialView();
        }
    }
}
