﻿namespace Hayate.Web.Models
{
    public sealed class DockerImageFullNameInput
    {
        public string Id { get; set; }
    }
}