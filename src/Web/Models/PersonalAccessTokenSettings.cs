﻿using System.ComponentModel.DataAnnotations;

namespace Hayate.Web.Models
{
    public sealed class PersonalAccessTokenSettings
    {
        [Required] public string PersonalAccessToken { get; set; }

        public string GitLabSettingsUri { get; set; }
        public bool Verified { get; set; }
    }
}
