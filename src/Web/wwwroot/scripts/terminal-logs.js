import {WebSocketFactory} from './terminal-base.js';

export class LogsWebSocketFactory extends WebSocketFactory {
    get type() {
        return 'logs';
    }

    get path() {
        return `logs`;
    }
}
