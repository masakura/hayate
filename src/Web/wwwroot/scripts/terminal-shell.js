import {WebSocketFactory} from './terminal-base.js';

export class ShellWebSocketFactory extends WebSocketFactory {
    get type() {
        return 'shell';
    }

    get path() {
        return `tty`;
    }
}
