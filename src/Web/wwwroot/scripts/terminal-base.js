export class WebSocketFactory {
    // noinspection JSMethodCanBeStatic
    /**
     * @return {string}
     */
    get type() {
        throw Error('require implement');
    }

    // noinspection JSMethodCanBeStatic
    /**
     * @return {string}
     */
    get path() {
        throw Error('require implement');
    }

    /**
     * @param {string} instanceId
     * @return {WebSocket}
     */
    create(instanceId) {
        const location = window.location;
        const obj = {
            protocol: location.protocol.replace('http', 'ws'),
            host: location.host,
            path: this.path,
        };
        const url = `${obj.protocol}${obj.host}/_/${obj.path}/${instanceId}`;

        return new WebSocket(url);
    }
}

export class WebSocketProvider {
    /**
     * @param {string} instanceId
     */
    constructor(instanceId) {
        this.instanceId = instanceId;
        this.providers = {};
    }

    /**
     * @param {WebSocketFactory} factory
     */
    register(factory) {
        this.providers[factory.type] = factory;
    }

    /**
     * @param type
     * @return {WebSocketFactory}
     */
    get(type) {
        return this.providers[type];
    }

    /**
     * @param {string} type
     * @return {WebSocket}
     */
    webSocket(type) {
        return this.get(type).create(this.instanceId);
    }
}
