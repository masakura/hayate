import {WebSocketProvider} from './terminal-base.js';
import {LogsWebSocketFactory} from './terminal-logs.js';
import {ShellWebSocketFactory} from './terminal-shell.js';

const webSocketProvider = new WebSocketProvider(document.querySelector('#instance-id').value);
webSocketProvider.register(new LogsWebSocketFactory());
webSocketProvider.register(new ShellWebSocketFactory());

class TerminalIdentity {
    /**
     * @param {number} number
     */
    constructor(number) {
        this.number = number;
    }

    /**
     * @returns {string}
     */
    asHref() {
        return `#${this.asId()}`;
    }

    /**
     * @returns {string}
     */
    asId() {
        return `tty${this.number}`;
    }

    /**
     * @return {string}
     */
    asLabel() {
        return `TTY#${this.number}`;
    }

    /**
     * @return {TerminalIdentity}
     */
    next() {
        return new TerminalIdentity(this.number + 1);
    }
}

class TemplateSource {
    constructor() {
        this.templates = {
            tab: document.querySelector('#tab-item-template'),
            pane: document.querySelector('#tab-page-template'),
        };
    }

    /**
     * @param {TerminalIdentity} id
     * @return {*|jQuery|HTMLElement}
     */
    tab(id) {
        const clone = $(this.templates.tab.innerHTML);
        clone.find('.nav-link').attr('href', id.asHref()).text(id.asLabel());
        return clone;
    }

    /**
     * @param {TerminalIdentity} id
     * @return {*|jQuery|HTMLElement}
     */
    pane(id) {
        const clone = $(this.templates.pane.innerHTML);
        clone.attr('id', id.asId());
        return clone;
    }
}

class Template {
    /**
     * @param {TerminalIdentity} id
     * @param {TemplateSource} source
     */
    constructor(id, source) {
        this.id = id;
        this.source = source;
    }

    /**
     * @return {*|jQuery|HTMLElement}
     */
    tab() {
        return this.source.tab(this.id);
    }

    /**
     * @return {*|jQuery|HTMLElement}
     */
    pane() {
        return this.source.pane(this.id);
    }
}

class TemplateFactory {
    constructor() {
        this.id = new TerminalIdentity(1);
        this.source = new TemplateSource();
    }

    create() {
        const template = new Template(this.id, this.source);
        this.id = this.id.next();
        return template;
    }
}

class TerminalControl {
    constructor(el) {
        this.el = el;
        this.terminal = new Terminal();
        this.terminal.open(el);
    }

    /**
     * @param {WebSocketProvider} webSocketProvider
     */
    attach(webSocketProvider) {
        const webSocket = webSocketProvider.webSocket(this.el.dataset.type);
        this.terminal.attach(webSocket);
    }
}

class TerminalList {
    constructor(el) {
        this.el = el;
    }

    /**
     * @param {Template} template
     */
    add(template) {
        this.el.querySelector('.nav-tabs').insertBefore(template.tab()[0], document.querySelector('.nav-item.plus'));
        this.el.querySelector('.tab-content').insertBefore(template.pane()[0], null);

        const terminal = new TerminalControl(this.el.querySelector(`${template.id.asHref()} .terminal`));
        terminal.attach(webSocketProvider);

        const tab = $(`[href="${template.id.asHref()}"]`);
        tab.tab('show');
    }
}

const terminalList = new TerminalList(document.querySelector('#terminal-list'));
const factory = new TemplateFactory();

{
    // show log terminal
    const terminal = new TerminalControl(document.querySelector('#logs .terminal'));

    document.querySelector('#start-instance-logs')
        .addEventListener('click', () => terminal.attach(webSocketProvider));
}

document.querySelector('#new-tty')
    .addEventListener('click', e => {
        terminalList.add(factory.create());

        e.preventDefault();
    });
