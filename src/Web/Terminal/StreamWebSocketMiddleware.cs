﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.StreamBinding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Hayate.Web.Terminal
{
    internal abstract class StreamWebSocketMiddleware
    {
        // ReSharper disable once UnusedParameter.Local
        protected StreamWebSocketMiddleware(RequestDelegate next)
        {
        }

        // ReSharper disable once UnusedMember.Global
        public async Task Invoke(HttpContext context, IInstanceRegistry instances)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                context.Response.StatusCode = 400;
                return;
            }

            var instanceId = GetInstanceId(context);

            var instance = instances.Find(instanceId);
            var stream = await StreamAsync(instance);

            var webSocket = await AcceptWebSocket(context);

            await stream.BindAsync(webSocket);
        }

        protected abstract Task<ISource> StreamAsync(IInstance instance);

        private static async Task<WebSocketDuplex> AcceptWebSocket(HttpContext context)
        {
            var webSocket = await context.WebSockets.AcceptWebSocketAsync();
            return new WebSocketDuplex(webSocket);
        }

        private static InstanceId GetInstanceId(HttpContext context)
        {
            var id = context.GetRouteValue("instanceId").ToString();
            return new InstanceId(id);
        }
    }
}
