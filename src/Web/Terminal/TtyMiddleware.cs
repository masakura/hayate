﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.StreamBinding;
using Microsoft.AspNetCore.Http;

namespace Hayate.Web.Terminal
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class TtyMiddleware : StreamWebSocketMiddleware
    {
        public TtyMiddleware(RequestDelegate next) : base(next)
        {
        }

        protected override async Task<ISource> StreamAsync(IInstance instance)
        {
            return await instance.TtyAsync();
        }
    }
}
