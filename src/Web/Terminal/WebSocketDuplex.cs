﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Web.Terminal
{
    internal sealed class WebSocketDuplex : ISource, IDestination, IDisposable
    {
        private readonly WebSocket _webSocket;
        private readonly byte[] _buffer;

        public WebSocketDuplex(WebSocket webSocket)
        {
            _webSocket = webSocket;
            _buffer = new byte[1024 * 1024];
        }

        public bool IsClosed => _webSocket.CloseStatus.HasValue;

        public async Task WriteAsync(string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            await _webSocket.SendAsync(new ArraySegment<byte>(bytes),
                WebSocketMessageType.Text, 
                true,
                CancellationToken.None);
        }

        public async Task<string> ReadAsync()
        {
            var result = await _webSocket.ReceiveAsync(new ArraySegment<byte>(_buffer), CancellationToken.None);
            return Encoding.UTF8.GetString(_buffer, 0, result.Count);
        }

        public void Dispose()
        {
            _webSocket?.Dispose();
        }
    }
}