﻿using Hayate.Containers;
using Hayate.Proxy.Applications;
using Hayate.Web.DockerRegistry;
using Hayate.Web.Terminal;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Routing;
using Microsoft.Dependency.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hayate.Web
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // ReSharper disable once UnusedMember.Global
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDockerImplements(_configuration);
            services.AddContainerServices();
            services.AddInstanceServices();
            services.AddApplicationProxyServices();
            services.AddProvider(_configuration);

            services.AddHayateCoreServices();
            services.AddInfrastructureServices(options => options.UseInMemoryDatabase("Hayate"));

            services.AddMvc();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                })
                .AddCookie()
                .AddGitLab(_configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // ReSharper disable once UnusedMember.Global
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseForwardedHeaders(CreateForwardedHeadersOptions());

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseWebSockets();

            app.UseRequestAuthenticate();

            app.UseMiddleware<DockerRegistryPasswordCredentialMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapMiddlewareRoute("_/proxy/{instanceId}/{*all}",
                    builder => builder.UseMiddleware<ApplicationMiddleware>());
                routes.MapMiddlewareRoute("_/logs/{instanceId}",
                    builder => builder.UseMiddleware<LogsMiddleware>());
                routes.MapMiddlewareRoute("_/tty/{instanceId}",
                    builder => builder.UseMiddleware<TtyMiddleware>());
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private static ForwardedHeadersOptions CreateForwardedHeadersOptions()
        {
            var options = new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            };
            options.KnownNetworks.Clear();
            options.KnownProxies.Clear();
            return options;
        }
    }
}
