﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hayate.Provider.GitLab;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hayate.Authentication.GitLab
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class GitLabHttpClient : IGitLabHttpClient, IDisposable
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly GitLabProviderOptions _options;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<GitLabHttpClient> _logger;

        public GitLabHttpClient(IOptions<GitLabProviderOptions> options, IHttpContextAccessor httpContextAccessor, ILogger<GitLabHttpClient> logger)
        {
            _options = options.Value;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }

        public async Task<HttpResponseMessage> GetAsync(string requestUri)
        {
            var uri = _options.GetApiEndpoint(requestUri);

            _logger.LogInformation($"GET {uri}");

            var token = await _httpContextAccessor.HttpContext.GetTokenAsync("GitLab", "access_token");

            _client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse($"Bearer {token}");

            return await _client.GetAsync(uri);
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}