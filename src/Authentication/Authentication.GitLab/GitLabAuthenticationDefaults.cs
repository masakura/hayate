﻿namespace Hayate.Authentication.GitLab
{
    // ReSharper disable UnusedMember.Global
    public static class GitLabAuthenticationDefaults
    {
        public static readonly string AuthenticationScheme = "GitLab";
    }
}