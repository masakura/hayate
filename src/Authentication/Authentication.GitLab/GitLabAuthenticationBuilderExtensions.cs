﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using Hayate.Authentication.GitLab;
using Hayate.Provider.GitLab;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class GitLabAuthenticationBuilderExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static AuthenticationBuilder AddGitLab(this AuthenticationBuilder builder,
            IConfiguration configuration)
        {
            return builder.AddGitLab(configuration, options => { });
        }

        // ReSharper disable once UnusedMethodReturnValue.Global
        public static AuthenticationBuilder AddGitLab(this AuthenticationBuilder builder, IConfiguration configuration,
            Action<OAuthOptions> configureOptions)
        {
            builder.Services.AddTransient<IGitLabHttpClient, GitLabHttpClient>();

            var fromSettings = GitLabProviderOptions.Create(configuration);

            return builder.AddOAuth("GitLab", options =>
            {
                options.ClientId = fromSettings.ClientId;
                options.ClientSecret = fromSettings.ClientSecret;
                options.AuthorizationEndpoint = fromSettings.AuthorizationEndpoint();
                options.TokenEndpoint = fromSettings.TokenEndpoint();
                options.UserInformationEndpoint = fromSettings.UserInfoEndpoint();
                options.CallbackPath = new PathString("/signin-gitlab");
                options.Scope.Add("api");
                options.Scope.Add("read_registry");
                options.Scope.Add("read_user");

                options.SaveTokens = true;

                options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                options.ClaimActions.MapJsonKey("urn:hayate:avatar", "avatar_url");
                options.ClaimActions.MapJsonKey("urn:hayate:profile", "web_url");

                options.Events.OnCreatingTicket = async context =>
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
                    request.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);

                    var response = await context.Backchannel.SendAsync(request,
                        HttpCompletionOption.ResponseHeadersRead, context.HttpContext.RequestAborted);
                    response.EnsureSuccessStatusCode();

                    var user = JObject.Parse(await response.Content.ReadAsStringAsync());

                    Console.WriteLine(user);

                    context.RunClaimActions(user);
                };

                configureOptions(options);
            });
        }
    }
}