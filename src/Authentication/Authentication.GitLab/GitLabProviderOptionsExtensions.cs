﻿using Hayate.Provider.GitLab;

namespace Hayate.Authentication.GitLab
{
    public static class GitLabProviderOptionsExtensions
    {
        public static string TokenEndpoint(this GitLabProviderOptions options)
        {
            return options.GetEndpoint("oauth/token");
        }
        public static string AuthorizationEndpoint(this GitLabProviderOptions options)
        {
            return options.GetEndpoint("oauth/authorize");
        }

        public static string UserInfoEndpoint(this GitLabProviderOptions options)
        {
            return options.GetApiEndpoint("user");
        }
    }
}