﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Hayate.Core
{
    /// <summary>
    ///     コレクションの基底クラス。読み取り専用です。
    /// </summary>
    /// <typeparam name="T">コレクションの要素の型。</typeparam>
    // ReSharper disable once InheritdocConsiderUsage
    public abstract class CoreCollection<T> : IEnumerable<T>
    {
        private readonly ReadOnlyCollection<T> _collection;

        protected CoreCollection(IEnumerable<T> collection)
        {
            _collection = collection.ToList().AsReadOnly();
        }

        /// <summary>
        ///     コレクションが空かどうかを取得します。
        /// </summary>
        public bool IsEmpty => !this.Any();

        /// <summary>
        ///     コレクションの要素の数を取得します。
        /// </summary>
        public int Count => _collection.Count;

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>) this).GetEnumerator();
        }
    }
}