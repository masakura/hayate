﻿namespace Hayate.Core
{
    public interface INotFound
    {
        bool IsNotFound { get; }
    }
}