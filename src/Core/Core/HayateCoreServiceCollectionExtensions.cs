﻿using Hayate.Core.Applications;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Projects;
using Hayate.Core.Tokens;
using Hayate.Core.Users;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class HayateCoreServiceCollectionExtensions
    {
        public static void AddHayateCoreServices(this IServiceCollection services)
        {
            services.AddCoreApplicationsServices();
            services.AddCoreProjectsServices();
            services.AddCoreImagesRegistryServices();
            services.AddCoreTokensServices();
            services.AddCoreUsersServices();
        }
    }
}
