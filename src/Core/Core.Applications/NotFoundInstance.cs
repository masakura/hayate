﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    internal sealed class NotFoundInstance : IInstance
    {
        private NotFoundInstance(InstanceId id)
        {
            Id = id;
        }

        public InstanceId Id { get; }
        public bool CanRestart { get; } = false;
        public bool CanTryUpgrade { get; } = false;

        public bool CanTransit(InstanceTransition transition)
        {
            throw new NotImplementedException();
        }

        public void Transit(InstanceTransition transition)
        {
            throw new NotImplementedException();
        }

        public Task<InstanceStatus> StatusAsync()
        {
            return Task.FromResult(InstanceStatus.Pending);
        }

        public Task<EndPoint> EndPointAsync()
        {
            return Task.FromResult(EndPoint.Empty);
        }

        public Task<LogsSource> LogsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<TtyStream> TtyAsync()
        {
            throw new NotImplementedException();
        }

        public void TransitRunning()
        {
            throw new InvalidOperationException();
        }

        public void TransitRestarted()
        {
            throw new InvalidOperationException();
        }

        public void TransitUpgraded()
        {
            throw new InvalidOperationException();
        }

        public Task RunAsync()
        {
            return Task.CompletedTask;
        }

        public static IInstance NotFound()
        {
            return NotFound(InstanceId.Empty);
        }

        public static IInstance NotFound(InstanceId id)
        {
            return new NotFoundInstance(id);
        }
    }
}
