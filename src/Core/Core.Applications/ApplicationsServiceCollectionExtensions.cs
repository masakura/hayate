﻿using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Hayate.Core.Applications
{
    internal static class ApplicationsServiceCollectionExtensions
    {
        public static void AddCoreApplicationsServices(this IServiceCollection services)
        {
            services.AddTransient<IApplicationRegistry, ApplicationRegistry>();
        }
    }
}
