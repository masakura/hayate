﻿using System;
using System.Threading.Tasks;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    /// <summary>
    ///     実行可能な環境。
    /// </summary>
    public sealed class Environment : INotFound
    {
        private readonly IRegistryImage _image;
        private readonly IInstance _instance;

        public Environment(IRegistryImage image, IInstance instance)
        {
            _image = image;
            _instance = instance;
        }

        /// <summary>
        ///     インスタンスの ID を取得します。
        /// </summary>
        public EnvironmentId Id => EnvironmentId.From(_instance.Id);

        /// <summary>
        ///     環境名を取得します。
        /// </summary>
        public EnvironmentName Name => _image.Name;

        /// <summary>
        ///     このインスタンスを所有するアプリケーションを取得します。
        /// </summary>
        public Application Application { get; private set; }

        /// <summary>
        ///     インスタンスが見つからなかった場合に true を返します。
        /// </summary>
        public bool IsNotFound => _image.IsNotFound;

        /// <summary>
        ///     インスタンスのステータスを取得します。
        /// </summary>
        /// <returns>インスタンスのステータス。</returns>
        public Task<InstanceStatus> StatusAsync()
        {
            return _instance.StatusAsync();
        }

        /// <summary>
        ///     インスタンスを操作可能かどうかを取得します。
        /// </summary>
        /// <param name="transition">操作。</param>
        /// <returns>指定された操作が可能かどうか。</returns>
        public bool CanTransit(InstanceTransition transition)
        {
            return _instance.CanTransit(transition);
        }

        /// <summary>
        ///     インスタンスを操作します。
        /// </summary>
        /// <param name="transition">操作。</param>
        public void Transit(InstanceTransition transition)
        {
            _instance.Transit(transition);
        }

        internal void Bind(Application application)
        {
            if (Application != null) throw new InvalidOperationException("Already binded。");
            Application = application;
        }

        public static Environment NotFound(InstanceId instanceId)
        {
            return new Environment(NotFoundRegistryImage.NotFound(), NotFoundInstance.NotFound(instanceId));
        }

        public static Environment NotFound(Application application, EnvironmentName environmentName)
        {
            var notFound = new Environment(NotFoundRegistryImage.NotFound(environmentName),
                NotFoundInstance.NotFound());
            notFound.Bind(application);
            return notFound;
        }
    }
}
