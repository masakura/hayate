﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    /// <summary>
    ///     アプリケーションを管理します。
    /// </summary>
    public interface IApplicationRegistry
    {
        /// <summary>
        ///     アプリケーションを取得します。
        /// </summary>
        /// <param name="id">アプリケーション ID。</param>
        /// <returns>アプリケーション。</returns>
        Task<Application> FindAsync(ApplicationId id);

        /// <summary>
        ///     アプリケーションを取得します。
        /// </summary>
        /// <param name="targetId">アプリケーションにリンクされている外部プロジェクトの ID。</param>
        /// <returns>アプリケーション。</returns>
        Task<Application> FindAsync(TargetProjectId targetId);

        /// <summary>
        ///     環境を取得します。
        /// </summary>
        /// <param name="instanceId">インスタンス ID。</param>
        /// <returns>環境。</returns>
        Task<Environment> FindEnvironmentAsync(InstanceId instanceId);

        /// <summary>
        ///     登録されているすべてのアプリケーションを取得します。
        /// </summary>
        /// <returns>アプリケーションのコレクション。</returns>
        Task<ApplicationCollection> ListAsync();
    }
}