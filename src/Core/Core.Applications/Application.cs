﻿using System.Collections.Generic;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    /// <summary>
    ///     アプリケーション。
    /// </summary>
    public sealed class Application : INotFound
    {
        private readonly Project _project;

        public Application(Project project, IEnumerable<Environment> instances)
        {
            _project = project;
            Environments = new EnvironmentCollection(instances);

            Environments.Bind(this);
        }

        /// <summary>
        ///     アプリケーション ID を取得します。
        /// </summary>
        public ApplicationId Id => new ApplicationId(_project.Id.ToGuid());

        /// <summary>
        ///     アプリケーション名を取得します。
        /// </summary>
        public ApplicationName Name => new ApplicationName(_project.Name.ToString());

        /// <summary>
        ///     アプリケーションに含まれる環境をすべて取得します。
        /// </summary>
        public EnvironmentCollection Environments { get; }

        /// <summary>
        ///     アプリケーションに関連付けられた外部プロジェクトの URL を取得します。
        /// </summary>
        public SiteUrl SiteUrl => _project.SiteUrl;

        /// <summary>
        ///     アプリケーションが見つからなかった場合は true を返します。
        /// </summary>
        public bool IsNotFound => _project.IsNotFound;
    }
}