﻿using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    /// <summary>
    ///     環境 ID。
    /// </summary>
    public struct EnvironmentId
    {
        private readonly string _value;

        private EnvironmentId(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static EnvironmentId From(InstanceId instanceId)
        {
            return new EnvironmentId(instanceId.ToString());
        }
    }
}