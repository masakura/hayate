﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationRegistry : IApplicationRegistry
    {
        private readonly EnvironmentAdapter _environments;
        private readonly ProjectRegistryAdapter _projects;

        public ApplicationRegistry(IProjectRegistry projects,
            IRegistryImageRegistry images,
            IInstanceRegistry instances)
        {
            _projects = new ProjectRegistryAdapter(projects);
            _environments = new EnvironmentAdapter(images, instances);
        }

        public async Task<Application> FindAsync(ApplicationId id)
        {
            var project = await _projects.FindAsync(id);
            return await ApplicationAsync(project);
        }

        public async Task<Application> FindAsync(TargetProjectId targetId)
        {
            var project = await _projects.FindAsync(targetId);
            return await ApplicationAsync(project);
        }

        public async Task<Environment> FindEnvironmentAsync(InstanceId instanceId)
        {
            // TODO 線形サーチは重すぎるので考える必要あり
            var applications = await ListAsync();
            return applications.FindEnvironment(instanceId);
        }

        public async Task<ApplicationCollection> ListAsync()
        {
            var projects = await _projects.ListRegisteredAsync();
            var applications = projects
                .Select(async project => await ApplicationAsync(project))
                .Select(result => result.Result);

            return new ApplicationCollection(applications);
        }

        private async Task<Application> ApplicationAsync(Project project)
        {
            var instances = await _environments.FindEnvironments(project);

            return new Application(project, instances);
        }

        private sealed class EnvironmentAdapter
        {
            private readonly IRegistryImageRegistry _images;
            private readonly IInstanceRegistry _instances;

            public EnvironmentAdapter(IRegistryImageRegistry images, IInstanceRegistry instances)
            {
                _images = images;
                _instances = instances;
            }

            public async Task<IEnumerable<Environment>> FindEnvironments(Project project)
            {
                var images = await _images.FindAsync(project.RepositoryName);

                return images.Select(image => FindOrCreateEnvironment(image));
            }

            private Environment FindOrCreateEnvironment(IRegistryImage image)
            {
                var instance = _instances.FindOrCreate(image);
                return new Environment(image, instance);
            }
        }

        private sealed class ProjectRegistryAdapter
        {
            private readonly IProjectRegistry _projects;

            public ProjectRegistryAdapter(IProjectRegistry projects)
            {
                _projects = projects;
            }

            public async Task<Project> FindAsync(ApplicationId id)
            {
                return await _projects.FindAsync(new ProjectId(id.ToGuid()));
            }

            public async Task<Project> FindAsync(TargetProjectId targetId)
            {
                return await _projects.FindAsync(targetId);
            }

            public async Task<ProjectCollection> ListRegisteredAsync()
            {
                return await _projects.ListRegisteredAsync();
            }
        }
    }
}