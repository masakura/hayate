﻿namespace Hayate.Core.Applications
{
    public struct ApplicationName
    {
        private readonly string _value;

        public ApplicationName(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}