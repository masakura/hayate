﻿using System;

namespace Hayate.Core.Applications
{
    public struct ApplicationId
    {
        private readonly Guid _value;

        public ApplicationId(Guid value)
        {
            _value = value;
        }

        public bool IsEmpty => _value == Guid.Empty;

        public Guid ToGuid()
        {
            return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}