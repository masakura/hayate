﻿namespace Hayate.Core.Applications
{
    /// <summary>
    ///     環境名。
    /// </summary>
    public struct EnvironmentName
    {
        private readonly string _value;

        public EnvironmentName(string value)
        {
            _value = value;
        }

        public static EnvironmentName Empty { get; } = default(EnvironmentName);

        public override string ToString()
        {
            return _value.ToLower();
        }
    }
}