﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    public static class ApplicationEnumerableExtensions
    {
        public static Environment FindEnvironment(this IEnumerable<Application> collection, InstanceId instanceId)
        {
            return collection.AllEnvironments().Find(instanceId);
        }

        private static IEnumerable<Environment> AllEnvironments(this IEnumerable<Application> collection)
        {
            return collection.SelectMany(application => application.Environments);
        }
    }
}