﻿using System.Collections.Generic;

namespace Hayate.Core.Applications
{
    public sealed class ApplicationCollection : CoreCollection<Application>
    {
        public ApplicationCollection(IEnumerable<Application> collection) : base(collection)
        {
        }
    }
}