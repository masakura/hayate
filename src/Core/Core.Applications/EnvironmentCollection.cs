﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Core.ImagesRegistry;

namespace Hayate.Core.Applications
{
    public sealed class EnvironmentCollection : CoreCollection<Environment>
    {
        private Application _application;

        public EnvironmentCollection(IEnumerable<Environment> collection) : base(collection)
        {
        }

        internal void Bind(Application application)
        {
            _application = application;

            foreach (var environment in this) environment.Bind(application);
        }

        public Environment Find(EnvironmentName name)
        {
            return this.FirstOrDefault(environment => environment.Name.Equals(name)) ?? NotFound(name);
        }

        private Environment NotFound(EnvironmentName name)
        {
            return Environment.NotFound(_application, name);
        }
    }
}