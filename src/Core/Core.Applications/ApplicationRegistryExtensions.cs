﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Core.Projects;

namespace Hayate.Core.Applications
{
    public static class ApplicationRegistryExtensions
    {
        /// <summary>
        ///     アプリケーションに含まれるインスタンスを取得します。
        /// </summary>
        /// <param name="applications"><see cref="IApplicationRegistry" /> インスタンス。</param>
        /// <param name="id">アプリケーションの ID。</param>
        /// <param name="environmentName">環境名。</param>
        /// <returns>環境。</returns>
        public static async Task<Environment> FindEnvironmentAsync(
            this IApplicationRegistry applications,
            ApplicationId id, EnvironmentName environmentName)
        {
            var application = await applications.FindAsync(id);
            return application.Environments.Find(environmentName);
        }

        /// <summary>
        ///     アプリケーションに含まれるインスタンスを取得します。
        /// </summary>
        /// <param name="applications"><see cref="IApplicationRegistry" /> インスタンス。</param>
        /// <param name="targetId">アプリケーションにリンクされている外部プロジェクトの ID。</param>
        /// <param name="environmentName">環境名。</param>
        /// <returns>環境。</returns>
        public static async Task<Environment> FindEnvironmentAsync(this IApplicationRegistry applications,
            TargetProjectId targetId, EnvironmentName environmentName)
        {
            var application = await applications.FindAsync(targetId);
            return application.Environments.Find(environmentName);
        }

        /// <summary>
        ///     起動済みまたは起動を開始したインスタンスを取得します。
        /// </summary>
        /// <param name="applications"><see cref="IApplicationRegistry" /> インスタンス。</param>
        /// <param name="id">アプリケーションの ID。</param>
        /// <param name="environmentName">環境名。</param>
        /// <returns>環境。</returns>
        public static async Task<Environment> FindRunningEnvironmentAsync(
            this IApplicationRegistry applications,
            ApplicationId id, EnvironmentName environmentName)
        {
            var instance = await applications.FindEnvironmentAsync(id, environmentName);

            instance.Transit(InstanceTransition.Running);

            return instance;
        }
    }
}
