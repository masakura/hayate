﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Core.Instances;

namespace Hayate.Core.Applications
{
    public static class EnvironmentEnumerableExtensions
    {
        public static Environment Find(this IEnumerable<Environment> collection, InstanceId instanceId)
        {
            return collection.FirstOrDefault(environment => environment.Id.Equals(EnvironmentId.From(instanceId))) ??
                   NotFound(instanceId);
        }

        private static Environment NotFound(InstanceId instanceId)
        {
            return Environment.NotFound(instanceId);
        }
    }
}