﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Core.ImagesRegistry
{
    public interface IRegistryImageRepository
    {
        Task<IEnumerable<IImageName>> FindAsync(IRepositoryName repositoryName);
        Task<IRegistryImage> FindAsync(IImageName imageName);
    }
}