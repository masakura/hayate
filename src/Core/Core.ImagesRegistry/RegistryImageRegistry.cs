﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Core.ImagesRegistry
{
    public sealed class RegistryImageRegistry : IRegistryImageRegistry
    {
        private readonly IRegistryImageRepository _images;

        public RegistryImageRegistry(IRegistryImageRepository images)
        {
            _images = images;
        }

        public async Task<IEnumerable<IRegistryImage>> FindAsync(IRepositoryName repositoryName)
        {
            var tags = await _images.FindAsync(repositoryName);
            return tags.Select(async tag => await _images.FindAsync(tag))
                .Select(result => result.Result);
        }
    }
}