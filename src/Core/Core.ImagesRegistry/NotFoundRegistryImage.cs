﻿using Hayate.Core.Applications;
using Hayate.Core.Instances;

namespace Hayate.Core.ImagesRegistry
{
    public sealed class NotFoundRegistryImage : IRegistryImage
    {
        private NotFoundRegistryImage(EnvironmentName name)
        {
            Name = name;
        }

        public EnvironmentName Name { get; }
        public IImageName ImageName { get; } = null;
        public bool IsNotFound { get; } = true;

        public static IRegistryImage NotFound()
        {
            return NotFound(EnvironmentName.Empty);
        }

        public static IRegistryImage NotFound(EnvironmentName name)
        {
            return new NotFoundRegistryImage(name);
        }
    }
}