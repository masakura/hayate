﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Core.ImagesRegistry
{
    /// <summary>
    ///     Image Registry のイメージを管理します。
    /// </summary>
    public interface IRegistryImageRegistry
    {
        /// <summary>
        ///     リポジトリに含まれるすべてのイメージを取得します。
        /// </summary>
        /// <param name="repositoryName">リポジトリ名。</param>
        /// <returns>イメージのコレクション。</returns>
        Task<IEnumerable<IRegistryImage>> FindAsync(IRepositoryName repositoryName);
    }
}