﻿using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Hayate.Core.ImagesRegistry
{
    internal static class ImagesRegistryServiceCollectionExtensions
    {
        public static void AddCoreImagesRegistryServices(this IServiceCollection services)
        {
            services.AddTransient<IRegistryImageRegistry, RegistryImageRegistry>();
        }
    }
}