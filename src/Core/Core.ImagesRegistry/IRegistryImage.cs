﻿using Hayate.Core.Applications;
using Hayate.Core.Instances;

namespace Hayate.Core.ImagesRegistry
{
    public interface IRegistryImage
    {
        EnvironmentName Name { get; }
        IImageName ImageName { get; }
        bool IsNotFound { get; }
    }
}