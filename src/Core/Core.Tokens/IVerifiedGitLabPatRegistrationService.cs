﻿using System.Threading.Tasks;

namespace Hayate.Core.Tokens
{
    public interface IVerifiedGitLabPatRegistrationService
    {
        Task SavePatAsync(PersonalAccessToken token);
        Task<VerifiedPersonalAccessToken> LoadVerifiedPatAsync();
    }
}
