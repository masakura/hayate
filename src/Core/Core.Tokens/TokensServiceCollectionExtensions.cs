﻿using Microsoft.Extensions.DependencyInjection;

namespace Hayate.Core.Tokens
{
    internal static class TokensServiceCollectionExtensions
    {
        public static void AddCoreTokensServices(this IServiceCollection services)
        {
            services.AddTransient<IGitLabPatRegistrationService, GitLabPatRegistrationService>();
            services.AddTransient<IVerifiedGitLabPatRegistrationService, VerifiedGitLabPatRegistrationService>();
        }
    }
}
