﻿namespace Hayate.Core.Tokens
{
    public struct PersonalAccessToken
    {
        private readonly string _value;

        public PersonalAccessToken(string value)
        {
            _value = value;
        }

        public static PersonalAccessToken Empty { get; } = new PersonalAccessToken();

        public bool IsEmpty => _value == null;

        public override string ToString()
        {
            return _value;
        }
    }
}
