﻿using System.Threading.Tasks;
using Hayate.Core.Users;

namespace Hayate.Core.Tokens
{
    public interface IPatRepository
    {
        Task SaveAsync(UserId userId, PersonalAccessToken token);
        Task<PersonalAccessToken> LoadAsync(UserId userId);
        PersonalAccessToken Load(UserId userId);
    }
}
