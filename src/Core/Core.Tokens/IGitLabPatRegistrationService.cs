﻿using System.Threading.Tasks;

namespace Hayate.Core.Tokens
{
    public interface IGitLabPatRegistrationService
    {
        Task SavePatAsync(PersonalAccessToken token);
        Task<PersonalAccessToken> LoadPatAsync();
        PersonalAccessToken LoadPat();
    }
}
