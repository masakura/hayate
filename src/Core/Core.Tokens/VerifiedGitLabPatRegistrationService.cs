﻿using System.Threading.Tasks;

namespace Hayate.Core.Tokens
{
    /// <summary>
    /// PAT 検証機能付きの PAT 登録クラス。
    /// </summary>
    public sealed class VerifiedGitLabPatRegistrationService : IVerifiedGitLabPatRegistrationService
    {
        private readonly IGitLabPatRegistrationService _target;
        private readonly IGitLabPatVerifyService _verifyService;

        public VerifiedGitLabPatRegistrationService(IGitLabPatRegistrationService target,
            IGitLabPatVerifyService verifyService)
        {
            _target = target;
            _verifyService = verifyService;
        }

        public Task SavePatAsync(PersonalAccessToken token)
        {
            return _target.SavePatAsync(token);
        }

        public async Task<VerifiedPersonalAccessToken> LoadVerifiedPatAsync()
        {
            var token = await _target.LoadPatAsync();
            var verified = await VerifyAsync(token);
            return new VerifiedPersonalAccessToken(token, verified);
        }

        private async Task<bool> VerifyAsync(PersonalAccessToken token)
        {
            if (token.IsEmpty) return false;

            return await _verifyService.VerfyAsync(token);
        }
    }
}
