﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Users;

namespace Hayate.Core.Tokens
{
    public sealed class GitLabPatRegistrationService : IGitLabPatRegistrationService
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IPatRepository _pats;

        public GitLabPatRegistrationService(ICurrentUserService currentUserService, IPatRepository pats)
        {
            _currentUserService = currentUserService;
            _pats = pats;
        }

        public async Task SavePatAsync(PersonalAccessToken token)
        {
            var currentUserId = GetCurrentUserId();
            if (currentUserId.IsAnonymouse) throw new InvalidOperationException("Invalid user or anonymouse");
            await _pats.SaveAsync(currentUserId, token);
        }

        public async Task<PersonalAccessToken> LoadPatAsync()
        {
            var currentUserId = GetCurrentUserId();
            if (currentUserId.IsAnonymouse) return PersonalAccessToken.Empty;
            return await _pats.LoadAsync(currentUserId);
        }

        public PersonalAccessToken LoadPat()
        {
            var currentUserId = GetCurrentUserId();
            if (currentUserId.IsAnonymouse) return PersonalAccessToken.Empty;
            return _pats.Load(currentUserId);
        }

        private UserId GetCurrentUserId()
        {
            return _currentUserService.GetCurrentUser().UserId;
        }
    }
}
