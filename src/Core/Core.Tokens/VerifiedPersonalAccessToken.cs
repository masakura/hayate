﻿namespace Hayate.Core.Tokens
{
    public struct VerifiedPersonalAccessToken
    {
        public PersonalAccessToken Token { get; }
        public bool Verified { get; }

        public VerifiedPersonalAccessToken(PersonalAccessToken token, bool verified)
        {
            Token = token;
            Verified = verified;
        }
    }
}