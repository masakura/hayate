﻿using System.Threading.Tasks;

namespace Hayate.Core.Tokens
{
    public interface IGitLabPatVerifyService
    {
        Task<bool> VerfyAsync(PersonalAccessToken token);
    }
}
