﻿using Microsoft.Extensions.DependencyInjection;

namespace Hayate.Core.Users
{
    internal static class UsersServiceCollectionExtensions
    {
        public static void AddCoreUsersServices(this IServiceCollection services)
        {
            services.AddTransient<ICurrentUserService, CurrentUserService>();
        }
    }
}
