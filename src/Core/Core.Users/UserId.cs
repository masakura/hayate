﻿namespace Hayate.Core.Users
{
    public struct UserId
    {
        private readonly string _value;

        public UserId(string value)
        {
            _value = value;
        }

        public static UserId Anonymouse { get; } = new UserId();
        public bool IsAnonymouse => _value == null;

        public override string ToString()
        {
            return _value;
        }
    }
}
