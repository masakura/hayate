﻿namespace Hayate.Core.Users
{
    public interface ICurrentUserService
    {
        User GetCurrentUser();
    }
}
