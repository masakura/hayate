﻿using Microsoft.AspNetCore.Http;

namespace Hayate.Core.Users
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public User GetCurrentUser()
        {
            return User.From(_httpContextAccessor.HttpContext.User);
        }
    }
}
