﻿using System.Security.Claims;

namespace Hayate.Core.Users
{
    public sealed class User
    {
        public User(UserId userId)
        {
            UserId = userId;
        }

        public UserId UserId { get; }
        public bool IsAnonymouse => UserId.IsAnonymouse;

        public static User Anonymouse { get; } = new User(UserId.Anonymouse);

        public static User From(ClaimsPrincipal principal)
        {
            var value = principal.FindFirst(ClaimTypes.NameIdentifier);
            return value != null ? new User(new UserId(value.Value)) : Anonymouse;
        }
    }
}
