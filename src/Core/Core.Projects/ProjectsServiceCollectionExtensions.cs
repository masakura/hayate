﻿using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Hayate.Core.Projects
{
    internal static class ProjectsServiceCollectionExtensions
    {
        public static void AddCoreProjectsServices(this IServiceCollection services)
        {
            services.AddTransient<IProjectRegistry, ProjectRegistry>();
        }
    }
}