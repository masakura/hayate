﻿using System.Collections.Generic;

namespace Hayate.Core.Projects
{
    internal static class ProjectEnumerableExtensions
    {
        public static ProjectCollection ToProjects(this IEnumerable<Project> collection)
        {
            return new ProjectCollection(collection);
        }
    }
}