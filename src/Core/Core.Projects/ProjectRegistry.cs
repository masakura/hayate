﻿using System.Linq;
using System.Threading.Tasks;

namespace Hayate.Core.Projects
{
    public sealed class ProjectRegistry : IProjectRegistry
    {
        private readonly IExternalProjectRepository _externals;
        private readonly IProjectMappingRepository _mappings;

        public ProjectRegistry(IExternalProjectRepository externals, IProjectMappingRepository mappings)
        {
            _externals = externals;
            _mappings = mappings;
        }

        public async Task<Project> FindOrRegisterAsync(TargetProjectId targetId)
        {
            var id = await FindOrNewProjectId(targetId);

            return await ProjectAsync(id, targetId);
        }

        public async Task<Project> FindAsync(ProjectId id)
        {
            var targetId = await _mappings.FindAsync(id);

            return await ProjectAsync(id, targetId);
        }

        public async Task<Project> FindAsync(TargetProjectId targetId)
        {
            var id = await _mappings.FindAsync(targetId);

            return await ProjectAsync(id, targetId);
        }

        public async Task<ProjectCollection> ListRegisteredAsync()
        {
            var projectIds = await _mappings.ListAsync();

            return projectIds
                .Select(async m => await ProjectAsync(m.ProjectId, m.TargetProjectId))
                .Select(x => x.Result)
                .ToProjects();
        }

        public async Task<ProjectCollection> ListAsync()
        {
            var externals = await _externals.ListAsync();

            return externals.Select(async external => await ProjectAsync(external))
                .Select(result => result.Result)
                .ToProjects();
        }

        private async Task<ProjectId> FindOrNewProjectId(TargetProjectId targetProjectId)
        {
            var id = await _mappings.FindAsync(targetProjectId);
            if (id.IsEmpty)
            {
                id = ProjectId.NewId();
                await _mappings.AddAsync(id, targetProjectId);
            }

            return id;
        }

        private async Task<Project> ProjectAsync(ProjectId id, TargetProjectId targetId)
        {
            var external = await FindExternalProject(targetId);
            return Project.Registered(id, external);
        }

        private async Task<IExternalProject> FindExternalProject(TargetProjectId targetId)
        {
            if (targetId.IsEmpty) return NotFoundExternalProject.Instance;

            return await _externals.FindAsync(targetId);
        }

        private async Task<Project> ProjectAsync(IExternalProject external)
        {
            var projectId = await _mappings.FindAsync(external.Id);

            return Project.From(projectId, external);
        }
    }
}