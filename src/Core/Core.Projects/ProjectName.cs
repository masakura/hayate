﻿namespace Hayate.Core.Projects
{
    /// <summary>
    ///     プロジェクト名。
    /// </summary>
    public struct ProjectName
    {
        private readonly string _value;

        public ProjectName(string value)
        {
            _value = value;
        }

        public static ProjectName Empty { get; } = new ProjectName();
        public bool IsEmpty => _value == null;

        public override string ToString()
        {
            return _value;
        }
    }
}