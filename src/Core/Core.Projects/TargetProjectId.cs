﻿namespace Hayate.Core.Projects
{
    public struct TargetProjectId
    {
        private readonly string _value;

        public TargetProjectId(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static TargetProjectId Empty { get; } = new TargetProjectId();

        public bool IsEmpty => string.IsNullOrEmpty(_value);
    }
}