﻿using Hayate.Core.Instances;

namespace Hayate.Core.Projects
{
    internal sealed class NotFoundExternalProject : IExternalProject
    {
        private NotFoundExternalProject()
        {
        }

        public static IExternalProject Instance { get; } = new NotFoundExternalProject();

        public TargetProjectId Id => TargetProjectId.Empty;
        public IRepositoryName RepositoryName => EmptyRepositoryName.Instance;
        public ProjectName Name { get; } = ProjectName.Empty;
        public SiteUrl SiteUrl { get; } = SiteUrl.Empty;
        public bool IsNotFound { get; } = true;
    }
}