﻿using System;

namespace Hayate.Core.Projects
{
    public struct ProjectId
    {
        public bool Equals(ProjectId other)
        {
            return _value.Equals(other._value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ProjectId && Equals((ProjectId) obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        private readonly Guid _value;

        public ProjectId(Guid value)
        {
            _value = value;
        }

        public bool IsEmpty => _value == Guid.Empty;
        public static ProjectId Empty { get; } = new ProjectId();

        public static ProjectId NewId()
        {
            return new ProjectId(Guid.NewGuid());
        }

        public Guid ToGuid()
        {
            return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}