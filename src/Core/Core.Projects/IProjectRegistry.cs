﻿using System.Threading.Tasks;

namespace Hayate.Core.Projects
{
    /// <summary>
    ///     プロジェクトを管理します。
    /// </summary>
    public interface IProjectRegistry
    {
        /// <summary>
        ///     プロジェクトを登録します。すでにプロジェクトが登録済みの場合、登録済みのプロジェクトを取得します。
        /// </summary>
        /// <param name="targetId">アプリケーションにリンクする外部プロジェクトの ID。</param>
        /// <returns>プロジェクト。</returns>
        Task<Project> FindOrRegisterAsync(TargetProjectId targetId);

        /// <summary>
        ///     プロジェクトを取得します。
        /// </summary>
        /// <param name="id">プロジェクト ID。</param>
        /// <returns>プロジェクト。</returns>
        Task<Project> FindAsync(ProjectId id);

        /// <summary>
        ///     プロジェクトを取得します。
        /// </summary>
        /// <param name="targetId">アプリケーションにりんくされている外部プロジェクトの ID。</param>
        /// <returns>プロジェクト。</returns>
        Task<Project> FindAsync(TargetProjectId targetId);

        /// <summary>
        ///     登録されているすべてのプロジェクトを取得します。
        /// </summary>
        /// <returns>プロジェクトのコレクション。</returns>
        Task<ProjectCollection> ListRegisteredAsync();

        /// <summary>
        ///     登録されていないものも含めてすべてのプロジェクトを取得します。
        /// </summary>
        /// <returns>プロジェクトのコレクション。</returns>
        Task<ProjectCollection> ListAsync();
    }
}