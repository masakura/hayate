﻿using Hayate.Core.Instances;

namespace Hayate.Core.Projects
{
    public interface IExternalProject
    {
        TargetProjectId Id { get; }
        IRepositoryName RepositoryName { get; }
        ProjectName Name { get; }
        SiteUrl SiteUrl { get; }
        bool IsNotFound { get; }
    }
}