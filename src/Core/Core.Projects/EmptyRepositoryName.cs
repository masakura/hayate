﻿using Hayate.Core.Instances;

namespace Hayate.Core.Projects
{
    internal sealed class EmptyRepositoryName : IRepositoryName
    {
        public static IRepositoryName Instance = new EmptyRepositoryName();

        private EmptyRepositoryName()
        {
        }

        public override string ToString()
        {
            return "";
        }
    }
}