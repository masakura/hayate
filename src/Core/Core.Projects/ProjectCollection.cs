﻿using System.Collections.Generic;
using System.Linq;

namespace Hayate.Core.Projects
{
    public sealed class ProjectCollection : CoreCollection<Project>
    {
        public ProjectCollection(IEnumerable<Project> collection) : base(collection)
        {
        }

        public Project Find(ProjectId id)
        {
            return this.FirstOrDefault(p => p.Id.Equals(id)) ?? Project.NotFound(id);
        }
    }
}