﻿using System;
using Hayate.Core.Instances;

namespace Hayate.Core.Projects
{
    public sealed class Project
    {
        private readonly IExternalProject _external;

        private Project(ProjectId id, IExternalProject external)
        {
            Id = id;
            _external = external;
        }

        public ProjectId Id { get; }
        public ProjectName Name => _external.Name;
        public SiteUrl SiteUrl => _external.SiteUrl;
        public IRepositoryName RepositoryName => _external.RepositoryName;
        public bool IsNotFound => _external.IsNotFound;
        public bool IsNotRegistered => Id.IsEmpty || IsNotFound;
        public TargetProjectId TargetId => _external.Id;

        public static Project Registered(ProjectId id, IExternalProject external)
        {
            if (id.IsEmpty) throw new ArgumentException("空の ID は指定できません。", nameof(id));

            return new Project(id, external);
        }

        public static Project NotFound(ProjectId id)
        {
            if (id.IsEmpty) throw new ArgumentException("空の ID は指定できません。", nameof(id));

            return new Project(id, NotFoundExternalProject.Instance);
        }

        public static Project NotRegistered(IExternalProject external)
        {
            return new Project(ProjectId.Empty, external);
        }

        public static Project From(ProjectId id, IExternalProject external)
        {
            return !id.IsEmpty ? Registered(id, external) : NotRegistered(external);
        }
    }
}