﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hayate.Core.Projects
{
    /// <summary>
    ///     アプリケーションとリンクする外部プロジェクトの情報を取得します。
    /// </summary>
    public interface IExternalProjectRepository
    {
        /// <summary>
        ///     アプリケーションとリンクする外部プロジェクトを取得します。
        /// </summary>
        /// <param name="targetProjectId">外部プロジェクトの ID。</param>
        /// <returns>外部プロジェクト。</returns>
        Task<IExternalProject> FindAsync(TargetProjectId targetProjectId);

        /// <summary>
        ///     アプリケーションとリンクするアイブプロジェクトのコレクションを取得します。
        /// </summary>
        /// <returns>外部プロジェクトのコレクション。</returns>
        Task<IEnumerable<IExternalProject>> ListAsync();
    }
}