﻿namespace Hayate.Core.Projects
{
    public struct ProjectMapping
    {
        public ProjectId ProjectId { get; }
        public TargetProjectId TargetProjectId { get; }

        public ProjectMapping(ProjectId projectId, TargetProjectId targetProjectId)
        {
            ProjectId = projectId;
            TargetProjectId = targetProjectId;
        }
    }
}