﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hayate.Core.Projects
{
    /// <summary>
    ///     はやてのプロジェクトとリンクされた外部プロジェクトの ID を管理します。
    /// </summary>
    public interface IProjectMappingRepository
    {
        /// <summary>
        ///     はやてのプロジェクト ID からリンクされた外部プロジェクトの ID を取得します。
        /// </summary>
        /// <param name="projectId">はやてのプロジェクト ID。</param>
        /// <returns>リンクされた外部プロジェクト ID。</returns>
        Task<TargetProjectId> FindAsync(ProjectId projectId);

        /// <summary>
        ///     疾風にリンクされた外部プロジェクトの ID からはやてのプロジェクト ID を取得します。
        /// </summary>
        /// <param name="targetProjectId">リンクされた外部プロジェクト ID。</param>
        /// <returns>はやてのプロジェクト ID。</returns>
        Task<ProjectId> FindAsync(TargetProjectId targetProjectId);

        /// <summary>
        ///     すべてのマッピングを取得します。
        /// </summary>
        /// <returns>マッピング情報のコレクション。</returns>
        Task<IEnumerable<ProjectMapping>> ListAsync();

        /// <summary>
        ///     はやてのプロジェクトにリンクされた外部プロジェクト ID とはやてのプロジェクト ID をマッピングします。
        /// </summary>
        /// <param name="projectId">はやてのプロジェクtp ID。</param>
        /// <param name="targetProjectId">リンクされたアイブのプロジェクト ID。</param>
        /// <returns></returns>
        Task AddAsync(ProjectId projectId, TargetProjectId targetProjectId);
    }
}