﻿using System;

namespace Hayate.Core.Projects
{
    public struct SiteUrl
    {
        private readonly Uri _value;

        public SiteUrl(Uri value)
        {
            _value = value;
        }

        public static SiteUrl Empty { get; } = new SiteUrl();
        public bool IsEmpty => _value == null;

        public override string ToString()
        {
            return _value?.ToString();
        }
    }
}