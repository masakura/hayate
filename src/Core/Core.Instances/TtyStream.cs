﻿using System;
using System.Text;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Core.Instances
{
    /// <summary>
    ///     仮想ターミナルのストリーム。入出力を持ちます。
    /// </summary>
    public abstract class TtyStream : ISource, IDestination, IDisposable
    {
        public abstract Task WriteAsync(string s);

        public abstract void Dispose();

        public abstract bool IsClosed { get; }

        public abstract Task<string> ReadAsync();

        public static TtyStream NoContainer()
        {
            return new SourceAdapter(StringSource.NoContainerString());
        }

        public override string ToString()
        {
            return this.ToStringAsync().Result;
        }

        private sealed class SourceAdapter : TtyStream
        {
            private readonly ISource _source;

            public SourceAdapter(ISource source)
            {
                _source = source;
            }

            public override Task WriteAsync(string s)
            {
                // Do nothing
                return Task.CompletedTask;
            }

            public override void Dispose()
            {
            }

            public override bool IsClosed => _source.IsClosed;
            public override Task<string> ReadAsync() => _source.ReadAsync();
        }

        public static TtyStream FromString(string s)
        {
            return new SourceAdapter(LogsSource.FromString(s));
        }
    }
}
