﻿using System;

namespace Hayate.Core.Instances
{
    public struct EndPoint
    {
        private readonly Uri _uri;

        public EndPoint(string uri) : this(new Uri(uri))
        {
        }

        private EndPoint(Uri uri)
        {
            _uri = uri;
        }

        public bool IsEmpty => _uri == null;
        public static EndPoint Empty => default(EndPoint);

        public Uri ToUri()
        {
            return _uri;
        }

        private bool Equals(EndPoint other)
        {
            return Equals(_uri, other._uri);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is EndPoint && Equals((EndPoint) obj);
        }

        public override int GetHashCode()
        {
            return _uri != null ? _uri.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return _uri?.ToString();
        }
    }
}