﻿using Hayate.Hash;

namespace Hayate.Core.Instances
{
    /// <summary>
    ///     インスタンス ID。
    /// </summary>
    public struct InstanceId
    {
        private readonly string _value;

        public InstanceId(string value)
        {
            _value = value;
        }

        public static InstanceId Empty { get; } = default(InstanceId);

        public override string ToString()
        {
            return _value;
        }

        public static InstanceId From(IImageName imageName)
        {
            var hash = HashConverter.ToSha256(imageName.ToString());
            return new InstanceId(hash);
        }
    }
}