﻿using System.Threading.Tasks;

namespace Hayate.Core.Instances
{
    /// <summary>
    ///     起動可能なアプリケーションのインスタンス。
    /// </summary>
    public interface IInstance
    {
        /// <summary>
        ///     インスタンスの ID。
        /// </summary>
        InstanceId Id { get; }

        /// <summary>
        ///     インスタンスが再起動可能かどうかを取得します。
        /// </summary>
        bool CanRestart { get; }

        /// <summary>
        ///     新しいイメージにアップグレードをトライできるかどうかを取得します。
        /// </summary>
        bool CanTryUpgrade { get; }

        /// <summary>
        ///     インスタンスを操作可能かどうかを取得します。
        /// </summary>
        /// <param name="transition">操作。</param>
        /// <returns>指定された操作が可能かどうか。</returns>
        bool CanTransit(InstanceTransition transition);

        /// <summary>
        ///     インスタンスを操作します。
        /// </summary>
        /// <param name="transition">操作。</param>
        void Transit(InstanceTransition transition);

        /// <summary>
        ///     インスタンスの現在の状態を取得します。
        /// </summary>
        /// <returns>現在の状態。</returns>
        Task<InstanceStatus> StatusAsync();

        /// <summary>
        ///     インスタンスが公開しているエンドポイントを取得します。
        /// </summary>
        /// <returns>エンドポイント。</returns>
        Task<EndPoint> EndPointAsync();

        /// <summary>
        ///     インスタンスの実行ログを取得します。
        /// </summary>
        /// <returns>ログストリーム。</returns>
        Task<LogsSource> LogsAsync();

        /// <summary>
        ///     インスタンスの TTY を開きます。
        /// </summary>
        /// <returns>TTY ストリーム。</returns>
        Task<TtyStream> TtyAsync();
    }
}
