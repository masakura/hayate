﻿using System.Text;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Core.Instances
{
    internal static class SourceExtensions
    {
        public static async Task<string> ToStringAsync(this ISource source)
        {
            var builder = new StringBuilder();
            while (true)
            {
                var line = await source.ReadAsync();
                if (line == null) break;
                builder.Append(line);
            }

            return builder.ToString();
        }

    }
}
