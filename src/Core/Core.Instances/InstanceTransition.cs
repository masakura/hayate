﻿namespace Hayate.Core.Instances
{
    /// <summary>
    ///     インスタンスを操作します。
    /// </summary>
    public enum InstanceTransition
    {
        /// <summary>
        ///     起動。
        /// </summary>
        Running,

        /// <summary>
        ///     再起動。
        /// </summary>
        Restarted,

        /// <summary>
        ///     イメージが更新されている場合のみ、再作成。
        /// </summary>
        TryUpgrade
    }
}
