﻿using System;
using System.Text;
using System.Threading.Tasks;
using Hayate.StreamBinding;

namespace Hayate.Core.Instances
{
    /// <summary>
    ///     インスタンスの実行ログのストリームラッパー。
    /// </summary>
    public abstract class LogsSource : ISource, IDisposable
    {
        public abstract void Dispose();

        public abstract bool IsClosed { get; }

        public abstract Task<string> ReadAsync();

        public override string ToString()
        {
            return this.ToStringAsync().Result;
        }

        /// <summary>
        ///     ストリームが取得できなかったときのためのストリーム。空の文字列です。
        /// </summary>
        /// <returns>取得できなかったときのためのストリーム。</returns>
        public static LogsSource NotFound()
        {
            return StringSource.NotFoundString();
        }

        /// <summary>
        ///     コンテナーがないなどでログを取得できなかったときのストリーム。
        /// </summary>
        /// <returns>コンテナーがないときのストリーム。</returns>
        public static LogsSource NoContainer()
        {
            return StringSource.NoContainerString();
        }

        public static LogsSource FromString(string s)
        {
            return new StringSource(s);
        }
    }
}
