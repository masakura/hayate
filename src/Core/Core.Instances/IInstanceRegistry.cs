﻿using Hayate.Core.ImagesRegistry;

namespace Hayate.Core.Instances
{
    /// <summary>
    ///     起動可能なアプリケーションインスタンスを管理します。
    /// </summary>
    public interface IInstanceRegistry
    {
        /// <summary>
        ///     インスタンスを取得します。
        /// </summary>
        /// <param name="id">インスタンス名。</param>
        /// <returns>インスタンス。</returns>
        IInstance Find(InstanceId id);

        /// <summary>
        ///     インスタンスを取得します。インスタンスが存在しない場合は作成されます。
        /// </summary>
        /// <param name="registryImage">インスタンスの元になるイメージ。</param>
        /// <returns>インスタンス。</returns>
        IInstance FindOrCreate(IRegistryImage registryImage);
    }
}