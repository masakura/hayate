﻿using System.Collections.Generic;
using System.Linq;

namespace Hayate.Core.Instances
{
    public sealed class InstanceStatus
    {
        private static readonly IDictionary<string, InstanceStatus> Map;

        static InstanceStatus()
        {
            var list = new[]
            {
                nameof(Running),
                nameof(Pulling),
                nameof(Creating),
                nameof(Created),
                nameof(Unknown),
                nameof(Restarting),
                nameof(Removing),
                nameof(Paused),
                nameof(Exited),
                nameof(Dead),
                nameof(Pending)
            };

            Map = list.ToDictionary(status => status, status => new InstanceStatus(status));

            Running = Map[nameof(Running)];
            Pulling = Map[nameof(Pulling)];
            Creating = Map[nameof(Creating)];
            Created = Map[nameof(Created)];
            Unknown = Map[nameof(Unknown)];
            Restarting = Map[nameof(Restarting)];
            Removing = Map[nameof(Removing)];
            Paused = Map[nameof(Paused)];
            Exited = Map[nameof(Exited)];
            Dead = Map[nameof(Dead)];
            Pending = Map[nameof(Pending)];
        }

        public static InstanceStatus Running { get; }
        public static InstanceStatus Pulling { get; }
        public static InstanceStatus Creating { get; }
        public static InstanceStatus Created { get; }
        public static InstanceStatus Unknown { get; }
        public static InstanceStatus Restarting { get; }
        public static InstanceStatus Removing { get; }
        public static InstanceStatus Paused { get; }
        public static InstanceStatus Exited { get; }
        public static InstanceStatus Dead { get; }
        public static InstanceStatus Pending { get; }

        private readonly string _status;

        private InstanceStatus(string status)
        {
            _status = status;
        }

        public override string ToString()
        {
            return _status;
        }

        public static InstanceStatus Parse(string status)
        {
            if (status == null) return Unknown;

            if (Map.TryGetValue(status, out var value)) return value;

            return Unknown;
        }
    }
}