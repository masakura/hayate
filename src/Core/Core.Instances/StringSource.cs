﻿using System.Threading.Tasks;

namespace Hayate.Core.Instances
{
    internal sealed class StringSource : LogsSource
    {
        private readonly string _message;
        private bool _isClosed;

        public StringSource(string message)
        {
            _message = message;
        }

        public override void Dispose()
        {
        }

        public override bool IsClosed => _isClosed;

        public override Task<string> ReadAsync()
        {
            if (_isClosed) return Task.FromResult<string>(null);

            _isClosed = true;
            return Task.FromResult(_message);
        }

        public static LogsSource NotFoundString()
        {
            return new StringSource(string.Empty);
        }

        public static LogsSource NoContainerString()
        {
            return new StringSource("No container\r\n");
        }
    }
}
