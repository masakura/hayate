﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Hayate.Http
{
    /// <summary>
    ///     <see cref="HttpClient" /> のラッパー。
    /// </summary>
    public interface IHttpClient : IDisposable
    {
        Task<HttpResponseMessage> GetAsync(string requestUrl);
        HttpRequestHeaders DefaultRequestHeaders { get; }
    }
}
