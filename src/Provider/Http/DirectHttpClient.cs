﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Hayate.Http
{
    public sealed class DirectHttpClient : IHttpClient
    {
        private readonly HttpClient _client = new HttpClient();

        public void Dispose()
        {
            _client.Dispose();
        }

        public Task<HttpResponseMessage> GetAsync(string requestUrl)
        {
            return _client.GetAsync(requestUrl);
        }

        public HttpRequestHeaders DefaultRequestHeaders => _client.DefaultRequestHeaders;
    }
}
