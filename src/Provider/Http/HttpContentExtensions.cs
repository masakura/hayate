﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Hayate.Http
{
    internal static class HttpContentExtensions
    {
        /// <summary>
        ///     JSON 形式のレスポンスをオブジェクトに変換します。
        /// </summary>
        /// <param name="content">レスポンスコンテンツ。</param>
        /// <typeparam name="T">変換する型。</typeparam>
        /// <returns>JSON をデシリアライズしたオブジェクト。</returns>
        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            var body = await content.ReadAsStringAsync();
            return JToken.Parse(body).ToObject<T>();
        }
    }
}