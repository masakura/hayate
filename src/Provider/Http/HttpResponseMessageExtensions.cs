﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Hayate.Http
{
    internal static class HttpResponseMessageExtensions
    {
        /// <summary>
        ///     JSON 形式のレスポンスをオブジェクトに変換します。
        /// </summary>
        /// <param name="response">レスポンスオブジェクト。</param>
        /// <typeparam name="T">変換する型。</typeparam>
        /// <returns>JSON をデシリアライズしたオブジェクト。</returns>
        public static async Task<T> ReadAsJsonAsync<T>(this HttpResponseMessage response)
        {
            return await response.Content.ReadAsJsonAsync<T>();
        }
    }
}