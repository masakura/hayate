﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Hayate.Http
{
    public static class HttpClientExtensions
    {
        private static async Task<string> GetStringAsync(this IHttpClient client, string requestUrl)
        {
            var response = await client.GetAsync(requestUrl);
            return await response.Content.ReadAsStringAsync();
        }

        public static Task<string> GetStringAsync(this IHttpClient client, Uri requestUrl)
        {
            return client.GetStringAsync(requestUrl.ToString());
        }

        public static async Task<T> GetFromJsonAsync<T>(this IHttpClient client, string requestUrl)
        {
            var body = await client.GetStringAsync(requestUrl);
            return JToken.Parse(body).ToObject<T>();
        }
    }
}
