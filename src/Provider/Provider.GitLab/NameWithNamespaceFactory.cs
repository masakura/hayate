﻿namespace Hayate.Provider.GitLab
{
    internal sealed class NameWithNamespaceFactory : WithNamespaceFactory<GitLabProjectNameWithNamespace>
    {
        public NameWithNamespaceFactory() : base(" / ")
        {
        }

        protected override GitLabProjectNameWithNamespace Create(string @namespace, string project)
        {
            return new GitLabProjectNameWithNamespace(new GitLabNamespaceName(@namespace),
                new GitLabProjectName(project));
        }
    }
}