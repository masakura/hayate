﻿using System.Linq;

namespace Hayate.Provider.GitLab
{
    internal abstract class WithNamespaceFactory<T>
    {
        private readonly string _separator;

        protected WithNamespaceFactory(string separator)
        {
            _separator = separator;
        }

        public T Create(string withNamespace)
        {
            var parts = new WithNamespaceParts(withNamespace, _separator);

            if (!parts.IsValid) return default(T);

            return Create(parts.Namespace, parts.Project);
        }

        protected abstract T Create(string @namespace, string project);

        public string Join(string @namespace, string project)
        {
            var parts = new[] {@namespace, project}.Where(s => !string.IsNullOrEmpty(s));

            return string.Join(_separator, parts);
        }
    }
}