﻿using System;
using Hayate.Containers.Docker;
using Hayate.Json;
using Newtonsoft.Json;

namespace Hayate.Provider.GitLab
{
    [JsonConverter(typeof(PathWithNamespaceJsonConverter))]
    public struct GitLabProjectPathWithNamespace
    {
        public GitLabNamespacePath Namespace { get; }
        public GitLabProjectPath Project { get; }

        private static readonly PathWithNamespaceFactory PathWithNamespaceFactory = new PathWithNamespaceFactory();

        public GitLabProjectPathWithNamespace(GitLabNamespacePath @namespace, GitLabProjectPath project)
        {
            Namespace = @namespace;
            Project = project;
        }

        public override string ToString()
        {
            return PathWithNamespaceFactory.Join(Namespace.ToString(), Project.ToString());
        }

        public DockerRepositoryName ToRepositoryName()
        {
            return new DockerRepositoryName(ToString());
        }

        public static GitLabProjectPathWithNamespace Parse(string pathWithNamespace)
        {
            return PathWithNamespaceFactory.Create(pathWithNamespace);
        }

        private sealed class PathWithNamespaceJsonConverter : StructJsonConverter<GitLabProjectPathWithNamespace>
        {
            protected override GitLabProjectPathWithNamespace Parse(object value)
            {
                return GitLabProjectPathWithNamespace.Parse(value.ToString());
            }
        }
    }
}