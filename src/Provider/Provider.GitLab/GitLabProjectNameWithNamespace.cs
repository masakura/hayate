﻿using Hayate.Json;
using Newtonsoft.Json;

namespace Hayate.Provider.GitLab
{
    [JsonConverter(typeof(NameWithNamespaceJsonConverter))]
    public struct GitLabProjectNameWithNamespace
    {
        private static readonly NameWithNamespaceFactory NameWithNamespaceFactory = new NameWithNamespaceFactory();

        public GitLabProjectNameWithNamespace(GitLabNamespaceName @namespace, GitLabProjectName project)
        {
            Namespace = @namespace;
            Project = project;
        }

        public GitLabNamespaceName Namespace { get; }
        public GitLabProjectName Project { get; }

        public override string ToString()
        {
            return NameWithNamespaceFactory.Join(Namespace.ToString(), Project.ToString());
        }

        public static GitLabProjectNameWithNamespace Parse(string nameWithNamespace)
        {
            return NameWithNamespaceFactory.Create(nameWithNamespace);
        }

        private sealed class NameWithNamespaceJsonConverter : StructJsonConverter<GitLabProjectNameWithNamespace>
        {
            protected override GitLabProjectNameWithNamespace Parse(object value)
            {
                return GitLabProjectNameWithNamespace.Parse(value.ToString());
            }
        }
    }
}