﻿namespace Hayate.Provider.GitLab
{
    public struct GitLabProjectPath
    {
        private readonly string _value;

        public GitLabProjectPath(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}