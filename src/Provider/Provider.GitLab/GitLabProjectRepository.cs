﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Http;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProjectRepository : IGitLabProjectRepository
    {
        private readonly IGitLabHttpClient _client;

        public GitLabProjectRepository(IGitLabHttpClient client)
        {
            _client = client;
        }

        public async Task<GitLabProject> ProjectAsync(GitLabProjectId id)
        {
            var response = await _client.GetAsync($"projects/{id.ToString()}");

            if (response.StatusCode == HttpStatusCode.NotFound) return GitLabProject.NotFound;

            return await FromResponseAsync<GitLabProject>(response);
        }

        public async Task<IEnumerable<GitLabProject>> ListProjectsAsync()
        {
            var response = await _client.GetAsync("projects?owned=true");

            return await FromResponseAsync<IEnumerable<GitLabProject>>(response);
        }

        private static async Task<T> FromResponseAsync<T>(HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode();

            return await response.ReadAsJsonAsync<T>();
        }
    }
}
