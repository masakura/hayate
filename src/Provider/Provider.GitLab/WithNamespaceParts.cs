﻿using System;
using System.Linq;

namespace Hayate.Provider.GitLab
{
    internal struct WithNamespaceParts
    {
        public WithNamespaceParts(string value, string separator)
        {
            var parts = value?.Split(new[] {separator}, StringSplitOptions.None);

            if (parts?.Length >= 2)
            {
                Namespace = string.Join(separator, parts.Take(parts.Length - 1));
                Project = parts.Last();
                IsValid = true;
            }
            else
            {
                Namespace = null;
                Project = null;
                IsValid = false;
            }
        }

        public bool IsValid { get; }
        public string Namespace { get; }
        public string Project { get; }
    }
}