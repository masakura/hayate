﻿namespace Hayate.Provider.GitLab
{
    internal sealed class PathWithNamespaceFactory : WithNamespaceFactory<GitLabProjectPathWithNamespace>
    {
        public PathWithNamespaceFactory() : base("/")
        {
        }

        protected override GitLabProjectPathWithNamespace Create(string @namespace, string project)
        {
            return new GitLabProjectPathWithNamespace(new GitLabNamespacePath(@namespace),
                new GitLabProjectPath(project));
        }
    }
}