﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hayate.Provider.GitLab
{
    /// <summary>
    ///     GitLab プロジェクトにアクセスします。
    /// </summary>
    public interface IGitLabProjectRepository
    {
        /// <summary>
        ///     プロジェクトを取得します。
        /// </summary>
        /// <param name="id">GitLab プロジェクトの ID。</param>
        /// <returns>GitLab プロジェクト。</returns>
        Task<GitLabProject> ProjectAsync(GitLabProjectId id);

        /// <summary>
        ///     プロジェクトのコレクションを取得します。
        /// </summary>
        /// <returns>GitLab プロジェクトのコレクション。</returns>
        Task<IEnumerable<GitLabProject>> ListProjectsAsync();
    }
}