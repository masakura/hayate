﻿namespace Hayate.Provider.GitLab
{
    public struct GitLabProjectName
    {
        private readonly string _value;

        public GitLabProjectName(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}