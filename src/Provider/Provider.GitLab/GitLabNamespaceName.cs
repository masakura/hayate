﻿namespace Hayate.Provider.GitLab
{
    public struct GitLabNamespaceName
    {
        private readonly string _value;

        public GitLabNamespaceName(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}