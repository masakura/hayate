﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Hayate.Provider.GitLab
{
    public interface IGitLabHttpClient
    {
        Task<HttpResponseMessage> GetAsync(string requestUri);
    }
}