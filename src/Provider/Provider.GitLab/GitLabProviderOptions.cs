﻿using System;
using Microsoft.Extensions.Configuration;

namespace Hayate.Provider.GitLab
{
    public class GitLabProviderOptions
    {
        public string Endpoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; } = "http://gitlab.com";

        public static GitLabProviderOptions Create(IConfiguration configuration)
        {
            var section = configuration.GetSection("Provider:GitLab");
            return new GitLabProviderOptions()
            {
                ClientId = section["ClientId"],
                ClientSecret = section["ClientSecret"],
                Endpoint = section["Endpoint"]
            };
        }

        public string GetEndpoint(string relativeUri)
        {
            return GetEndpointUri(relativeUri).ToString();
        }

        public string GetApiEndpoint(string endpoint = null)
        {
            var uri = GetEndpointUri("/api/v4/");
            if (!string.IsNullOrEmpty(endpoint)) uri = new Uri(uri, endpoint);
            return uri.ToString();
        }

        private Uri GetEndpointUri(string relativeUri)
        {
            var @base = new Uri(Endpoint);
            return new Uri(@base, relativeUri);
        }
    }
}