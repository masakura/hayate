﻿using System;
using Hayate.Containers.Docker;
using Newtonsoft.Json;

namespace Hayate.Provider.GitLab
{
    public sealed class GitLabProject : ISourceProject
    {
        public GitLabProject()
        {
            IsNotFound = false;
        }

        public GitLabProject(GitLabProjectId id,
            GitLabProjectPathWithNamespace pathWithNamespace,
            GitLabProjectNameWithNamespace nameWithNamespace,
            Uri webUrl) : this()
        {
            Id = id;
            PathWithNamespace = pathWithNamespace;
            NameWithNamespace = nameWithNamespace;
            WebUrl = webUrl;
        }

        private GitLabProject(bool isNotFound)
        {
            IsNotFound = isNotFound;
        }

        [JsonProperty("id")] public GitLabProjectId Id { get; private set; }

        [JsonProperty("path_with_namespace")]
        public GitLabProjectPathWithNamespace PathWithNamespace { get; private set; }

        [JsonProperty("name_with_namespace")]
        public GitLabProjectNameWithNamespace NameWithNamespace { get; private set; }

        [JsonProperty("web_url")]
        public Uri WebUrl { get; private set; }

        public static GitLabProject NotFound { get; } = new GitLabProject(true);

        public bool IsNotFound { get; }

        public DockerRepositoryName RepositoryName => PathWithNamespace.ToRepositoryName();

        ProjectName ISourceProject.Name => new ProjectName(NameWithNamespace.Project.ToString());
        Uri ISourceProject.ProjectSite => WebUrl;
    }
}