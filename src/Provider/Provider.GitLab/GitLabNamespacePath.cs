﻿namespace Hayate.Provider.GitLab
{
    public struct GitLabNamespacePath
    {
        private readonly string _value;

        public GitLabNamespacePath(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}