﻿using Hayate.Core.Projects;
using Hayate.Json;
using Newtonsoft.Json;

namespace Hayate.Provider.GitLab
{
    [JsonConverter(typeof(ProjectIdJsonConverter))]
    public struct GitLabProjectId
    {
        private readonly string _value;

        public GitLabProjectId(TargetProjectId projectId) : this(Parse(projectId))
        {
        }

        public GitLabProjectId(long value)
        {
            _value = value.ToString();
        }

        public override string ToString()
        {
            return _value;
        }

        private static long Parse(TargetProjectId projectId)
        {
            return long.TryParse(projectId.ToString(), out var value) ? value : 0;
        }

        private sealed class ProjectIdJsonConverter : StructJsonConverter<GitLabProjectId>
        {
            protected override GitLabProjectId Parse(object value)
            {
                return new GitLabProjectId((long) value);
            }

            protected override bool RequireSupport(JsonToken tokenType)
            {
                return tokenType == JsonToken.Integer;
            }
        }
    }
}