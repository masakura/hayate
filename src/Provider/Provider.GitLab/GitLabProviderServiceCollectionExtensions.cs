﻿using Hayate.Provider;
using Hayate.Provider.GitLab;
using Microsoft.Extensions.Configuration;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class GitLabProviderServiceCollectionExtensions
    {
        public static void AddGitLabProvider(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<GitLabProviderOptions>(options =>
            {
                var fromSetting = GitLabProviderOptions.Create(configuration);
                options.ClientId = fromSetting.ClientId;
                options.ClientSecret = fromSetting.ClientSecret;
                options.Endpoint = fromSetting.Endpoint;
            });
            services.AddTransient<IGitLabProjectRepository, GitLabProjectRepository>();
        }
    }
}