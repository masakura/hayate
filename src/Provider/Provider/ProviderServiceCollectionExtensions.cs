﻿using Microsoft.Extensions.Configuration;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ProviderServiceCollectionExtensions
    {
        public static void AddProvider(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddGitLabProvider(configuration);
            services.AddDockerRegistryProvider(configuration);
        }
    }
}