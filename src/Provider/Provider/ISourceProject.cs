﻿using System;

namespace Hayate.Provider
{
    /// <summary>
    ///     アプリの元になる外部プロジェクトの情報。
    /// </summary>
    public interface ISourceProject
    {
        /// <summary>
        ///     プロジェクト名を取得します。
        /// </summary>
        ProjectName Name { get; }

        /// <summary>
        ///     プロジェクトサイトの URL を取得します。
        /// </summary>
        Uri ProjectSite { get; }

        /// <summary>
        ///     プロジェクトが見つからなかったことを取得します。
        /// </summary>
        bool IsNotFound { get; }
    }
}