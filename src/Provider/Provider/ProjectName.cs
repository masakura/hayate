﻿namespace Hayate.Provider
{
    public struct ProjectName
    {
        private readonly string _value;

        public ProjectName(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }
    }
}