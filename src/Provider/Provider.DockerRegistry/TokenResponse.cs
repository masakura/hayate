﻿namespace Hayate.Provider.DockerRegistry
{
    public sealed class TokenResponse
    {
        public string Token { get; set; }
    }
}