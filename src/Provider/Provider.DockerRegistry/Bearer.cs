﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Hayate.Http;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class Bearer
    {
        private readonly BearerWwwAuthentiateHeader _value;

        public Bearer(BearerWwwAuthentiateHeader value)
        {
            _value = value;
        }

        public static Bearer Parse(string text)
        {
            var value = BearerWwwAuthentiateHeader.Parse(text);
            return new Bearer(value);
        }

        public static IEnumerable<string> SplitPairs(string line)
        {
            return new Regex("\\s*,\\s*").Split(line);
        }

        public static (string key, string value) SplitKeyValue(string pair)
        {
            var result = new Regex("\\s*=\\s*").Split(pair, 2);
            return (result[0].Trim('"', ' '), result[1].Trim('"', ' '));
        }

        public Uri GetRequestUri()
        {
            return new Uri($"{_value.Realm}?service={_value.Service}&scope={_value.Scope}");
        }

        public async Task<string> GetTokenAsync(string password, HttpMessageHandler handler)
        {
            using (var client = new HttpClient(handler))
            {
                var bytes = Encoding.UTF8.GetBytes($"hayate:{password}");
                var base64 = Convert.ToBase64String(bytes);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64);
                var response = await client.GetAsync(GetRequestUri());
                var tokenResponse = await response.Content.ReadAsJsonAsync<TokenResponse>();
                return tokenResponse.Token;
            }
        }
    }
}
