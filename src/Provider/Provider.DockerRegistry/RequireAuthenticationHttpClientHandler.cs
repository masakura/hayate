﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Http;
using Newtonsoft.Json.Linq;

namespace Hayate.Provider.DockerRegistry
{
    /// <summary>
    ///     HTTP Response Header の WWW-Authenticate に反応してトークンを取得して再度アクセスします。
    /// </summary>
    internal sealed class RequireAuthenticationHttpClientHandler : HttpClientHandler
    {
        private readonly DockerRegistryPasswordCredential _credential;

        public RequireAuthenticationHttpClientHandler(DockerRegistryPasswordCredential credential)
        {
            _credential = credential;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            var authenticateEndpoint = BearerParts.From(response)?.ToUri();

            if (authenticateEndpoint == null) return response;

            var token = await GetAccessTokenAsync(authenticateEndpoint);
            if (token == null) return response;

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return await base.SendAsync(request, cancellationToken);
        }

        private async Task<string> GetAccessTokenAsync(Uri endpoint)
        {
            using (var client = new RequestAuthenticateHttpClient(new DirectHttpClient()))
            {
                // ReSharper disable once ImpureMethodCallOnReadonlyValueField
                client.DefaultRequestHeaders.Authorization = _credential.ToAuthenticationHeader();
                var body = await client.GetStringAsync(endpoint);
                return JToken.Parse(body).ToObject<JwtTokenResponse>()?.Token;
            }
        }
    }
}
