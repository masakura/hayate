﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Http;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class DockerRegistryDirectHttpClient : IHttpClient
    {
        private readonly HttpClient _client;
        private readonly DockerRegistryPasswordCredential _credential;

        internal DockerRegistryDirectHttpClient(DockerRegistryPasswordCredential credential)
        {
            _credential = credential;
            _client = CreateHttpClient();
        }

        public void Dispose()
        {
            _client?.Dispose();
        }

        public Task<HttpResponseMessage> GetAsync(string requestUrl)
        {
            return _client.GetAsync(requestUrl);
        }

        public HttpRequestHeaders DefaultRequestHeaders => _client.DefaultRequestHeaders;

        private HttpClient CreateHttpClient()
        {
            return new HttpClient(new RequireAuthenticationHttpClientHandler(_credential))
            {
                DefaultRequestHeaders =
                {
                    Accept =
                    {
                        Item("application/vnd.docker.distribution.manifest.v1+json"),
                        Item("application/vnd.docker.distribution.manifest.v1+prettyjws"),
                        Item("application/vnd.docker.distribution.manifest.v2+json"),
                        Item("application/vnd.docker.distribution.manifest.list.v2+json"),
                        Item("application/vnd.docker.container.image.v1+json")
                    }
                }
            };
        }

        private static MediaTypeWithQualityHeaderValue Item(string value)
        {
            return new MediaTypeWithQualityHeaderValue(value);
        }
    }
}
