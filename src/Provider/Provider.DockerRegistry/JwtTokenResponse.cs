﻿using Newtonsoft.Json;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class JwtTokenResponse
    {
        [JsonProperty("token")] public string Token { get; private set; }
    }
}