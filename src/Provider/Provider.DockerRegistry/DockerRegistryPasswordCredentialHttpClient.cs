﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Http;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class DockerRegistryPasswordCredentialHttpClient : IHttpClient
    {
        private readonly Lazy<IHttpClient> _lazyClient;

        public DockerRegistryPasswordCredentialHttpClient(IDockerRegistryPasswordCredentialStore credentialStore)
        {
            _lazyClient = new Lazy<IHttpClient>(() =>
            {
                var credential = credentialStore.GetCurrentUserCredentialAsync().GetAwaiter().GetResult();
                return new DockerRegistryDirectHttpClient(credential);
            });
        }

        public DockerRegistryPasswordCredentialHttpClient(DockerRegistryPasswordCredential credential)
        {
            _lazyClient = new Lazy<IHttpClient>(() => new DockerRegistryDirectHttpClient(credential));
        }

        public void Dispose()
        {
            if (_lazyClient.IsValueCreated) _lazyClient.Value.Dispose();
        }

        public Task<HttpResponseMessage> GetAsync(string requestUrl)
        {
            return _lazyClient.Value.GetAsync(requestUrl);
        }

        public HttpRequestHeaders DefaultRequestHeaders => _lazyClient.Value.DefaultRequestHeaders;
    }
}
