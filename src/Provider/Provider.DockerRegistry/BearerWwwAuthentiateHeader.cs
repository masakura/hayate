﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class BearerWwwAuthentiateHeader
    {
        public BearerWwwAuthentiateHeader(string scheme, string realm, string service, string scope)
        {
            Scheme = scheme;
            Realm = realm;
            Service = service;
            Scope = scope;
        }

        public string Scheme { get; }
        public string Realm { get; }
        public string Service { get; }
        public string Scope { get; }

        public static BearerWwwAuthentiateHeader Parse(string text)
        {
            // TODO 値に " が入っていたときとかが考慮されていない

            var parts = new Regex("\\s").Split(text, 2);
            var parameters = SplitPairs(parts[1])
                .Select(SplitKeyValue)
                .ToDictionary(pair => pair.key, pair => pair.value);

            return new BearerWwwAuthentiateHeader(parts[0], parameters["realm"], parameters["service"], parameters["scope"]);
        }

        private static IEnumerable<string> SplitPairs(string line)
        {
            return new Regex("\\s*,\\s*").Split(line);
        }

        private static (string key, string value) SplitKeyValue(string pair)
        {
            var result = new Regex("\\s*=\\s*").Split(pair, 2);
            return (result[0].Trim('"', ' '), result[1].Trim('"', ' '));
        }
    }
}
