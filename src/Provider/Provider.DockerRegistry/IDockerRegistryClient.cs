﻿using System;
using System.Threading.Tasks;
using Hayate.Containers.Docker;

namespace Hayate.Provider.DockerRegistry
{
    /// <summary>
    ///     Docker Registry Client。
    /// </summary>
    public interface IDockerRegistryClient : IDisposable
    {
        Task<TagsListResponse> TagsAsync(DockerRepositoryName repository);
        DockerImageName AppendHostAndPort(DockerImageName image);
        Task<bool> VerifyAccessAsync();
    }
}
