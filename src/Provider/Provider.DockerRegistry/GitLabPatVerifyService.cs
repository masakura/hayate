﻿using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Core.Tokens;

namespace Hayate.Provider.DockerRegistry
{
    internal sealed class GitLabPatVerifyService : IGitLabPatVerifyService
    {
        private readonly DockerRegistryClientFactory _clientFactory;

        public GitLabPatVerifyService(DockerRegistryClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public Task<bool> VerfyAsync(PersonalAccessToken token)
        {
            var credential = new DockerRegistryPasswordCredential(token);
            var client = _clientFactory.Create(credential);
            return client.VerifyAccessAsync();
        }
    }
}
