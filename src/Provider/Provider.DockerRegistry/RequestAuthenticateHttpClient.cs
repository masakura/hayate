﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Http;

namespace Hayate.Provider.DockerRegistry
{
    /// <summary>
    ///     <see cref="RequireAuthenticateException" /> をスローするための <see cref="IHttpClient" /> インターフェイスの実装。
    /// </summary>
    public sealed class RequestAuthenticateHttpClient : IHttpClient
    {
        private readonly IHttpClient _target;

        public RequestAuthenticateHttpClient(IHttpClient target)
        {
            _target = target;
        }

        public void Dispose()
        {
            _target.Dispose();
        }

        public async Task<HttpResponseMessage> GetAsync(string requestUrl)
        {
            var response = await _target.GetAsync(requestUrl);

            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new RequireAuthenticateException();
            }

            return response;
        }

        public HttpRequestHeaders DefaultRequestHeaders => _target.DefaultRequestHeaders;
    }
}
