﻿using System;
using Hayate.Containers.Docker;
using Hayate.Core.Tokens;
using Hayate.Provider.DockerRegistry;
using Microsoft.Extensions.Configuration;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class DockerRegistryProviderServiceCollectionExtensions
    {
        public static void AddDockerRegistryProvider(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(provider =>
            {
                var credentialStore = provider.GetRequiredService<IDockerRegistryPasswordCredentialStore>();
                return new DockerRegistryClientFactory(RegistryEndpoint(configuration), credentialStore);
            });
            services.AddTransient<IGitLabPatVerifyService, GitLabPatVerifyService>();
        }

        private static Uri RegistryEndpoint(IConfiguration configuration)
        {
            return new Uri(configuration["Provider:GitLab:Registry"]);
        }
    }
}
