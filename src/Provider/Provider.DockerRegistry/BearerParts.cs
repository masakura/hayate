﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Hayate.Provider.DockerRegistry
{
    /// <summary>
    ///     HTTP Response Header の WWWW-Authenticate ヘッダーを解析します。
    /// </summary>
    public sealed class BearerParts
    {
        private readonly Dictionary<string, string> _parameters;

        public BearerParts(string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            _parameters = value.Split(',')
                .Select(parameter => parameter.Trim().Split(new[] {'='}, 2))
                .Where(parameter => parameter.Length == 2)
                .ToDictionary(parameter => parameter[0], parameter => parameter[1].Trim('"'));

            Realm = _parameters["realm"];
            _parameters.Remove("realm");
        }

        public string Realm { get; }

        public Uri ToUri()
        {
            var query2 = new QueryString(_parameters);
            query2.AddAll();
            return query2.ToUri(Realm);
        }

        private sealed class QueryString
        {
            private readonly IDictionary<string, string> _parameters;
            private readonly NameValueCollection _query = HttpUtility.ParseQueryString(string.Empty);

            public QueryString(IDictionary<string, string> parameters)
            {
                _parameters = parameters;
            }

            public Uri ToUri(string realm)
            {
                return new UriBuilder(realm)
                {
                    Query = _query.ToString()
                        .Replace("%2f", "/")
                        .Replace("%3a", ":")
                }.Uri;
            }

            public void AddAll()
            {
                foreach (var pair in _parameters)
                {
                    _query.Add(pair.Key, pair.Value);
                }
            }
        }

        public static BearerParts From(HttpResponseMessage response)
        {
            var parameter = response.Headers.WwwAuthenticate
                .FirstOrDefault(x => x.Scheme == "Bearer")
                ?.Parameter;

            if (parameter == null) return null;

            return new BearerParts(parameter);
        }
    }
}
