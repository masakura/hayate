﻿using System.Collections.Generic;
using Hayate.Containers.Docker;
using Newtonsoft.Json;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class TagsListResponse
    {
        [JsonProperty("name")] public DockerRepositoryName Name { get; private set; }
        [JsonProperty("tags")] public IEnumerable<DockerTagName> Tags { get; private set; }
    }
}