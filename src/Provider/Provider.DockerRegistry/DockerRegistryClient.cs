﻿using System;
using System.Net;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Http;

namespace Hayate.Provider.DockerRegistry
{
    /// <summary>
    ///     Docker Registry Client。
    /// </summary>
    public sealed class DockerRegistryClient : IDockerRegistryClient
    {
        private readonly IHttpClient _client;
        private readonly Uri _endpoint;

        public DockerRegistryClient(Uri endpoint, IHttpClient client)
        {
            _endpoint = endpoint;
            _client = client;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<TagsListResponse> TagsAsync(DockerRepositoryName repository)
        {
            return await _client.GetFromJsonAsync<TagsListResponse>(MakeEndpoint(repository, "tags/list"));
        }

        public async Task<bool> VerifyAccessAsync()
        {
            var response = await _client.GetAsync(MakeVersionsEndpoint());
            return response.StatusCode == HttpStatusCode.OK;
        }

        public DockerImageName AppendHostAndPort(DockerImageName image)
        {
            return image.AppendHostAndPort(_endpoint);
        }

        private string MakeEndpoint(DockerRepositoryName repository, string path)
        {
            return new Uri(_endpoint, $"v2/{repository.ToString()}/{path}").ToString();
        }

        private string MakeVersionsEndpoint()
        {
            return new Uri(_endpoint, $"v2/").ToString();
        }
    }
}
