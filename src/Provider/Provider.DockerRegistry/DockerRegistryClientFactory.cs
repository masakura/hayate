﻿using System;
using System.Threading.Tasks;
using Hayate.Containers.Docker;

namespace Hayate.Provider.DockerRegistry
{
    public sealed class DockerRegistryClientFactory
    {
        private readonly Uri _endpoint;
        private readonly IDockerRegistryPasswordCredentialStore _credentialStore;

        public DockerRegistryClientFactory(Uri endpoint, IDockerRegistryPasswordCredentialStore credentialStore)
        {
            _endpoint = endpoint;
            _credentialStore = credentialStore;
        }

        public async Task<IDockerRegistryClient> CreateAsync()
        {
            var credential = await _credentialStore.GetCurrentUserCredentialAsync();
            return Create(credential);
        }

        public IDockerRegistryClient Create(DockerRegistryPasswordCredential credential)
        {
            var httpClient = new DockerRegistryPasswordCredentialHttpClient(credential);
            return new DockerRegistryClient(_endpoint, httpClient);
        }
    }
}