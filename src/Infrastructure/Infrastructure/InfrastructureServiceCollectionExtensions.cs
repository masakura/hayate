﻿using System;
using Hayate.Infrastructure;
using Microsoft.EntityFrameworkCore;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class InfrastructureServiceCollectionExtensions
    {
        public static void AddInfrastructureServices(this IServiceCollection services,
            Action<DbContextOptionsBuilder> options)
        {
            services.AddDbContextPool<ApplicationDbContext>(options);
            services.AddInfrastructureCore();
            services.AddInfrastructureGitLab();
            services.AddInfrastructureDockerImageRegistry();
        }
    }
}
