﻿using System;
using Hayate.Infrastructure.Core;
using Microsoft.EntityFrameworkCore;

namespace Hayate.Infrastructure
{
    // ReSharper disable UnusedAutoPropertyAccessor.Local
    public sealed class ApplicationDbContext : DbContext
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ProjectMappingRow> ProjectMappings { get; private set; }
        public DbSet<UserPersonalAccessToken> UserPersonalAccessTokens { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProjectMappingRow>()
                .HasIndex(m => m.TargetProjectId)
                .IsUnique();
        }

        public static ApplicationDbContext CreateInMemory()
        {
            var options = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            return new ApplicationDbContext(options);
        }
    }
}
