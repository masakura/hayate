﻿using Hayate.Containers.Docker;
using Hayate.Core.Applications;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Infrastructure.DockerImageRegistry
{
    public sealed class DockerRegistryImage : IRegistryImage
    {
        private readonly DockerImageName _imageName;

        private DockerRegistryImage(DockerImageName imageName)
        {
            _imageName = imageName;
        }

        public EnvironmentName Name => new EnvironmentName(_imageName.Tag.ToString());
        public IImageName ImageName => _imageName;
        public bool IsNotFound => false;

        public static IRegistryImage From(DockerImageName imageName)
        {
            return new DockerRegistryImage(imageName);
        }
    }
}