﻿using Hayate.Core.ImagesRegistry;
using Hayate.Infrastructure.DockerImageRegistry;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class InfrastructureDockerImageRegistryServiceCollectionExtensions
    {
        public static void AddInfrastructureDockerImageRegistry(this IServiceCollection services)
        {
            services.AddTransient<IRegistryImageRepository, DockerImageRegistryRepository>();
        }
    }
}