﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Containers.Docker;
using Hayate.Core.Applications;
using Hayate.Core.ImagesRegistry;
using Hayate.Core.Instances;
using Hayate.Provider.DockerRegistry;

namespace Hayate.Infrastructure.DockerImageRegistry
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class DockerImageRegistryRepository : IRegistryImageRepository
    {
        private readonly DockerRegistryClientFactory _clientFactory;

        public DockerImageRegistryRepository(DockerRegistryClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IEnumerable<IImageName>> FindAsync(IRepositoryName repositoryName)
        {
            if (!(repositoryName is DockerRepositoryName)) return Enumerable.Empty<IImageName>();

            var repository = (DockerRepositoryName) repositoryName;

            using (var client = await _clientFactory.CreateAsync())
            {

                var tags = await client.TagsAsync(repository);
                return tags.Tags
                    .Select(tag => new DockerImageName(repository, tag))
                    .Select(name => client.AppendHostAndPort(name))
                    .OfType<IImageName>();
            }
        }

        public Task<IRegistryImage> FindAsync(IImageName imageName)
        {
            if (!(imageName is DockerImageName))
                return Task.FromResult(NotFoundRegistryImage.NotFound(new EnvironmentName()));

            var image = (DockerImageName) imageName;

            return Task.FromResult(DockerRegistryImage.From(image));
        }
    }
}
