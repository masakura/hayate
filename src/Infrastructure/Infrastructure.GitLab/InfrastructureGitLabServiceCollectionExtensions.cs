﻿using Hayate.Core.Projects;
using Hayate.Infrastructure.Core;
using Hayate.Infrastructure.GitLab;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class InfrastructureGitLabServiceCollectionExtensions
    {
        public static void AddInfrastructureGitLab(this IServiceCollection services)
        {
            services.AddTransient<IExternalProjectRepository, GitLabExternalProjectRepository>();
        }
    }
}