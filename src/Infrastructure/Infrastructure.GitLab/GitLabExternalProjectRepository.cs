﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.Projects;
using Hayate.Provider.GitLab;

namespace Hayate.Infrastructure.GitLab
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class GitLabExternalProjectRepository : IExternalProjectRepository
    {
        private readonly IGitLabProjectRepository _projects;

        public GitLabExternalProjectRepository(IGitLabProjectRepository projects)
        {
            _projects = projects;
        }

        public async Task<IExternalProject> FindAsync(TargetProjectId targetProjectId)
        {
            var project = await _projects.ProjectAsync(new GitLabProjectId(targetProjectId));

            return GitLabExternalProject.From(project);
        }

        public async Task<IEnumerable<IExternalProject>> ListAsync()
        {
            // TODO オーナーされているプロジェクトのみ?
            IEnumerable<GitLabProject> projects = await _projects.ListProjectsAsync();

            return projects.Select(project => GitLabExternalProject.From(project));
        }
    }
}