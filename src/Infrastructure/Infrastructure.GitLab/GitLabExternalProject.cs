﻿using Hayate.Core.Instances;
using Hayate.Core.Projects;
using Hayate.Instances;
using Hayate.Provider.GitLab;

namespace Hayate.Infrastructure.GitLab
{
    internal sealed class GitLabExternalProject : IExternalProject
    {
        private readonly GitLabProject _project;

        private GitLabExternalProject(GitLabProject project)
        {
            _project = project;
        }

        public TargetProjectId Id => new TargetProjectId(_project.Id.ToString());
        public IRepositoryName RepositoryName => _project.RepositoryName;
        public ProjectName Name => new ProjectName(_project.NameWithNamespace.Project.ToString());
        public SiteUrl SiteUrl => new SiteUrl(_project.WebUrl);
        public bool IsNotFound => false;

        public static IExternalProject From(GitLabProject project)
        {
            return project != null ? new GitLabExternalProject(project) : null;
        }
    }
}