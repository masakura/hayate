﻿using Hayate.Core.Projects;
using Hayate.Core.Tokens;
using Hayate.Infrastructure.Core;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class InfrastructureCoreServiceCollectionExtensions
    {
        public static void AddInfrastructureCore(this IServiceCollection services)
        {
            services.AddTransient<IProjectMappingRepository, ProjectMappingRepository>();
            services.AddTransient<IPatRepository, PatRepository>();
        }
    }
}
