﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hayate.Core.Projects;
using Microsoft.EntityFrameworkCore;

namespace Hayate.Infrastructure.Core
{
    public sealed class ProjectMappingRepository : IProjectMappingRepository
    {
        private readonly ApplicationDbContext _db;

        public ProjectMappingRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<TargetProjectId> FindAsync(ProjectId projectId)
        {
            var mapping = await _db.ProjectMappings.FindAsync(projectId.ToGuid());

            if (mapping == null) return TargetProjectId.Empty;

            return new TargetProjectId(mapping.TargetProjectId);
        }

        public async Task<ProjectId> FindAsync(TargetProjectId targetProjectId)
        {
            // TODO インデックスを貼る
            var mapping = await _db.ProjectMappings.FirstOrDefaultAsync(m =>
                    m.TargetProjectId == targetProjectId.ToString());
            
            if (mapping == null) return ProjectId.Empty;
            
            return new ProjectId(mapping.ProjectId);
        }

        public async Task<IEnumerable<ProjectMapping>> ListAsync()
        {
            var query = _db.ProjectMappings
                .Select(m => m.ToEntity());

            return await query.ToArrayAsync();
        }

        public async Task AddAsync(ProjectId projectId, TargetProjectId targetProjectId)
        {
            await _db.ProjectMappings.AddAsync(new ProjectMappingRow(projectId.ToGuid(), targetProjectId.ToString()));
            await _db.SaveChangesAsync();
        }
    }
}