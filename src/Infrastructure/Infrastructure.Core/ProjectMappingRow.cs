﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hayate.Core.Projects;

namespace Hayate.Infrastructure.Core
{
    [Table("ProjectMappings")]
    // ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
    public sealed class ProjectMappingRow
    {
        // ReSharper disable once UnusedMember.Global
        public ProjectMappingRow()
        {
        }

        public ProjectMappingRow(Guid projectId, string targetProjectId)
        {
            ProjectId = projectId;
            TargetProjectId = targetProjectId;
        }

        [Key] public Guid ProjectId { get; private set; }
        public string TargetProjectId { get; private set; }

        public ProjectMapping ToEntity()
        {
            return new ProjectMapping(new ProjectId(ProjectId), new TargetProjectId(TargetProjectId));
        }
    }
}