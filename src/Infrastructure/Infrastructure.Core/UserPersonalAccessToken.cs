﻿using System.ComponentModel.DataAnnotations;

namespace Hayate.Infrastructure.Core
{
    public sealed class UserPersonalAccessToken
    {
        [Key]
        public string UserId { get; set; }

        [Required]
        public string PersonalAccessToken { get; set; }
    }
}
