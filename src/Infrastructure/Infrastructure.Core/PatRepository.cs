﻿using System.Threading.Tasks;
using Hayate.Core.Tokens;
using Hayate.Core.Users;

namespace Hayate.Infrastructure.Core
{
    public sealed class PatRepository : IPatRepository
    {
        private readonly ApplicationDbContext _db;

        public PatRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task SaveAsync(UserId userId, PersonalAccessToken token)
        {
            var entity = await _db.UserPersonalAccessTokens.FindAsync(userId.ToString());
            if (entity == null)
            {
                entity = new UserPersonalAccessToken
                {
                    UserId = userId.ToString(),
                    PersonalAccessToken = token.ToString()
                };
                await _db.UserPersonalAccessTokens.AddAsync(entity);
            }
            else
            {
                entity.PersonalAccessToken = token.ToString();
            }

            await _db.SaveChangesAsync();
        }

        public async Task<PersonalAccessToken> LoadAsync(UserId userId)
        {
            var entity = await _db.UserPersonalAccessTokens.FindAsync(userId.ToString());
            return CreatePat(entity);
        }

        public PersonalAccessToken Load(UserId userId)
        {
            var entity = _db.UserPersonalAccessTokens.Find(userId.ToString());
            return CreatePat(entity);
        }

        private static PersonalAccessToken CreatePat(UserPersonalAccessToken entity)
        {
            return entity != null ? new PersonalAccessToken(entity.PersonalAccessToken) : PersonalAccessToken.Empty;
        }
    }
}
