﻿using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Docker.DotNet;
using Hayate.Core.Instances;

namespace Hayate.Containers.Docker
{
    public sealed class DockerContainerTtyStream : TtyStream
    {
        private readonly MultiplexedStream _stream;

        public DockerContainerTtyStream(MultiplexedStream stream)
        {
            _stream = stream;
        }

        public override bool IsClosed { get; }

        public override async Task WriteAsync(string s)
        {
            var buffer = Encoding.UTF8.GetBytes(s);
            await _stream.WriteAsync(buffer, 0, buffer.Length, CancellationToken.None);
        }

        public override void Dispose()
        {
            _stream.Dispose();
        }

        public override async Task<string> ReadAsync()
        {
            var buffer = new byte[1024];
            var result = await _stream.ReadOutputAsync(buffer, 0, buffer.Length, CancellationToken.None);
            return Encoding.UTF8.GetString(buffer, 0, result.Count);
        }

        public bool Is(MultiplexedStream stream)
        {
            return _stream == stream;
        }
    }
}
