﻿using System.Collections.Generic;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Containers.Docker
{
    public static class StatusConverter
    {
        private static readonly IDictionary<string, InstanceStatus> Map = new Dictionary<string, InstanceStatus>
        {
            {"created", InstanceStatus.Created},
            {"restarting", InstanceStatus.Restarting},
            {"running", InstanceStatus.Running},
            {"removing", InstanceStatus.Removing},
            {"paused", InstanceStatus.Paused},
            {"exited", InstanceStatus.Exited},
            {"dead", InstanceStatus.Dead}
        };

        public static InstanceStatus FromDockerContainer(string status)
        {
            return Map.TryGetValue(status, out var result) ? result : InstanceStatus.Unknown;
        }
    }
}