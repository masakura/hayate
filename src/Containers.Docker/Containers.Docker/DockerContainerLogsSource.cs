﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Containers.Docker
{
    public sealed class DockerContainerLogsSource : LogsSource
    {
        private readonly Stream _stream;

        public DockerContainerLogsSource(Stream stream)
        {
            _stream = stream;
        }

        public override bool IsClosed => !_stream.CanRead;

        public override async Task<string> ReadAsync()
        {
            // LogsContainer API のストリームはこんな形式のデータ
            // 01 00 00 00  00 00 00 06
            //              ~~~~~~~~~~~
            //              payload length
            // 68 65 6c 6c  6f 0a          hello\n
            // https://medium.com/@dhanushgopinath/reading-docker-container-logs-with-golang-docker-engine-api-702233fac044

            var header = new byte[8];
            var count = await _stream.ReadAsync(header, 0, header.Length);
            if (count < 8) return null;
            var size = header[7] + ((uint) header[6] << 8) + ((uint) header[5] << 16) + ((uint) header[4] << 24);
            var buffer = new byte[size];
            await _stream.ReadAsync(buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(buffer).Replace("\n", "\r\n");
        }

        public override void Dispose()
        {
            _stream?.Dispose();
        }
    }
}