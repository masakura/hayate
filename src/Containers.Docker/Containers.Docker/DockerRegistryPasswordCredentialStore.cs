﻿using System.Threading.Tasks;
using Hayate.Core.Tokens;

namespace Hayate.Containers.Docker
{
    public sealed class DockerRegistryPasswordCredentialStore : IDockerRegistryPasswordCredentialStore
    {
        private readonly IGitLabPatRegistrationService _patService;

        public DockerRegistryPasswordCredentialStore(IGitLabPatRegistrationService patService)
        {
            _patService = patService;
        }

        public async Task<DockerRegistryPasswordCredential> GetCurrentUserCredentialAsync()
        {
            var accessToken = await _patService.LoadPatAsync();
            return CreateCredential(accessToken);
        }

        public DockerRegistryPasswordCredential GetCurrentUserCredential()
        {
            var accessToken = _patService.LoadPat();
            return CreateCredential(accessToken);
        }

        private static DockerRegistryPasswordCredential CreateCredential(PersonalAccessToken accessToken)
        {
            return new DockerRegistryPasswordCredential(accessToken);
        }
    }
}
