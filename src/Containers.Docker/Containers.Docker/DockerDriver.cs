﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;
using Hayate.Core.Instances;
using Microsoft.Extensions.Options;

namespace Hayate.Containers.Docker
{
    public sealed class DockerDriver : IDriver
    {
        private readonly IDockerClient _client;
        private readonly DockerOptions _options;

        // ReSharper disable once MemberCanBePrivate.Global
        public DockerDriver(IDockerClient client,
            IOptions<DockerOptions> options)
        {
            _client = client;
            _options = options.Value;
        }

        public async Task<IImageId> PullAsync(IImageName imageName)
        {
            var credential = DockerRegistryPasswordCredentialScope.CurrentCredential;
            var authConfig = credential.ToAuthConfig();

            try
            {
                await _client.Images.CreateImageAsync(new ImagesCreateParameters {FromImage = imageName.ToString()},
                    authConfig,
                    new Progress<JSONMessage>());
            }
            catch (DockerApiException ex)
            {
                if (ex.Message.Contains("unauthorized:")) throw new RequireAuthenticateException(ex);
            }

            var image = await _client.Images.InspectImageAsync(imageName.ToString());
            return new DockerImageId(image.ID);
        }

        public Task<IContainerId> CreateContainerAsync(IImageId imageId)
        {
            return CreateContainerAsync(imageId.ToString());
        }

        public async Task StartContainerAsync(IContainerId containerId)
        {
            await _client.Containers.StartContainerAsync(containerId.ToString(), new ContainerStartParameters());
        }

        public async Task RestartContainerAsync(IContainerId containerId)
        {
            await _client.Containers.RestartContainerAsync(containerId.ToString(), new ContainerRestartParameters());
        }

        public async Task<InstanceStatus> ContainerStatusAsync(IContainerId containerId)
        {
            var result = await _client.Containers.InspectContainerAsync(containerId.ToString());
            return StatusConverter.FromDockerContainer(result.State.Status);
        }

        public async Task<EndPoint> EndPointAsync(IContainerId containerId)
        {
            var result = await _client.Containers.InspectContainerAsync(containerId.ToString());

            return GetEndPointPort(result);
        }

        public async Task<LogsSource> LogsContainerAsync(IContainerId containerId)
        {
            var parameters = new ContainerLogsParameters
            {
                Tail = "25",
                Follow = true,
                ShowStdout = true,
                ShowStderr = true
            };

            var stream = await _client.Containers.GetContainerLogsAsync(containerId.ToString(),
                parameters);

            return new DockerContainerLogsSource(stream);
        }

        public async Task<TtyStream> ExecContainerAsync(IContainerId containerId, string cmd)
        {
            var response = await _client.Containers.ExecCreateContainerAsync(containerId.ToString(),
                new ContainerExecCreateParameters
                {
                    AttachStdin = true,
                    AttachStdout = true,
                    AttachStderr = true,
                    Tty = true,
                    Cmd = new List<string>(new[] {cmd})
                });

            var stream = await _client.Containers.StartWithConfigContainerExecAsync(response.ID,
                new ContainerExecStartParameters
                {
                    AttachStdin = true,
                    AttachStdout = true,
                    AttachStderr = true,
                    Tty = true
                });

            return new DockerContainerTtyStream(stream);
        }

        public static DockerDriver Create(IDockerClient client, IOptions<DockerOptions> options)
        {
            return new DockerDriver(client, options);
        }

        private EndPoint GetEndPointPort(ContainerInspectResponse result)
        {
            var hostPort = HostPortUtility.GetHostPort(result.NetworkSettings.Ports);

            return hostPort == 0 ? EndPoint.Empty : new EndPoint($"http://{_options.DockerHost}:{hostPort}");
        }

        private async Task<IContainerId> CreateContainerAsync(string image)
        {
            var container = await _client.Containers.CreateContainerAsync(
                new CreateContainerParameters
                {
                    Image = image,
                    HostConfig = new HostConfig
                    {
                        PublishAllPorts = true
                    }
                });
            return new DockerContainerId(container.ID);
        }
    }
}
