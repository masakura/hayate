﻿using System;
using System.Net.Http.Headers;
using System.Text;
using Docker.DotNet.Models;
using Hayate.Core.Tokens;

namespace Hayate.Containers.Docker
{
    public struct DockerRegistryPasswordCredential
    {
        private readonly string _password;
        private readonly string _username;

        public DockerRegistryPasswordCredential(PersonalAccessToken token) : this("hayate", token.ToString())
        {
        }

        public DockerRegistryPasswordCredential(string username, string password)
        {
            _username = username;
            _password = password;
        }

        public bool HasPassword => !string.IsNullOrEmpty(_password);
        public static DockerRegistryPasswordCredential Empty = new DockerRegistryPasswordCredential();

        public AuthConfig ToAuthConfig()
        {
            return HasPassword
                ? new AuthConfig
                {
                    Username = _username,
                    Password = _password
                }
                : null;
        }

        public string ToBase64Text()
        {
            if (!HasPassword) return null;

            var bytes = Encoding.UTF8.GetBytes($"{_username}:{_password}");
            return Convert.ToBase64String(bytes);
        }

        public AuthenticationHeaderValue ToAuthenticationHeader()
        {
            if (!HasPassword) return null;

            return new AuthenticationHeaderValue("Basic", ToBase64Text());
        }
    }
}
