﻿using Hayate.Json;
using Hayate.Utility;
using Newtonsoft.Json;

namespace Hayate.Containers.Docker
{
    [JsonConverter(typeof(DockerTagNameJsonConverter))]
    public struct DockerTagName
    {
        private readonly string _name;

        public DockerTagName(string name)
        {
            Validator.IsNotNull(name, nameof(name));
            Validator.IsNotEmpty(name, nameof(name));

            _name = name;
        }

        public static DockerTagName Default => new DockerTagName("latest");

        public override string ToString()
        {
            return _name;
        }

        private sealed class DockerTagNameJsonConverter : StructJsonConverter<DockerTagName>
        {
            protected override DockerTagName Parse(object value)
            {
                return new DockerTagName(value.ToString());
            }
        }
    }
}