﻿using System.Threading.Tasks;

namespace Hayate.Containers.Docker
{
    public interface IDockerRegistryPasswordCredentialStore
    {
        Task<DockerRegistryPasswordCredential> GetCurrentUserCredentialAsync();
        DockerRegistryPasswordCredential GetCurrentUserCredential();
    }
}