﻿using Hayate.Utility;

namespace Hayate.Containers.Docker
{
    public struct DockerImageId : IImageId
    {
        private readonly string _id;

        public DockerImageId(string id)
        {
            Validator.IsNotNull(id, nameof(id));
            Validator.IsNotEmpty(id, nameof(id));

            _id = id;
        }

        public override string ToString()
        {
            return _id;
        }
    }
}