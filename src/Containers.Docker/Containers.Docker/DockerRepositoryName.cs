﻿using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Json;
using Hayate.Utility;
using Newtonsoft.Json;

namespace Hayate.Containers.Docker
{
    [JsonConverter(typeof(DockerRepositoryNameJsonConverter))]
    public struct DockerRepositoryName : IRepositoryName
    {
        private readonly string _name;

        public DockerRepositoryName(string name)
        {
            Validator.IsNotNull(name, nameof(name));
            Validator.IsNotEmpty(name, nameof(name));

            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }

        private sealed class DockerRepositoryNameJsonConverter : StructJsonConverter<DockerRepositoryName>
        {
            protected override DockerRepositoryName Parse(object value)
            {
                return new DockerRepositoryName(value.ToString());
            }
        }
    }
}