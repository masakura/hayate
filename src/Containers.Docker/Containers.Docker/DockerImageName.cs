﻿using System;
using Hayate.Core.Instances;
using Hayate.Utility;

namespace Hayate.Containers.Docker
{
    public struct DockerImageName : IImageName
    {
        public DockerRepositoryName Repository { get; }
        public DockerTagName Tag { get; }

        public DockerImageName(DockerRepositoryName repositoryName) : this(repositoryName, DockerTagName.Default)
        {
        }

        public DockerImageName(DockerRepositoryName repositoryName, DockerTagName tagName)
        {
            Repository = repositoryName;
            Tag = tagName;
        }

        public override string ToString()
        {
            // ReSharper disable RedundantToStringCallForValueType
            // ReSharper disable ImpureMethodCallOnReadonlyValueField
            return $"{Repository.ToString()}:{Tag.ToString()}";
            // ReSharper restore RedundantToStringCallForValueType
            // ReSharper restore ImpureMethodCallOnReadonlyValueField
        }

        public static DockerImageName Parse(string s)
        {
            Validator.IsNotNull(s, nameof(s));

            var parts = s.Split(new[] {':'}, 2);

            // ReSharper disable once HeapView.ObjectAllocation.Evident
            if (parts.Length == 0 || parts.Length > 2) throw new InvalidOperationException($"{s} をパースできません");

            var image = new DockerRepositoryName(parts[0]);
            var tag = parts.Length == 2 ? new DockerTagName(parts[1]) : DockerTagName.Default;

            return new DockerImageName(image, tag);
        }

        public DockerImageName AppendHostAndPort(Uri endpoint)
        {
            return Parse($"{endpoint.Host}{GetPortString(endpoint)}/{ToString()}");    
        }

        private string GetPortString(Uri endpoint)
        {
            if (endpoint.Scheme == "http" && endpoint.Port == 80) return string.Empty;
            if (endpoint.Scheme == "https" && endpoint.Port == 443) return string.Empty;
            return $":{endpoint.Port}";
        }
    }
}