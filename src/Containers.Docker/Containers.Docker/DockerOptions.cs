﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using Docker.DotNet;
using Docker.DotNet.X509;

namespace Hayate.Containers.Docker
{
    public sealed class DockerOptions
    {
        /// <summary>
        ///     Docker Engine の接続先を取得または設定します。
        /// </summary>
        public Uri Endpoint { get; set; } = CreateDefaultEndpoint();

        /// <summary>
        ///     Docker Engine に接続するときに利用するクライアント証明書パス (pfx) を取得または設定します。
        /// </summary>
        public string CertFile { get; set; }

        /// <summary>
        ///     コンテナが公開するポートの接続先 (docker.example.com) を取得または設定します。
        /// </summary>
        public string DockerHost { get; set; } = "localhost";
        
        private static Uri CreateDefaultEndpoint()
        {
            return RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                ? new Uri("npipe://./pipe/docker_engine")
                : new Uri("unix:///var/run/docker.sock");
        }

        internal Credentials GetCredentials()
        {
            if (string.IsNullOrEmpty(CertFile))
                return null;

            var credentials = new CertificateCredentials(new X509Certificate2(CertFile));
            // TODO サーバー証明書検証をしっかりする
            credentials.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
            return credentials;
        }
    }
}