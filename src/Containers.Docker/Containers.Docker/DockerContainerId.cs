﻿using Hayate.Utility;

namespace Hayate.Containers.Docker
{
    public struct DockerContainerId : IContainerId
    {
        private readonly string _id;

        public DockerContainerId(string id)
        {
            Validator.IsNotNull(id, nameof(id));
            Validator.IsNotEmpty(id, nameof(id));

            _id = id;
        }

        public override string ToString()
        {
            return _id;
        }
    }
}