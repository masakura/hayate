﻿using System;
using Docker.DotNet;
using Hayate.Containers;
using Hayate.Containers.Docker;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class DockerServiceCollectionExtensions
    {
        public static void AddDockerImplements(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DockerOptions>(options =>
            {
                new OptionContext(configuration)
                    .Override("Endpoint", value => options.Endpoint = new Uri(value))
                    .Override("CertFile", value => options.CertFile = value)
                    .Override("DockerHost", value => options.DockerHost = value);
            });

            services.AddTransient<IDockerClient>(provider =>
            {
                var options = provider.GetService<IOptions<DockerOptions>>().Value;

                DockerClientConfiguration config;
                var credentials = options.GetCredentials();

                if (credentials != null)
                    config = new DockerClientConfiguration(options.Endpoint, credentials);
                else
                    config = new DockerClientConfiguration(options.Endpoint);

                return config.CreateClient();
            });

            services.AddTransient<IDockerRegistryPasswordCredentialStore, DockerRegistryPasswordCredentialStore>();

            services.AddTransient<DockerDriver, DockerDriver>();
            services.AddTransient<IDriver>(provider =>
            {
                var driver = provider.GetRequiredService<DockerDriver>();
                var logger = provider.GetRequiredService<ILogger<DockerDriver>>();
                return new LoggingDriver(driver, logger);
            });
        }

        private sealed class OptionContext
        {
            private readonly IConfiguration _configuration;

            public OptionContext(IConfiguration configuration)
            {
                _configuration = configuration.GetSection("Docker");
            }

            public OptionContext Override(string key, Action<string> setter)
            {
                return Override(key, setter, () => null);
            }

            public OptionContext Override(string key, Action<string> setter, Func<string> @default)
            {
                var value = _configuration[key];
                if (string.IsNullOrEmpty(value)) value = @default();
                if (value != null) setter(value);
                return this;
            }
        }
    }
}
