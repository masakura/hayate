﻿using System.Collections.Generic;
using System.Linq;
using Docker.DotNet.Models;

namespace Hayate.Containers.Docker
{
    public static class HostPortUtility
    {
        public static int GetHostPort(IDictionary<string, IList<PortBinding>> ports)
        {
            var bindings = ports.Values.SelectMany(portBinding => portBinding);

            return Parse(bindings.LastOrDefault()?.HostPort);
        }

        private static int Parse(string value)
        {
            return int.TryParse(value, out var p) ? p : 0;
        }
    }
}