using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hayate.Containers.Docker
{
    public sealed class DockerRegistryPasswordCredentialScope : IDisposable
    {
        private static readonly AsyncLocal<DockerRegistryPasswordCredentialScope> AsyncLocalScope =
            new AsyncLocal<DockerRegistryPasswordCredentialScope>();

        private readonly IDockerRegistryPasswordCredentialStore _store;

        private DockerRegistryPasswordCredentialScope(IDockerRegistryPasswordCredentialStore store)
        {
            _store = store;

            AsyncLocalScope.Value = this;
        }

        private static IDockerRegistryPasswordCredentialStore Current
        {
            get
            {
                var valueStore = AsyncLocalScope.Value._store;

                if (valueStore == null)
                    throw new InvalidOperationException(
                        $"Could not found {nameof(IDockerRegistryPasswordCredentialStore)} in the current scope.");

                return valueStore;
            }
        }

        public static DockerRegistryPasswordCredential CurrentCredential => Current.GetCurrentUserCredential();

        public void Dispose()
        {
            AsyncLocalScope.Value = null;
        }

        public static DockerRegistryPasswordCredentialScope NewScope(DockerRegistryPasswordCredential credential)
        {
            return new DockerRegistryPasswordCredentialScope(new StaticCredentialStore(credential));
        }

        public static IDisposable NewScope(IDockerRegistryPasswordCredentialStore store)
        {
            return new DockerRegistryPasswordCredentialScope(store);
        }

        private sealed class StaticCredentialStore : IDockerRegistryPasswordCredentialStore
        {
            private readonly DockerRegistryPasswordCredential _credential;

            public StaticCredentialStore(DockerRegistryPasswordCredential credential)
            {
                _credential = credential;
            }

            public Task<DockerRegistryPasswordCredential> GetCurrentUserCredentialAsync()
            {
                return Task.FromResult(_credential);
            }

            public DockerRegistryPasswordCredential GetCurrentUserCredential()
            {
                return _credential;
            }
        }
    }
}
