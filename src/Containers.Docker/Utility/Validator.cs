﻿using System;

namespace Hayate.Utility
{
    internal static class Validator
    {
        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
        public static void IsNotNull(object value, string name)
        {
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            if (value == null) throw new ArgumentNullException(name);
        }

        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
        public static void IsNotEmpty(string value, string name)
        {
            // ReSharper disable once HeapView.ObjectAllocation.Evident
            if (value == "") throw new ArgumentException("空文字列は許可されていません", name);
        }
    }
}