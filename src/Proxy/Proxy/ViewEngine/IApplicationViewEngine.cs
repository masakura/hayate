﻿using System.Threading.Tasks;

namespace Hayate.Proxy.ViewEngine
{
    /// <summary>
    ///     アプリの状態表示などで利用するビューエンジン。
    /// </summary>
    public interface IApplicationViewEngine
    {
        /// <summary>
        ///     ビューの結果を取得します。
        /// </summary>
        /// <param name="key">キーもしくはファイル名。</param>
        /// <param name="model">ビューで利用するモデル。</param>
        /// <returns>ビューの結果。</returns>
        Task<string> CompileRenderAsync(string key, object model = null);
    }
}