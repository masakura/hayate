﻿using System.Threading.Tasks;
using Hayate.Proxy.Applications;
using Hayate.Proxy.ViewEngine;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Http
{
    internal static class HttpResponseExtensions
    {
        private static async Task WriteRazorAsync(this HttpResponse response, string key, object model = null)
        {
            response.ContentType = "text/html; charset=utf-8";

            var engine = response.HttpContext.RequestServices.GetRequiredService<IApplicationViewEngine>();
            var body = await engine.CompileRenderAsync(key, model);
            await response.WriteAsync(body);
        }

        public static Task WriteStatusAsync(this HttpResponse response, IApplicationStatus status)
        {
            return response.WriteRazorAsync("Views/Status.cshtml", status);
        }
    }
}