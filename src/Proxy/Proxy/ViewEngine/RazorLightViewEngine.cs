﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using RazorLight;

namespace Hayate.Proxy.ViewEngine
{
    public sealed class RazorLightViewEngine : IApplicationViewEngine
    {
        private const string Default = "./";
        private readonly RazorLightEngine _engine;

        /// <summary>
        ///     ビューが格納されているルートディレクトリを指定して <see cref="RazorLightEngine" /> を初期化します。
        /// </summary>
        /// <param name="root">ビューテンプレートの格納先。</param>
        public RazorLightViewEngine(string root)
        {
            _engine = new RazorLightEngineBuilder()
                .UseFilesystemProject(root)
                .UseMemoryCachingProvider()
                .Build();
        }

        public Task<string> CompileRenderAsync(string key, object model = null)
        {
            return _engine.CompileRenderAsync(key, model ?? new { });
        }

        public static IApplicationViewEngine Create(IServiceProvider services, string relative = Default)
        {
            return Create(services.GetRequiredService<IHostingEnvironment>(), relative);
        }

        private static RazorLightViewEngine Create(IHostingEnvironment env, string relative = Default)
        {
            var root = env.ContentRootFileProvider.GetFileInfo(relative).PhysicalPath;
            return new RazorLightViewEngine(root);
        }
    }
}