﻿using Hayate.Proxy.ViewEngine;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.Dependency.Extensions
{
    internal static class ViewEngineServiceCollectionExtensions
    {
        public static void AddProxyViewEngine(this IServiceCollection services)
        {
            services.AddSingleton(provider => RazorLightViewEngine.Create(provider));
        }
    }
}