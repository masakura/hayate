﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hayate.Proxy.Converters
{
    internal sealed class NullHttpContentCreator : HttpContentCreator
    {
        public static readonly HttpContentCreator Instance = new NullHttpContentCreator();

        private NullHttpContentCreator()
        {
        }

        public override Task<HttpContent> FromAsync(HttpContent content, Func<string, string> converter)
        {
            return Task.FromResult(content);
        }

        protected override Task<HttpContent> CreateHttpContentAsync(string body,
            Encoding encoding)
        {
            return Task.FromResult<HttpContent>(null);
        }

        protected override Task<string> ReadStringAsync(HttpContent content, Encoding encoding)
        {
            return Task.FromResult<string>(null);
        }
    }
}