﻿using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    public sealed class HtmlPathResponseTranslator : ResponseTranslator
    {
        public override async Task TranslateAsync(HttpResponseMessage response, EndPoint original, EndPoint target)
        {
            if (!IsHtml(response)) return;

            var contentFactory = HttpContentCreator.Create(response.Content);
            var content = await contentFactory.FromAsync(response.Content, text => ReplacePath(text, original, target));
            CopyHeaders(response.Content, content);
            response.Content = content;
        }

        private static void CopyHeaders(HttpContent source, HttpContent destination)
        {
            var length = destination.Headers.ContentLength;
            destination.Headers.Clear();
            foreach (var header in source.Headers) destination.Headers.Add(header.Key, header.Value.ToArray());

            destination.Headers.ContentLength = length;
        }

        private static bool IsHtml(HttpResponseMessage response)
        {
            return response?.Content?.Headers?.ContentType?.MediaType == "text/html";
        }

        private static string ReplacePath(string text, EndPoint original, EndPoint target)
        {
            return Regex.Replace(text, "(href|src|action)(=)('[^']+'|\"[^\"]+\")", match =>
            {
                var v = new
                {
                    Attribute = match.Groups[1],
                    Equal = match.Groups[2],
                    Value = RemoveQuote(match.Groups[3].Value),
                    Quote = GetQuote(match.Groups[3].Value)
                };
                var converted = ProxyUriConverter.Instance.Convert(v.Value, original, target);

                return $"{v.Attribute}{v.Equal}{v.Quote}{converted}{v.Quote}";
            });
        }

        private static char GetQuote(string value)
        {
            return value.FirstOrDefault();
        }

        private static string RemoveQuote(string value)
        {
            value = value.Substring(1);
            value = value.Substring(0, value.Length - 1);
            return value;
        }
    }
}