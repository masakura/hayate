﻿using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hayate.Proxy.Converters
{
    public sealed class GZipHttpContentCreator : HttpContentCreator
    {
        private static readonly HttpContentCreator Instance = new GZipHttpContentCreator();

        private GZipHttpContentCreator()
        {
        }

        public new static HttpContentCreator Create(HttpContent content)
        {
            return content.Headers.ContentEncoding.Contains("gzip") ? Instance : null;
        }

        protected override async Task<HttpContent> CreateHttpContentAsync(string body, Encoding encoding)
        {
            return await GZipFromAsync(body, encoding);
        }

        protected override async Task<string> ReadStringAsync(HttpContent content, Encoding encoding)
        {
            return await GUnzipFromAsync(content, encoding);
        }

        public static async Task<string> GUnzipFromAsync(HttpContent content, Encoding encoding = null)
        {
            if (encoding == null) encoding = Encoding.UTF8;

            using (var gunzip = new GZipStream(await content.ReadAsStreamAsync(), CompressionMode.Decompress))
            using (var reader = new StreamReader(gunzip, encoding))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public static async Task<HttpContent> GZipFromAsync(string body, Encoding encoding = null)
        {
            if (encoding == null) encoding = Encoding.UTF8;

            using (var buffer = new MemoryStream())
            using (var gzip = new GZipStream(buffer, CompressionMode.Compress))
            {
                var bytes = encoding.GetBytes(body);
                await gzip.WriteAsync(bytes, 0, bytes.Length);
                gzip.Flush();
                buffer.Flush();

                return new ByteArrayContent(buffer.ToArray());
            }
        }
    }
}