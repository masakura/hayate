﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    public sealed class AggregateResponseTranslator : ResponseTranslator
    {
        private readonly IEnumerable<ResponseTranslator> _translators;

        public AggregateResponseTranslator(IEnumerable<ResponseTranslator> translators)
        {
            _translators = translators ?? throw new ArgumentNullException(nameof(translators));
        }

        public override async Task TranslateAsync(HttpResponseMessage response, EndPoint original, EndPoint target)
        {
            foreach (var translator in _translators)
                await translator.TranslateAsync(response, original, target);
        }
    }
}