﻿using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    /// <summary>
    ///     HTTP レスポンスを書き換えます。
    /// </summary>
    public abstract class ResponseTranslator
    {
        public static ResponseTranslator Default { get; } = new ResponseTranslator[]
        {
            new LocationHeaderResponseTranslator(),
            new HtmlPathResponseTranslator(),
            new CookiePathResponseTranslator()
        }.ToAggregate();

        /// <summary>
        ///     HTTP レスポンスを書き換えます。
        /// </summary>
        /// <param name="response">HttpClient でアクセスしたレスポンス。</param>
        /// <param name="original">接続元のエンドポイント。</param>
        /// <param name="target">接続先の円dポイント。</param>
        /// <remarks>
        ///     プロキシアクセスで接続元へレスポンスを返す前に内容を書き換えます。
        /// </remarks>
        public abstract Task TranslateAsync(HttpResponseMessage response, EndPoint original, EndPoint target);
    }
}