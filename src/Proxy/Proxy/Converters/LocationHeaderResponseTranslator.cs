﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    /// <summary>
    ///     HTTP Location ヘッダーを書き換えます。
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         接続先のレスポンスヘッダーに Location が含まれる場合、アクセス元から見た URL に書き換えます。
    ///     </para>
    ///     <para>
    ///         Location ヘッダーが以下のように帰ってきたとします。
    ///     </para>
    ///     <example>
    ///         Location: http://example.com/login
    ///     </example>
    ///     <para>
    ///         これをアクセス元から見た URL に書き換えます。
    ///     </para>
    ///     <example>
    ///         Location: http://localhost:5000/_/proxy/example/login
    ///     </example>
    /// </remarks>
    public sealed class LocationHeaderResponseTranslator : ResponseTranslator
    {
        /// <inheritdoc cref="ResponseTranslator.TranslateAsync" />
        public override Task TranslateAsync(HttpResponseMessage response, EndPoint original, EndPoint target)
        {
            var headers = response.Headers ?? throw new ArgumentNullException("response.Headers");

            var location = headers?.Location?.ToString();
            var replaced = ProxyUriConverter.Instance.Convert(location, original, target);

            if (replaced != null) headers.Location = new Uri(replaced);

            return Task.CompletedTask;
        }
    }
}