﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hayate.Proxy.Converters
{
    public sealed class PlainHttpContentCreator : HttpContentCreator
    {
        private static readonly PlainHttpContentCreator Instance = new PlainHttpContentCreator();

        public new static HttpContentCreator Create(HttpContent content)
        {
            return content.Headers.ContentEncoding.Count == 0 ? Instance : null;
        }

        protected override Task<HttpContent> CreateHttpContentAsync(string body, Encoding encoding)
        {
            return Task.FromResult<HttpContent>(new StringContent(body));
        }

        protected override Task<string> ReadStringAsync(HttpContent content, Encoding encoding)
        {
            return content.ReadAsStringAsync();
        }
    }
}