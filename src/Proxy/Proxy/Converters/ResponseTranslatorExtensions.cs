﻿using System;
using System.Collections.Generic;

namespace Hayate.Proxy.Converters
{
    public static class ResponseTranslatorExtensions
    {
        public static ResponseTranslator ToAggregate(this IEnumerable<ResponseTranslator> translators)
        {
            if (translators == null) throw new ArgumentNullException(nameof(translators));
            return new AggregateResponseTranslator(translators);
        }
    }
}