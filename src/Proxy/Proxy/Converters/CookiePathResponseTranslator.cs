﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    /// <summary>
    ///     Cookie のパスを書き換えます。
    /// </summary>
    public sealed class CookiePathResponseTranslator : ResponseTranslator
    {
        public override Task TranslateAsync(HttpResponseMessage response, EndPoint original, EndPoint target)
        {
            if (!response.Headers.TryGetValues("Set-Cookie", out var values)) return Task.CompletedTask;
            
            var replaced = values
                .Select(value => ConvertCookiePath(value, original, target))
                .ToArray();

            if (replaced == null) return Task.CompletedTask;

            response.Headers.Remove("Set-Cookie");
            response.Headers.Add("Set-Cookie", replaced);

            return Task.CompletedTask;
        }

        private string ConvertCookiePath(string value, EndPoint original, EndPoint target)
        {
            return Regex.Replace(value, "(path=)([^;\\s]+)", match =>
            {
                var v = new
                {
                    Key = match.Groups[1],
                    Value = ProxyUriConverter.Instance.Convert(match.Groups[2].Value, original, target)
                };

                return $"{v.Key}{v.Value}";
            });
        }
    }
}