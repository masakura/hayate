﻿using System;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Converters
{
    /// <summary>
    ///     プロキシのために URL を変換します。
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         プロキシのために URL を変換します。
    ///     </para>
    ///     <para>
    ///         例えば、接続先が http://foo.example.com/bar だった時に http://localhost:5000/_/foo/proxy/bar と書き換えます。
    ///     </para>
    /// </remarks>
    public sealed class ProxyUriConverter
    {
        public static readonly ProxyUriConverter Instance = new ProxyUriConverter();

        // ReSharper disable once MemberCanBeMadeStatic.Global
        public string Convert(string uri, EndPoint original, EndPoint target)
        {
            if (uri == null) return null;

            if (uri.StartsWith("/"))
            {
                return new Uri(original.ToString()).AbsolutePath + uri.Substring(1);
            }
            
            var destination = target.ToString();
            if (!uri.StartsWith(destination)) return uri;

            return original + uri.Substring(destination.Length);
        }
    }
}