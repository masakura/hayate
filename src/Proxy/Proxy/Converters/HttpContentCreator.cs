﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hayate.Proxy.Converters
{
    /// <summary>
    ///     <see cref="HttpContent" /> インスタンスのクローンを作成します。
    /// </summary>
    public abstract class HttpContentCreator
    {
        public static HttpContentCreator Create(HttpContent content)
        {
            return PlainHttpContentCreator.Create(content) ??
                   GZipHttpContentCreator.Create(content) ??
                   NullHttpContentCreator.Instance;
        }

        /// <summary>
        ///     ボディ部分を変換しながら新しい <see cref="HttpContent" /> インスタンスを作成します。
        /// </summary>
        /// <param name="content">元になる <see cref="HttpContent" /> インスタンス。</param>
        /// <param name="converter">ボディを書き換える関数。</param>
        /// <returns><see cref="HttpContent" /> インスタンスのコピー。</returns>
        public virtual async Task<HttpContent> FromAsync(HttpContent content, Func<string, string> converter)
        {
            var encoding = GetEncoding(content);

            var body = await ReadStringAsync(content, encoding);

            return await CreateHttpContentAsync(converter(body), encoding);
        }

        /// <summary>
        ///     ボディを元に <see cref="HttpContent" /> インスタンスを作成します。
        /// </summary>
        /// <param name="body">ボディ文字列。</param>
        /// <param name="encoding">エンコーディング。</param>
        /// <returns><see cref="HttpContent" /> インスタンス。</returns>
        protected abstract Task<HttpContent> CreateHttpContentAsync(string body, Encoding encoding);

        /// <summary>
        ///     <see cref="HttpContent" /> インスタンスからボディを読み取ります。
        /// </summary>
        /// <param name="content"><see cref="HttpContent" /> インスタンス。</param>
        /// <param name="encoding">エンコーディング。</param>
        /// <returns>読み込んだボディの文字列。</returns>
        protected abstract Task<string> ReadStringAsync(HttpContent content, Encoding encoding);

        protected static Encoding GetEncoding(HttpContent content)
        {
            var charset = content.Headers.ContentType?.CharSet;
            if (string.IsNullOrEmpty(charset)) return Encoding.UTF8;

            return Encoding.GetEncoding(charset);
        }
    }
}