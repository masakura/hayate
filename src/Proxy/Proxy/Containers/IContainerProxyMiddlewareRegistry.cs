﻿using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy.Applications;

namespace Hayate.Proxy.Containers
{
    /// <summary>
    ///     コンテナへのプロキシを管理します。
    /// </summary>
    public interface IContainerProxyMiddlewareRegistry
    {
        IApplicationMiddleware FindOrCreate(EndPoint target);
    }
}