﻿using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy.Applications;

namespace Hayate.Proxy.Containers
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ContainerProxyMiddlewareRegistry : IContainerProxyMiddlewareRegistry,
        IApplicationMiddlewareRegistry
    {
        public async Task<IApplicationMiddleware> FindAsync(IApplication application)
        {
            var target = await application.EndPointAsync();
            return !target.IsEmpty ? new ContainerProxyMiddleware(target) : null;
        }

        public IApplicationMiddleware FindOrCreate(EndPoint target)
        {
            return new ContainerProxyMiddleware(target);
        }
    }
}