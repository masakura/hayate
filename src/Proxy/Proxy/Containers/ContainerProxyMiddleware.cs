﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Proxy.Applications;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Proxy;
using Microsoft.Extensions.Options;

namespace Hayate.Proxy.Containers
{
    public sealed class ContainerProxyMiddleware : IApplicationMiddleware
    {
        private readonly ProxyMiddleware _proxy;
        private readonly Func<HttpContext, Task> _requestErrorHandler;

        public ContainerProxyMiddleware(EndPoint target) : this(target, new ProxyHttpClientHandler(target),
            ErrorHandler)
        {
        }

        public ContainerProxyMiddleware(EndPoint target, HttpClientHandler handler,
            Func<HttpContext, Task> requestErrorHandler)
        {
            _requestErrorHandler = requestErrorHandler;
            var options = new OptionsWrapper<ProxyOptions>(ProxyMiddlewareOptionsFactory.Create(target, handler));
            _proxy = new ProxyMiddleware(context => Task.CompletedTask, options);
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _proxy.Invoke(context);
            }
            catch (HttpRequestException)
            {
                await _requestErrorHandler(context);
            }
        }

        private static Task ErrorHandler(HttpContext context)
        {
            return context.Response.WriteStatusAsync(WaitingConnected.Instance);
        }
    }
}