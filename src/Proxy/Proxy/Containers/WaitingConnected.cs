﻿using Hayate.Proxy.Applications;

namespace Hayate.Proxy.Containers
{
    public sealed class WaitingConnected : IApplicationStatus
    {
        private WaitingConnected()
        {
        }

        public static IApplicationStatus Instance { get; } = new WaitingConnected();

        public string Message => "アプリへの接続を待機しています。";
    }
}