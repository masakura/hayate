﻿using System.Net.Http;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Microsoft.AspNetCore.Builder;

namespace Hayate.Proxy.Containers
{
    public static class ProxyMiddlewareOptionsFactory
    {
        public static ProxyOptions Create(EndPoint target, HttpClientHandler handler)
        {
            var uri = target.ToUri();

            return new ProxyOptions
            {
                Scheme = uri.Scheme,
                Host = uri.Host,
                Port = uri.Port.ToString(),
                BackChannelMessageHandler = handler
            };
        }
    }
}