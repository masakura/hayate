﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;
using Hayate.Proxy.Converters;

namespace Hayate.Proxy.Containers
{
    /// <summary>
    ///     接続先へ接続する <see cref="HttpClientHandler" /> クラスの実装。
    /// </summary>
    public sealed class ProxyHttpClientHandler : HttpClientHandler
    {
        private readonly EndPoint _target;
        private readonly ResponseTranslator _translators;

        public ProxyHttpClientHandler(EndPoint target)
        {
            _target = target;
            _translators = ResponseTranslator.Default;

            // シンプルな HTTP Proxy として動作させるため、HttpClient の便利機能は邪魔
            // HttpClient が勝手にリダイレクトを処理するのを防ぐ
            AllowAutoRedirect = false;
            // HttpClient が勝手に Cookie を処理するのを防ぐ
            UseCookies = false;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var original = new EndPoint(request.Headers.GetValues("X-HAYATE-APP-BASE").FirstOrDefault());

            var response = await base.SendAsync(request, cancellationToken);

            await ReplaceAsync(response, original);

            return response;
        }

        private async Task ReplaceAsync(HttpResponseMessage response, EndPoint original)
        {
            await _translators.TranslateAsync(response, original, _target);
        }
    }
}