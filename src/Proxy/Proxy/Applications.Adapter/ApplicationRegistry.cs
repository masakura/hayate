﻿using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications.Adapter
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationRegistry : IApplicationRegistry
    {
        private readonly IInstanceRegistry _instances;

        public ApplicationRegistry(IInstanceRegistry instances)
        {
            _instances = instances;
        }

        public IApplication Find(InstanceId instanceId)
        {
            var instance = _instances.Find(instanceId);
            
            return new InstanceAdapter(instance);
        }
    }
}