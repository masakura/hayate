﻿using System.Collections.Generic;
using System.Linq;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications.Adapter
{
    public static class RequireStart
    {
        // TODO Instance 名前空間へ移動?
        
        private static readonly IEnumerable<InstanceStatus> RequireStarts = new HashSet<InstanceStatus>(new[]
        {
            InstanceStatus.Unknown,
            InstanceStatus.Removing, 
            InstanceStatus.Paused, 
            InstanceStatus.Exited,
            InstanceStatus.Dead, 
            InstanceStatus.Pending
        });

        public static bool Require(InstanceStatus status)
        {
            return RequireStarts.Contains(status);
        }
    }
}