﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications.Adapter
{
    /// <summary>
    ///     <see cref="Instance" /> インスタンスアダプター。
    /// </summary>
    // ReSharper disable once InheritdocConsiderUsage
    public sealed class InstanceAdapter : IApplication
    {
        private readonly IInstance _instance;

        public InstanceAdapter(IInstance instance)
        {
            _instance = instance;
        }

        public InstanceId InstanceId => _instance.Id;

        public Task<EndPoint> EndPointAsync()
        {
            return _instance.EndPointAsync();
        }

        public async Task<IApplicationStatus> StatusAsync()
        {
            var status = await _instance.StatusAsync();
            return InstanceStatusAdapter.Create(status);
        }

        public async Task<bool> RequireStartAsync()
        {
            return RequireStart.Require(await _instance.StatusAsync());
        }
    }
}