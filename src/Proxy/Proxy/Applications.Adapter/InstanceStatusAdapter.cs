﻿using System.Collections.Generic;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications.Adapter
{
    public sealed class InstanceStatusAdapter : IApplicationStatus
    {
        private static readonly IDictionary<InstanceStatus, InstanceStatusAdapter> Cache =
            new Dictionary<InstanceStatus, InstanceStatusAdapter>();

        private readonly InstanceStatus _status;

        private InstanceStatusAdapter(InstanceStatus status)
        {
            _status = status;
        }

        public string Message => _status.ToString();

        private bool Equals(InstanceStatusAdapter other)
        {
            return Equals(_status, other._status);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            // ReSharper disable once MergeCastWithTypeCheck
            return obj is InstanceStatusAdapter && Equals((InstanceStatusAdapter) obj);
        }

        public override int GetHashCode()
        {
            return _status != null ? _status.GetHashCode() : 0;
        }

        public static InstanceStatusAdapter Create(InstanceStatus status)
        {
            if (!Cache.TryGetValue(status, out var value))
            {
                value = new InstanceStatusAdapter(status);
                Cache[status] = value;
            }

            return value;
        }
    }
}