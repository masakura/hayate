﻿using Hayate.Proxy.Applications;
using Hayate.Proxy.Applications.Adapter;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.Dependency.Extensions
{
    internal static class ProxyApplicationsAdapterServiceCollectionExtensions
    {
        public static void AddProxyApplicationsAdapter(this IServiceCollection services)
        {
            services.AddTransient<IApplicationRegistry, ApplicationRegistry>();
        }
    }
}
