﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hayate.Proxy.Applications;
using Hayate.Proxy.Containers;

namespace Hayate.Proxy
{
    public sealed class CompositeApplicationMiddlewareRegistry : IApplicationMiddlewareRegistry
    {
        private readonly IEnumerable<IApplicationMiddlewareRegistry> _registries =
            new List<IApplicationMiddlewareRegistry>
            {
                new ContainerProxyMiddlewareRegistry(),
                RedirectToEnvironmentManagerMiddlewareRegistry.Create()
                ,
                ApplicationStatusMiddlewareRegistry.Create()
            }.AsReadOnly();
            

        public async Task<IApplicationMiddleware> FindAsync(IApplication application)
        {
            foreach (var registry in _registries)
            {
                var middleware = await registry.FindAsync(application);
                if (middleware != null) return middleware;
            }

            return null;
        }
    }
}