﻿using System.Threading.Tasks;
using Hayate.Core.Instances;
using Microsoft.AspNetCore.Http;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     インスタンス管理画面にリダイレクトをするミドルウェア。
    /// </summary>
    /// <remarks>
    ///     インスタンスが起動していない場合に利用する、インスタンス管理画面にリダイレクトするミドルウェアです。
    ///     管理画面でインスタンスを起動してからアプリを利用できるようにするためのものです。
    /// </remarks>
    // ReSharper disable once InheritdocConsiderUsage
    public sealed class RedirectToEnvironmentManagerMiddleware : IApplicationMiddleware
    {
        public RedirectToEnvironmentManagerMiddleware(InstanceId instanceId)
        {
            InstanceId = instanceId;
        }

        public InstanceId InstanceId { get; }

        public Task Invoke(HttpContext context)
        {
            context.Response.Redirect(
                $"/Redirect/FromInstanceId?instanceId={InstanceId.ToString()}");
            return Task.CompletedTask;
        }
    }
}