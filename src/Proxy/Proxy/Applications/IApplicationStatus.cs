﻿namespace Hayate.Proxy.Applications
{
    public interface IApplicationStatus
    {
        string Message { get; }
    }
}