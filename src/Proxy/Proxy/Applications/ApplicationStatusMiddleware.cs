﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリケーションの状態を表示するミドルウェア。
    /// </summary>
    /// <remarks>
    ///     アプリケーションの状態 "Pulling" や "Running" を表示します。
    /// </remarks>
    // ReSharper disable once InheritdocConsiderUsage
    public sealed class ApplicationStatusMiddleware : IApplicationMiddleware
    {
        private readonly Func<HttpContext, IApplicationStatus, Task> _handler;

        public ApplicationStatusMiddleware(IApplicationStatus status) : this(status, WriteAsync)
        {
        }

        public ApplicationStatusMiddleware(IApplicationStatus status,
            Func<HttpContext, IApplicationStatus, Task> handler)
        {
            _handler = handler;
            Status = status;
        }

        public IApplicationStatus Status { get; }

        public Task Invoke(HttpContext context)
        {
            return _handler(context, Status);
        }

        private static Task WriteAsync(HttpContext context, IApplicationStatus status)
        {
            return context.Response.WriteStatusAsync(status);
        }
    }
}