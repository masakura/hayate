﻿using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationMiddlewareProvider : IApplicationMiddlewareProvider
    {
        private readonly IApplicationRegistry _applications;
        private readonly IApplicationMiddlewareRegistry _middlewares;

        public ApplicationMiddlewareProvider(IApplicationRegistry applications,
            IApplicationMiddlewareRegistry middlewares)
        {
            _applications = applications;
            _middlewares = middlewares;
        }

        public Task<IApplicationMiddleware> FindAsync(InstanceId instanceId)
        {
            var application = _applications.Find(instanceId);
            return _middlewares.FindAsync(application);
        }
    }
}