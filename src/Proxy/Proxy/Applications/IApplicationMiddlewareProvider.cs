﻿using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリケーションを処理するミドルウェアを管理します。
    /// </summary>
    public interface IApplicationMiddlewareProvider
    {
        /// <summary>
        ///     アプリケーションを処理するミドルウェアを取得します。
        /// </summary>
        /// <param name="instanceId">インスタンス ID。</param>
        /// <returns>ミドルウェア。</returns>
        Task<IApplicationMiddleware> FindAsync(InstanceId instanceId);
    }
}