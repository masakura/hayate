﻿using System.Threading.Tasks;
using Hayate.Containers;
using Microsoft.AspNetCore.Http;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリを処理する ASP.NET Core 用ミドルウェア。
    /// </summary>
    public interface IApplicationMiddleware
    {
        Task Invoke(HttpContext context);
    }
}