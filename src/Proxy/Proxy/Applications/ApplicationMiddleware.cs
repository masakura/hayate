﻿using System;
using System.Threading.Tasks;
using Hayate.Core.Instances;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリケーションを処理するミドルウェア。
    /// </summary>
    /// <summary>
    ///     <para>
    ///         アプリケーションを処理します。様々なサービスを呼び出し、適切な処理を行います。
    ///     </para>
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationMiddleware
    {
        public ApplicationMiddleware(RequestDelegate next)
        {
            if (next == null) throw new ArgumentNullException(nameof(next));
        }

        // ReSharper disable once UnusedMember.Global
        public async Task Invoke(HttpContext context, IApplicationMiddlewareProvider middlewares)
        {
            var instanceId = GetInstanceId(context);

            var request = context.Request;
            request.Path = "/" + context.GetRouteValue("all");

            var original =
                $"{request.Scheme}://{request.Host}/_/proxy/{instanceId.ToString()}/";

            context.Request.Headers.Add("X-HAYATE-APP-BASE", original);
            context.Request.Headers.Add("X-HAYATE-APP-HOST", request.Host.ToString());

            var middleware = await middlewares.FindAsync(instanceId);
            await middleware.Invoke(context);
        }

        private static InstanceId GetInstanceId(HttpContext context)
        {
            var id = context.GetRouteValue("instanceId").ToString();
            return new InstanceId(id);
        }
    }
}
