﻿using System.Threading.Tasks;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリケーションを処理するミドルウェアを管理します。
    /// </summary>
    public interface IApplicationMiddlewareRegistry
    {
        /// <summary>
        ///     アプリケーションを処理するミドルウェアを取得します。
        /// </summary>
        /// <param name="application">アプリケーション。</param>
        /// <returns>ミドルウェア。</returns>
        Task<IApplicationMiddleware> FindAsync(IApplication application);
    }
}