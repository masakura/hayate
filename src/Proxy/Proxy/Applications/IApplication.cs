﻿using System.Threading.Tasks;
using Hayate.Containers;
using Hayate.Core.Instances;
using Hayate.Instances;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     プロキシをするアプリケーション。
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// インスタンス ID を取得します。
        /// </summary>
        InstanceId InstanceId { get; }

        /// <summary>
        ///     アプリコンテナのエンドポイントを取得します。
        /// </summary>
        /// <returns>エンドポイント。</returns>
        Task<EndPoint> EndPointAsync();

        /// <summary>
        ///     アプリの状態を取得します。
        /// </summary>
        /// <returns>アプリの状態。</returns>
        Task<IApplicationStatus> StatusAsync();

        /// <summary>
        ///     アプリを開始する必要があることを取得します。
        /// </summary>
        /// <returns>アプリを開始する必要があります。</returns>
        Task<bool> RequireStartAsync();
    }
}