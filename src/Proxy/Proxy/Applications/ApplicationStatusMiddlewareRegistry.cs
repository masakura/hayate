﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hayate.Core.Instances;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     <see cref="ApplicationStatusMiddleware" /> インスタンスを管理します。
    /// </summary>
    // ReSharper disable once InheritdocConsiderUsage
    public abstract class ApplicationStatusMiddlewareRegistry : IApplicationMiddlewareRegistry
    {
        public abstract Task<IApplicationMiddleware> FindAsync(IApplication application);

        public static ApplicationStatusMiddlewareRegistry Create()
        {
            return new CacheRegistry(new DirectRegistry());
        }

        private sealed class DirectRegistry : ApplicationStatusMiddlewareRegistry
        {
            public override async Task<IApplicationMiddleware> FindAsync(IApplication application)
            {
                var status = await application.StatusAsync();
                return new ApplicationStatusMiddleware(status);
            }
        }

        private sealed class CacheRegistry : ApplicationStatusMiddlewareRegistry
        {
            private readonly IDictionary<InstanceId, IApplicationMiddleware> _cache =
                new Dictionary<InstanceId, IApplicationMiddleware>();

            private readonly ApplicationStatusMiddlewareRegistry _parent;

            public CacheRegistry(ApplicationStatusMiddlewareRegistry parent)
            {
                _parent = parent;
            }

            public override async Task<IApplicationMiddleware> FindAsync(IApplication application)
            {
                if (!_cache.TryGetValue(application.InstanceId, out var value))
                {
                    value = await _parent.FindAsync(application);
                    _cache[application.InstanceId] = value;
                }

                return value;
            }
        }
    }
}