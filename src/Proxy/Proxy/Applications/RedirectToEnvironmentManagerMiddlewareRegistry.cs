﻿using System.Threading.Tasks;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     <see cref="RedirectToEnvironmentManagerMiddleware" /> インスタンスを管理します。
    /// </summary>
    public sealed class RedirectToEnvironmentManagerMiddlewareRegistry : IApplicationMiddlewareRegistry
    {
        private RedirectToEnvironmentManagerMiddlewareRegistry()
        {
        }

        public async Task<IApplicationMiddleware> FindAsync(IApplication application)
        {
            var requireStart = await application.RequireStartAsync();

            return !requireStart ? null : new RedirectToEnvironmentManagerMiddleware(application.InstanceId);
        }

        public static RedirectToEnvironmentManagerMiddlewareRegistry Create()
        {
            return new RedirectToEnvironmentManagerMiddlewareRegistry();
        }
    }
}