﻿using Hayate.Proxy.Applications;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.Dependency.Extensions
{
    internal static class ProxyApplicationsServiceCollectionExtensions
    {
        public static void AddProxyApplications(this IServiceCollection services)
        {
            services.AddTransient<IApplicationMiddlewareProvider, ApplicationMiddlewareProvider>();
        }
    }
}
