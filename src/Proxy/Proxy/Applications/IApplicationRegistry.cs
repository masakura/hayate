﻿using Hayate.Core.Instances;

namespace Hayate.Proxy.Applications
{
    /// <summary>
    ///     アプリケーションを管理します。
    /// </summary>
    public interface IApplicationRegistry
    {
        /// <summary>
        ///     アプリケーションを取得します。
        /// </summary>
        /// <param name="instanceId">インスタンス ID。</param>
        /// <returns>アプリケーション。</returns>
        IApplication Find(InstanceId instanceId);
    }
}