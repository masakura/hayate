﻿using Hayate.Proxy;
using Hayate.Proxy.Applications;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace Microsoft.Dependency.Extensions
{
    public static class ProxyServiceCollectionExtensions
    {
        public static void AddApplicationProxyServices(this IServiceCollection services)
        {
            services.AddProxyApplications();
            services.AddProxyApplicationsAdapter();
            services.AddTransient<IApplicationMiddlewareRegistry, CompositeApplicationMiddlewareRegistry>();

            services.AddProxyViewEngine();
        }
    }
}
