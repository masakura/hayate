﻿using System.Threading.Tasks;

namespace Hayate.StreamBinding
{
    /// <summary>
    ///     データの読み取り元。
    /// </summary>
    public interface ISource
    {
        /// <summary>
        ///     読み取り元がクローズされているかどうかを取得します。
        /// </summary>
        bool IsClosed { get; }

        /// <summary>
        ///     データを読み取ります。
        /// </summary>
        /// <returns>読み込まれたデータ。</returns>
        Task<string> ReadAsync();
    }
}