﻿using System;
using System.Threading.Tasks;

namespace Hayate.StreamBinding
{
    public static class SourceExtensions
    {
        /// <summary>
        ///     ソースの内容を読み取ってデスティネーションに出力し続けます。
        /// </summary>
        /// <param name="source">ソース。</param>
        /// <param name="destination">デスティネーション。</param>
        public static async Task PipeAsync(this ISource source, IDestination destination)
        {
            await PerformPipeAsync(source, destination);

            Dispose(source, destination);
        }

        /// <summary>
        ///     ソースとデスティネーションを関連付けます。
        ///     ソースからディスティネーションへ、ディスティネーションからソースへ出力し続けます。
        /// </summary>
        /// <param name="source">ソース。</param>
        /// <param name="destination">デスティネーション。</param>
        public static async Task BindAsync(this ISource source, IDestination destination)
        {
            var task1 = PerformPipeAsync(source, destination);
            var task2 = PerformReversePipeAsync(destination, source);

            await Task.WhenAll(task1, task2);

            Dispose(source, destination);
        }

        private static async Task PerformPipeAsync(ISource source, IDestination destination)
        {
            while (!(source.IsClosed || destination.IsClosed))
            {
                var result = await ReadAsync(source);
                await WriteAsync(destination, result);
            }
        }

        private static async Task PerformReversePipeAsync(IDestination source, ISource destination)
        {
            var s = source as ISource;
            if (s == null) return;

            var d = destination as IDestination ?? NullDestination.Instance;
            await PerformPipeAsync(s, d);
        }

        private static void Dispose(ISource source, IDestination destination)
        {
            (source as IDisposable)?.Dispose();
            (destination as IDisposable)?.Dispose();
        }

        public static async Task PipeToNullAsync(this ISource source)
        {
            await PipeAsync(source, NullDestination.Instance);
        }

        private static async Task<string> ReadAsync(ISource source)
        {
            if (source.IsClosed) return null;

            return await source.ReadAsync();
        }

        private static async Task WriteAsync(IDestination destination, string result)
        {
            if (destination.IsClosed) return;

            await destination.WriteAsync(result);
        }
    }
}
