﻿using System.Threading.Tasks;

namespace Hayate.StreamBinding
{
    /// <summary>
    ///     データの書き込み先。
    /// </summary>
    public interface IDestination
    {
        /// <summary>
        ///     書き込み先がクローズされているかどうかを取得します。
        /// </summary>
        bool IsClosed { get; }

        /// <summary>
        ///     データを書き込みます。
        /// </summary>
        /// <param name="s">書き込むデータ。</param>
        Task WriteAsync(string s);
    }
}