﻿using System.Threading.Tasks;

namespace Hayate.StreamBinding
{
    /// <summary>
    ///     何もしないデスティネーション。すべてのデータを出力したふりをします。
    /// </summary>
    internal sealed class NullDestination : IDestination
    {
        private NullDestination()
        {
        }

        public static IDestination Instance { get; } = new NullDestination();

        public bool IsClosed { get; } = false;

        public Task WriteAsync(string s)
        {
            // 何もしません
            return Task.CompletedTask;
        }
    }
}