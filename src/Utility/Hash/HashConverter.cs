﻿using System.Security.Cryptography;
using System.Text;

namespace Hayate.Hash
{
    /// <summary>
    ///     ハッシュ値に変換します。
    /// </summary>
    public static class HashConverter
    {
        /// <summary>
        /// 文字列を SHA256 ハッシュ化します。
        /// </summary>
        /// <param name="s">文字列。</param>
        /// <returns>SHA256 な文字列。</returns>
        public static string ToSha256(string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            using (var crypto = new SHA256CryptoServiceProvider())
            {
                var hash = crypto.ComputeHash(bytes);
                var builder = new StringBuilder();
                foreach (var b in hash)
                {
                    builder.Append(b.ToString("x2"));
                }

                return builder.ToString();
            }
        }

    }
}