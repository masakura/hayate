﻿using System;
using Newtonsoft.Json;

namespace Hayate.Json
{
    public abstract class StructJsonConverter<T> : JsonConverter
    {
        public sealed override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public sealed override bool CanConvert(Type objectType)
        {
            throw new NotSupportedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            // ReSharper disable once HeapView.BoxingAllocation
            return RequireSupport(reader.TokenType)
                ? Parse(reader.Value)
                : default(T);
        }

        protected virtual bool RequireSupport(JsonToken tokenType)
        {
            return tokenType == JsonToken.String;
        }

        protected abstract T Parse(object value);
    }
}