#!/usr/bin/env bash

docker-machine create --driver generic \
    --generic-ip-address=hayate-docker \
    --generic-ssh-user ${USER} \
    --generic-ssh-port 2222 \
    --engine-insecure-registry registry.hayate.example.jp \
    hayate-docker 