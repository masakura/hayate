#!/usr/bin/env bash

export COMPOSE_INTERACTIVE_NO_CLI=1

SCRIPT_DIR=$(cd $(dirname $0); pwd)
cd $SCRIPT_DIR

eval $(docker-machine env hayate-docker)

docker-compose exec runner gitlab-runner unregister --all-runners
docker-compose exec runner gitlab-runner register --non-interactive \
    --run-untagged \
    --url http://gitlab.hayate.example.jp \
    --registration-token $GITLAB_CI_TOKEN \
    --executor docker \
    --docker-image busybox \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-extra-hosts "gitlab.hayate.example.jp:172.17.0.1" \
    --docker-extra-hosts  "register.hayate.example.jp:172.17.0.1"